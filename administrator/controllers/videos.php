<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends CI_Controller {

	public function __construct() {

		parent::__construct(); 

	}

	public function index() {

		$data = array();
		$data['title'] = 'Videos';
		$data['main_content'] = $this->main_content();

 		$this->load->view('main_template', $data);		

	}

	public function main_content(){

		$limit 	 = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset  = ($curpage-1)*$limit;
		$paging  = 3;

		$items = $this->mysql_queries->get_data(array(
			'table'	=> 'tbl_videos',
			'offset'	=> $offset,
			'limit'	=> $limit,
			'order'	=> 'timestamp'
		));

		$totalrows = $this->mysql_queries->get_data(array(
			'table'	=> 'tbl_videos'
		));

		$data = array();
		$data['items'] = $items;
		$data['total'] = sizeof($totalrows);
		$data['pagination'] = $this->globals->pagination(sizeof($totalrows), $curpage ,site_url('videos/index'), $paging, $limit);

		$main = $this->load->view('videos-content', $data, TRUE);

		return $main;

	}

	public function save() {

		$parse_url = parse_url($_POST['video_url']);
		$parse_str = parse_str($parse_url['query'], $video_url);

		$params = array(
			'table'=>'tbl_videos',
			'post'=>array(
				'title'=>$_POST['name'],
				'video_url'=>$video_url['v']
			)
		);
		$this->mysql_queries->insert_data($params);

		// $this->mysql_queries->insert_data(array(
		// 	'table'	=> 'tbl_videos',
		// 	'post'	=> array(
		// 		'title'	=> $_POST['name'],
		// 		'video_url'	=> $video_url['v']
		// 	)
		// ));
		
		redirect('videos');

	}

	public function update() {

		$parse_url = parse_url($_POST['video_url']);
		$parse_str = parse_str($parse_url['query'], $video_url);

		$this->mysql_queries->update_data(array(
			'table'	=> 'tbl_videos',
			'where'	=> 'id='.$_POST['id'],
			'post'	=> array(
				'title'	=> $_POST['name'],
				'video_url'	=> $video_url['v']
			)
		));

		redirect('videos');

	}

	public function edit( $id ) {

		$this->data['video'] = $this->mysql_queries->get_data(array(
			'table'	=> 'tbl_videos',
			'where'	=> 'id='.$id
		));

		$this->load->view('popup/videos/edit', $this->data);

	}

	public function delete( $id ) {

		$this->mysql_queries->delete_data(array(
			'table'	=> 'tbl_videos',
			'field'	=> 'id',
			'value'	=> $id
		));

	}

}