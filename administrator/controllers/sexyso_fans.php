<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sexyso_fans extends CI_Controller {

	public function __construct() {

		parent::__construct(); 

	}

	public function index() {

		$data = array();
		$data['title'] = 'Celebrity Tweets';
		$data['main_content'] = $this->main_content();

 		$this->load->view('main_template', $data);		

	}

	public function main_content(){

		$limit 	 = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset  = ($curpage-1)*$limit;
		$paging  = 3;


		$items = $this->mysql_queries->get_data(array('table' => 'tbl_testimonials', 'where' => 'category="sexysofans"', 'offset'=> $offset, 'limit' => $limit, 'order' => 'timestamp DESC' ));
		$totalrows = $this->mysql_queries->get_data(array('table' => 'tbl_testimonials', 'where' => 'category="sexysofans"'));

		$data = array();
		$data['items'] = $items;
		$data['total'] = sizeof($totalrows);
		$data['pagination'] = $this->globals->pagination(sizeof($totalrows), $curpage ,site_url('sexyso_fans/index'), $paging, $limit);

		$main = $this->load->view('sexysofans-content', $data, TRUE);

		return $main;

	}

	public function new_sexysofan() {
		
		$data['title'] = 'New Celebrity Tweet';
		$data['main_content'] = $this->load->view('sexysofans-new-content', $data, true);
		$this->load->view('main_template', $data);

	}

	public function save() {

		$params = array(
			'table'=>'tbl_testimonials',
			'post'=>$_POST
		);
		$this->mysql_queries->insert_data( $params );
		redirect('sexyso_fans');

	}

	public function edit( $id ) {

		$params = array(
			'table'=>'tbl_testimonials',
			'where'=>'id='.$id
		);
		$data['sexysofan'] = $this->mysql_queries->get_data( $params );
		$data['title'] = 'Edit Celebrity Tweet';
		$data['main_content'] = $this->load->view('sexysofans-new-content', $data, true);
		$this->load->view('main_template', $data);

	}

	public function update() {

		$params = array(
			'table'=>'tbl_testimonials',
			'where'=>'id='.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data( $params );
		redirect('sexyso_fans');

	}

	public function delete( $id ) {

		$params = array(
			'table'=>'tbl_testimonials',
			'field'=>'id',
			'value'=>$id
		);
		$this->mysql_queries->delete_data( $params );
		redirect('sexyso_fans');

	}

}



?>