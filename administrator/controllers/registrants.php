<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrants extends CI_Controller {

	public function __construct() {

		parent::__construct(); 

	}

	public function index() {

		$data = array( 'title'			=> "Registrants",
				 'main_content'	=> $this->main_content());

 		$this->load->view('main_template', $data);		

	}

	public function main_content() {

		$limit 	 = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset  = ($curpage-1)*$limit;
		$paging  = 3;

		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if( isset($_GET['search']) || isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}

			//reset pagination by redirecting to page 1
			if(isset($filter['search'])) {

				/* 
				'search' is the trigger for reseting ng pagination
				we unset 'search' to avoid inifinite redirect and add
				'filter' to the array to retrigger the search
				*/

				unset($filter['search']);
				$filter['filter']=1;

				//here goes the reset
				redirect('registrants/index/1'.'?'.http_build_query($filter, '', "&"), 'location');
			} else {

				//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['firstname']) ? " AND firstname LIKE '%".$filter['firstname']."%'" : false;
				$search_filters .= isset($filter['lastname']) ? " AND lastname LIKE '%".$filter['lastname']."%'" : false;
				$search_filters .= isset($filter['email']) ? " AND email LIKE '%".$filter['email']."%'" : false;
				$search_filters .= isset($filter['from'])  ? " AND date(timestamp) >= '".$filter['from']."'" : false;
				$search_filters .= isset($filter['to'])  ? " AND date(timestamp) <= '".$filter['to']."'" : false;
			}

		}

		/*end search function*/

		$params = array('table' => 'tbl_registrants',
					// 'fields' => '*, concat(lname,", ",fname) as name',
					'where' => $search_filters,
					'offset'=> $offset,
					'limit' => $limit
						);

		$registrants = $this->mysql_queries->get_data($params);

		$params = array('table' => 'tbl_registrants','where' => $search_filters);

		$totalrows = $this->mysql_queries->get_data($params);
		$registrants = $registrants;

		$data = array('registrants' => $registrants,
				  'total'	=> sizeof($totalrows),
				  'pagination'	=> $this->globals->pagination(sizeof($totalrows), $curpage ,site_url('registrants/index'), $paging, $limit)
		);

		$main = $this->load->view('registrants-content', $data, TRUE);

		return $main;

	}

	public function _remap() {

		$this->index();

	}

}