<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Celeb_tweets extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$data = array();
		$data['title'] = 'Celebrity Tweets';
		$data['main_content'] = $this->main_content();

 		$this->load->view('main_template', $data);

	}

	public function main_content(){

		$limit 	 = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset  = ($curpage-1)*$limit;
		$paging  = 3;


		$items = $this->mysql_queries->get_data(array('table' => 'tbl_testimonials', 'where' => 'category="celebtweets"', 'offset'=> $offset, 'limit' => $limit, 'order' => 'timestamp DESC' ));
		$totalrows = $this->mysql_queries->get_data(array('table' => 'tbl_testimonials', 'where' => 'category="celebtweets"'));

		$data = array();
		$data['items'] = $items;
		$data['total'] = sizeof($totalrows);
		$data['pagination'] = $this->globals->pagination(sizeof($totalrows), $curpage ,site_url('celeb_tweets/index'), $paging, $limit);

		$main = $this->load->view('celebtweets-content', $data, TRUE);

		return $main;

	}

	public function new_celebtweet() {

		$data['title'] = 'New Celebrity Tweet';
		$data['main_content'] = $this->load->view('celebtweets-new-content', $data, true);
		$this->load->view('main_template', $data);

	}

	public function save() {
		if($_POST){

	        $config['upload_path'] = 'uploads/images/';
	        $config["allowed_types"] = 'gif|jpg|jpeg|png';
	        $config['file_name'] =uniqid();

	        $this->load->library('upload', $config);

	        if(!$this->upload->do_upload())
	        {
	            $error = $this->upload->display_errors();
	            print_r($error); die();
	        } else
	        {
	            $finfo=$this->upload->data();

	            $config2['image_library'] = 'gd2';
				$config2['source_image'] = 'uploads/images/'.$finfo['file_name'];
				$config2['create_thumb'] = TRUE;
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 61;
				$config2['height'] = 61;
				$this->load->library('image_lib', $config2);

				if (!$this->image_lib->resize())
				{
				    echo $this->image_lib->display_errors();
				}else{
					$this->image_lib->resize();
				}

				$post = array(
						'title' => $_POST['title'],
						'username' => $_POST['username'],
						'description' => $_POST['description'],
						'photo' => 'images/'.$finfo['file_name'],
						'category' => $_POST['category'],
						'thumb_photo' => 'images/'.$finfo['raw_name'].'_thumb'.$finfo['file_ext']
						);

				$params = array(
					'table'=>'tbl_testimonials',
					'post'=> $post
				);

				$this->mysql_queries->insert_data( $params );
				redirect('celeb_tweets');
			}
		}
	}

	public function edit( $id ) {

		$params = array(
			'table'=>'tbl_testimonials',
			'where'=>'id='.$id
		);
		$data['celebtweet'] = $this->mysql_queries->get_data( $params );
		$data['title'] = 'Edit Celebrity Tweet';
		$data['main_content'] = $this->load->view('celebtweets-new-content', $data, true);
		$this->load->view('main_template', $data);

	}

	public function update() {

		if($_POST){

			$config['upload_path'] = 'uploads/images/';
	        $config["allowed_types"] = 'gif|jpg|jpeg|png';
	        $this->load->library('upload', $config);

	        if(!$this->upload->do_upload())
	        {
	            $error = $this->upload->display_errors();
	            print_r($error); die();
	        } else
	        {
	            $finfo=$this->upload->data();

	            $config2['image_library'] = 'gd2';
				$config2['source_image'] = 'uploads/images/'.$finfo['file_name'];
				$config2['create_thumb'] = TRUE;
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 61;
				$config2['height'] = 61;
				$this->load->library('image_lib', $config2);

				if (!$this->image_lib->resize())
				{
				    echo $this->image_lib->display_errors();
				}else{
					$this->image_lib->resize();
				}

				$post = array(
						'title' => $_POST['title'],
						'username' => $_POST['username'],
						'description' => $_POST['description'],
						'photo' => 'images/'.$finfo['file_name'],
						'thumb_photo' => 'images/'.$finfo['raw_name'].'_thumb'.$finfo['file_ext']
						);

				$params = array(
					'table'=>'tbl_testimonials',
					'where'=>'id='.$_POST['id'],
					'post'=> $post
				);
				$this->mysql_queries->update_data( $params );
				redirect('celeb_tweets');
			}
		}

	}

	public function delete( $id ) {

		$params = array(
			'table'=>'tbl_testimonials',
			'field'=>'id',
			'value'=>$id
		);
		$this->mysql_queries->delete_data( $params );
		redirect('celeb_tweets');
	}

}



?>
