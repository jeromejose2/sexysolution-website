<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Packages extends CI_Controller {

	public function __construct() {

		parent::__construct(); 

	}

	public function index() {

		$data = array();
		$data['title'] = 'Packages';
		$data['main_content'] = $this->main_content();

 		$this->load->view('main_template', $data);		

	}

	public function main_content(){

		$limit 	 = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset  = ($curpage-1)*$limit;
		$paging  = 3;


		$items = $this->mysql_queries->get_data(array('table' => 'tbl_packages', 'offset'=> $offset, 'limit' => $limit, 'order' => 'timestamp DESC' ));
		$totalrows = $this->mysql_queries->get_data(array('table' => 'tbl_packages' ));

		$data = array();
		$data['items'] = $items;
		$data['total'] = sizeof($totalrows);
		$data['pagination'] = $this->globals->pagination(sizeof($totalrows), $curpage ,site_url('packages/index'), $paging, $limit);

		$main = $this->load->view('packages-content', $data, TRUE);

		return $main;

	}

	public function new_package() {
		
		$data['title'] = 'New Package';
		$data['main_content'] = $this->load->view('packages-new-content', $data, true);
		$this->load->view('main_template', $data);

	}

	public function save() {

		$params = array(
			'table'=>'tbl_packages',
			'post'=>$_POST
		);
		$this->mysql_queries->insert_data( $params );
		redirect('packages');

	}

	public function edit( $id ) {

		$params = array(
			'table'=>'tbl_packages',
			'where'=>'id='.$id
		);
		$data['package'] = $this->mysql_queries->get_data( $params );
		$data['title'] = 'Edit Package';
		$data['main_content'] = $this->load->view('packages-new-content', $data, true);
		$this->load->view('main_template', $data);

	}

	public function update() {

		$params = array(
			'table'=>'tbl_packages',
			'where'=>'id='.$_POST['id'],
			'post'=>$_POST
		);
		$this->mysql_queries->update_data( $params );
		redirect('packages');

	}

	public function delete( $id ) {

		$params = array(
			'table'=>'tbl_packages',
			'field'=>'id',
			'value'=>$id
		);
		$this->mysql_queries->delete_data( $params );

	}

}



?>