<div class="page-controls">

    <a class="btn btn-primary" id="new-code"> <i class="icon-plus"></i><span class="hidden-phone"> New</span></a>

     <a href="<?=site_url('export/promo_codes?'.http_build_query($_GET, '', "&"))?>" class="btn hidden-phone"><i class="icon-download-alt"></i> Export</a> 

</div>



    <table class="table table-hover table-bordered table-heading">

        <thead>

            <tr>

                <td>TITLE</td>

                <td>CODE</td>

                <td style="width: 110px">ACTION</td>

            </tr>

        </thead>

        <tbody>

            </tr>

            <tr>

               <?php 

            if( $items ) : 

                foreach( $items as $k => $v ) :

            ?>

             <tr id="row-<?=$v['id']?>">

                <td><?=$v['title']?></td>

                <td>
                    <?php
                        $codes = unserialize($v['code']);
                        if( $codes ) {
                            foreach( $codes as $k => $value ) {
                                echo $value.'<br>';
                            }
                        }
                    ?>
                </td>

                <td>

                  <a class="btn btn-small edit" edit-id="<?=$v['id']?>">Edit</a>

                  <a class="btn btn-small delete" delete-id=<?=$v['id']?>>Delete</a>

                </td>

            </tr>

            <?php endforeach; else: ?>

            <tr>

                <td colspan="10"><center>No Result</center></td>

            </tr>

            <?php endif;?>

            <tr>

               <td colspan="10"><h4>Total: <?=$total?></h4></td>

          </tr>

        </tbody>



    </table>





    <div class="pagination  pull-right"><?php echo $pagination?></div>



<style type="text/css">img._status{cursor: pointer;}</style>



<script type="text/javascript">

$(function() {



    var id;



    $('#new-code').click(function(){

          popup.open({url:'<?=base_url()?>admin/popup/new_code', top: 40});

    });



    $('.edit').on('click', function(){

          id = $(this).attr('edit-id');

          popup.confirm('Edit this video?', 'Edit', function(){

            popup.open({url:'<?=base_url()?>admin/code/edit/'+id});

          });

    });



    $('.delete').on('click', function(){

          id = $(this).attr('delete-id');

          popup.confirm('Delete this video?', 'Delete', function(){

            $.post('<?=base_url()?>admin/code/delete/'+id, function(){

                $('#row-'+id).hide();

            });

          });

          

    });



    $('.advanced').click(function(){

          $('.advance-search').slideToggle();

     })



    $('input[name="from"]').datepicker({

        changeMonth: true,

        numberOfMonths: 1,

        dateFormat:'yy-mm-dd',

        onClose: function( selectedDate ) {

            $( 'input[name="to"]').datepicker( "option", "minDate", selectedDate );

        }

    });

    $('input[name="to"]').datepicker({

        defaultDate: "+1w",

        changeMonth: true,

        numberOfMonths: 1,

        dateFormat:'yy-mm-dd',

        onClose: function( selectedDate ) {

            $('input[name="from"]').datepicker( "option", "maxDate", selectedDate );

        }

    });



    $('.action > .btn').click(function(){

          var el = $(this);

          var activeClass = 'btn-success';

          var action = 'status';

          var post;

          var data = {table : 'entries',

                      wherec: 'id',

                      wheref: el.parent().data('id'),

                      items : {}}



          if( el.hasClass(activeClass) ){

               el.removeClass(activeClass);

               data.items[action] = 0;

          }else{

               el.addClass(activeClass);

               el.siblings().removeClass(activeClass);

               data.items[action] = el.attr('rel');

          }

          $.post('<?=site_url()?>/ajax/update',data);

     })

    

});

</script>