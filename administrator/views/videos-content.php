<!-- BEGIN NEW POPUP -->
<div class="modal hide fade" id="popupNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 315px; margin-left: -142.5px">
<div class="form-content" style="background: rgba(0,0,0,0);">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4><small>NEW VIDEO</small></h4>
  </div>
  <br>
   <form action="<?=base_url()?>admin/videos/save" method="POST">
      <div class="input-prepend">
              <span class="add-on">TITLE</span>
              <input type="text" class="input-medium required new-title" name="name" alt="Enter video title" >
      </div>

      <div class="input-prepend">
              <span class="add-on">VIDEO URL</span>
              <input type="text" class="input-medium required new-url" name="video_url" alt="Enter video url" >
      </div>
  </div>
  <div class="modal-footer">
        <input class="btn btn-small btn-primary submit" type="submit" value="SAVE">
      </div>
  </form>
</div>
</div>
<!-- END NEW POPUP -->

<!-- BEGIN DELETE POPUP -->
<div class="modal hide fade" id="popupDelete">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><small>DELETE VIDEO</small></h4>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete this item?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-small btn-primary delete-yes" data-dismiss="modal" aria-hidden="true">YES</button>
    <button class="btn btn-small" data-dismiss="modal" aria-hidden="true">CLOSE</button>
  </div>
</div>
<!-- END DELETE POPUP -->

<!-- BEGIN EDIT POPUP -->
<div class="modal hide fade" id="popupEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 315px; margin-left: -142.5px">
<div class="form-content" style="background: rgba(0,0,0,0);">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4><small>EDIT VIDEO</small></h4>
  </div>
  <br>
   <form action="<?=base_url()?>admin/videos/update" method="POST">
      <input type="hidden" name="id" class="edit-id">
      <div class="input-prepend">
              <span class="add-on">TITLE</span>
              <input type="text" class="input-medium required edit-title" name="name" alt="Enter video title" >
      </div>

      <div class="input-prepend">
              <span class="add-on">VIDEO URL</span>
              <input type="text" class="input-medium required edit-url" name="video_url" alt="Enter video url" >
      </div>
  </div>
  <div class="modal-footer">
        <input class="btn btn-small btn-primary submit" type="submit" value="UPDATE">
      </div>
  </form>
</div>
</div>
<!-- END EDIT POPUP -->

<!-- BEGIN PREVIEW POPUP -->
<div class="modal hide fade" id="popupPreview">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><small>PREVIEW VIDEO</small></h4>
  </div>
  <div class="modal-body">
      <div class="video-preview"></div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-small preview-close" data-dismiss="modal" aria-hidden="true">CLOSE</button>
  </div>
</div>
<!-- END PREVIEW POPUP -->

<div class="page-controls">
    <a href="#popupNew" class="btn btn-primary new" role="button" data-toggle="modal"> <i class="icon-plus"></i><span class="hidden-phone"> New</span></a>
     <!-- <a href="<?=site_url('export/entries?'.http_build_query($_GET, '', "&"))?>" class="btn hidden-phone"><i class="icon-download-alt"></i> Export</a>  -->
</div>

    <table class="table table-hover table-bordered table-heading">
        <thead>
            <tr>
                <td style="width: 100px">VIDEO</td>
                <td>TITLE</td>
                <td style="width: 110px">ACTION</td>
            </tr>
        </thead>
        <tbody>
            </tr>
            <tr>
               <?php 
            if( $items ) : 
                foreach( $items as $k => $v ) :
            ?>
             <tr id="row-<?=$v['id']?>">
                <td><a href="#popupPreview" class="preview" data-url="<?=$v['video_url']?>" role="button" data-toggle="modal"><img style="width: 100px; height: 100px; border-radius: 5px; cursor: pointer" src="https://img.youtube.com/vi/<?=$v['video_url']?>/0.jpg"></a></td>
                <td><?=$v['title']?></td>
                <td>
                  <a href="#popupEdit" role="button" data-toggle="modal" class="btn btn-small edit" edit-id="<?=$v['id']?>" data-id="<?=$v['id']?>" data-url="<?=$v['video_url']?>" data-title="<?=$v['title']?>">Edit</a>
                  <a href="#popupDelete" role="button" data-toggle="modal" class="btn btn-small delete" delete-id="<?=$v['id']?>" data-id="<?=$v['id']?>">Delete</a>
                </td>
            </tr>
            <?php endforeach; else: ?>
            <tr>
                <td colspan="10"><center>No Result</center></td>
            </tr>
            <?php endif;?>
            <tr>
               <td colspan="10"><h4>Total: <?=$total?></h4></td>
          </tr>
        </tbody>

    </table>


    <div class="pagination  pull-right"><?php echo $pagination?></div>

<style type="text/css">img._status{cursor: pointer;}</style>

<script type="text/javascript">
  function deleteThis(id) {
    $.post('<?=base_url()?>admin/videos/delete/'+id, function(){
        $('#row-'+id).hide();
    });
  }

$(function() {

    var id;

    $('.edit').on('click', function(){
        var eid = $(this).data('id');
        var title = $(this).data('title');
        var url = $(this).data('url');

        $('.edit-id').val(eid);
        $('.edit-title').val(title);
        $('.edit-url').val('https://www.youtube.com/watch?v='+url);
    });

    $('.new').click(function(){
        $('.new-title').val('');
        $('.new-url').val('');
    });

    $('.delete').on('click', function(){
        var delete_id = $(this).data('id');
        $('.delete-yes').attr('onclick', 'deleteThis('+delete_id+')');
    });

    $('.preview').on('click', function(){
        var url = $(this).data('url');
        $('.video-preview').html('<object width="525" height="350" data="https://www.youtube.com/v/'+url+'?autoplay=1" type="application/x-shockwave-flash"></object>');
    });
    
    $('.advanced').click(function(){
          $('.advance-search').slideToggle();
     })

    $('input[name="from"]').datepicker({
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat:'yy-mm-dd',
        onClose: function( selectedDate ) {
            $( 'input[name="to"]').datepicker( "option", "minDate", selectedDate );
        }
    });
    $('input[name="to"]').datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat:'yy-mm-dd',
        onClose: function( selectedDate ) {
            $('input[name="from"]').datepicker( "option", "maxDate", selectedDate );
        }
    });

    $('.action > .btn').click(function(){
          var el = $(this);
          var activeClass = 'btn-success';
          var action = 'status';
          var post;
          var data = {table : 'entries',
                      wherec: 'id',
                      wheref: el.parent().data('id'),
                      items : {}}

          if( el.hasClass(activeClass) ){
               el.removeClass(activeClass);
               data.items[action] = 0;
          }else{
               el.addClass(activeClass);
               el.siblings().removeClass(activeClass);
               data.items[action] = el.attr('rel');
          }
          $.post('<?=site_url()?>/ajax/update',data);
     })
    
});

  $('.vid-pop').on('click', function(){
      var id = $(this).data('id');
      popup.open({url:'<?=base_url()?>admin/popup/videos_preview', data:{id:id}});
  });
</script>