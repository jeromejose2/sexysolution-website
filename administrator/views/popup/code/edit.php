<div class="container" style="width:350px;margin:0 auto;">

<a href="javascript:void(0)" class="lytebox-close" style="right: 320px">x</a>

<div class="well">

      

    <div class="form-content" style="background: rgba(0,0,0,0);">



      <h5>Update Promo Code</h5>



     <form action="<?=base_url()?>admin/code/update" method="POST">

        <input style="display:none" name="id" value="<?=$code[0]['id']?>">

        <div class="input-prepend">

                <span class="add-on">TITLE</span>

                <input type="text" class="input-medium" name="title" value="<?=$code[0]['title']?>">

        </div>

        <div id="more"></div>

        <?php if( $codes ) : ?>
            <?php $i=1; foreach( $codes as $k => $v ) : ?>
                <div class="input-prepend" id="<?=$i?>">
                    <span class="add-on">CODE</span>
                    <input type="text" class="input-medium" name="code[]" value="<?=$v?>">
                    <a title="Remove this code" class="badge badge-important" onclick="del(<?=$i?>)" style="margin-top: 5px"><i class="icon-remove"></i></a>
                </div>
            <?php $i++; endforeach ?>
        <?php else : ?>
            <div class="input-prepend">
                    <span class="add-on">CODE</span>
                    <input type="text" class="input-medium required" name="code[]" alt="Enter code" >
            </div>
        <?php endif; ?>

        <a class="btn btn-small add-code">Add Code</a>
        <br>
        <br>

        <input class="btn btn-primary submit" type="submit" value="Update">

    </form>



   </div>

</div>

</div>

<script>
  $('.add-code').bind('click', function(){
      var id = Math.floor(Math.random() * 90000) + 10000;
      $('#more').after('<div class="input-prepend" id="'+id+'"><span class="add-on">CODE</span><input type="text" class="input-medium required" name="code[]" alt="Enter code" ><a title="Remove this code" class="badge badge-important" style="margin-top: 5px" onclick="del('+id+')"><i class="icon-remove"></i></a></div>');
  });

  function del( id ) {
      $('#'+id).remove();
  }
</script>