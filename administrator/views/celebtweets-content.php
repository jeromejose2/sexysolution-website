<!-- BEGIN DELETE POPUP -->
<div class="modal hide fade" id="popupDelete">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><small>DELETE celebtweet</small></h4>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete this item?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-small btn-primary delete-yes" data-dismiss="modal" aria-hidden="true">YES</button>
    <button class="btn btn-small" data-dismiss="modal" aria-hidden="true">CLOSE</button>
  </div>
</div>
<!-- END DELETE POPUP -->

<div class="page-controls">
    <a href="<?=base_url()?>admin/celeb_tweets/new_celebtweet" class="btn btn-primary"> <i class="icon-plus"></i><span class="hidden-phone"> New</span></a>
     <!-- <a href="<?=site_url('export/entries?'.http_build_query($_GET, '', "&"))?>" class="btn hidden-phone"><i class="icon-download-alt"></i> Export</a> -->
     <div class=" advance-search">
        <div class="form-content">
            <form class="form-search">
                <div class="input-prepend">
                    <span class="add-on">AUTHOR</span>
                    <input type="text" class="input-medium" name="author" value="<?=isset($_GET['author']) ? $_GET['author'] : '' ?>" >
                </div>
               

                <div class="input-prepend">
                    <span class="add-on">HASHTAG</span>
                    <input type="text" class="input-medium" name="hashtag" value="<?=isset($_GET['hashtag']) ? $_GET['hashtag'] : '' ?>" >
                </div>

                <div class="input-prepend">
                    <span class="add-on">SOURCE</span>
                    <select class="input-medium" name="source">
                        <option value="">All</option>
                        <option value="upload" <?=isset($_GET['source']) && $_GET['source']=='upload'  ? 'selected="selected"' : '' ?>>Uploaded</option>
                        <option value="instagram" <?=isset($_GET['source']) && $_GET['source']=='instagram'  ? 'selected="selected"' : '' ?>>Instagram</ption>
                        <option value="twitter" <?=isset($_GET['source']) && $_GET['source']=='twitter'  ? 'selected="selected"' : '' ?>>Twitter</option>
                    </select>
                </div>

                <div class="input-prepend">
                    <span class="add-on">CAPTION</span>
                    <input type="text" class="input-medium" name="caption" value="<?=isset($_GET['caption']) ? $_GET['caption'] : '' ?>" >
                </div>

                <div class="input-prepend">
                    <span class="add-on">STATUS</span>
                    <select class="input-medium" name="status">
                        <option value="">All</option>
                        <option value="1" <?=isset($_GET['status']) && $_GET['status']==1  ? 'selected="selected"' : '' ?>>Approved</option>
                        <option value="0" <?=isset($_GET['status']) && $_GET['status']==0  ? 'selected="selected"' : '' ?>>Pending</option>
                        <option value="2" <?=isset($_GET['status']) && $_GET['status']==2  ? 'selected="selected"' : '' ?>>Declined</option>
                         <option value="3" <?=isset($_GET['status']) && $_GET['status']==3  ? 'selected="selected"' : '' ?>>Winners</option>
                    </select>
                </div>


               <div class="input-prepend">
                    <span class="add-on">DATE FROM</span>
                    <input type="text" class="input-small" value="<?=isset($_GET['from']) ? $_GET['from'] : '' ?>" name="from">
                </div>
                    
                <div class="input-prepend">
                    <span class="add-on">DATE TO</span>
                    <input type="text" class="input-small" value="<?=isset($_GET['to']) ? $_GET['to'] : '' ?>" name="to" >
                </div>

                <div class="input-prepend">

                   <span class="add-on">ITEMS/PAGE</span>
                    <select name="psize" class="input-small">
                        <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                        <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                        <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                        <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                        <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                    </select>
                </div>
                    
                <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>
            </form>
        </div>
     </div>
</div>

   <table class="table table-hover table-bordered table-heading">
        <thead>
            <tr>
                <td>PHOTO</td>
                <td>NAME</td>
                <td>USERNAME</td>
                <td>DESCRIPTION</td>
                <td style="width: 110px">ACTION</td>
            </tr>
        </thead>
        <tbody>
            </tr>
            <tr>
               <?php 
            if( $items ) : 
                foreach( $items as $k => $v ) :
            ?>
             <tr id="row-<?=$v['id']?>">
                <td><!-- <a href="#" onclick="img_popup('<?=base_url()?>uploads/<?=$v['photo']?>');"> -->
                         <img src="<?=base_url()?>uploads/<?=$v['photo']?>" width="30" height="30">
                    <!-- </a> -->
                </td>
                <td><?=$v['title']?></td>
                <td><?=$v['username']?></td>
                <td><?=$v['description']?></td>
                <td>
                    <a href="<?=base_url()?>admin/celeb_tweets/edit/<?=$v['id']?>" class="btn btn-small">Edit</a>
                    <a href="#popupDelete" class="btn btn-small delete" role="button" data-toggle="modal" data-id="<?=$v['id']?>" delete-id="<?=$v['id']?>">Delete</a>
                </td>
            </tr>
            <?php endforeach; else: ?>
            <tr>
                <td colspan="10"><center>No Result</center></td>
            </tr>
            <?php endif;?>
            <tr>
               <td colspan="10"><h4>Total: <?=$total?></h4></td>
          </tr>
        </tbody>

    </table>


    <div class="pagination  pull-right"><?php echo $pagination?></div>

<style type="text/css">img._status{cursor: pointer;}</style>

<script type="text/javascript">
$(function() {

    $('.advanced').click(function(){
          $('.advance-search').slideToggle();
     })
    $('.delete').on('click', function(){
        var id = $(this).data('id');
        $('.delete-yes').attr('data-id', id);
    });

    $('.delete-yes').on('click', function(){
        var id = $(this).data('id');
        $.post('<?=base_url()?>admin/celeb_tweets/delete/'+id, function(){
            $('#row-'+id).hide();
            location.reload();
        });
    });

    // $('.delete').on('click', function(){
    //     var id = $(this).data('id');
    //     popup.open({message:'Delete this item?', top:150, type:'confirm', onConfirm: function(){
    //         $.post('<?=base_url()?>admin/celebtweets/delete/'+id, function(){
    //             $('#row-'+id).hide();
    //         });
    //     }});
    // });

    $('input[name="from"]').datepicker({
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat:'yy-mm-dd',
        onClose: function( selectedDate ) {
            $( 'input[name="to"]').datepicker( "option", "minDate", selectedDate );
        }
    });

    $('input[name="to"]').datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat:'yy-mm-dd',
        onClose: function( selectedDate ) {
            $('input[name="from"]').datepicker( "option", "maxDate", selectedDate );
        }
    });

    $('.action > .btn').click(function(){
          var el = $(this);
          var activeClass = 'btn-success';
          var action = 'status';
          var post;
          var data = {table : 'entries',
                      wherec: 'id',
                      wheref: el.parent().data('id'),
                      items : {}}

         if( el.hasClass(activeClass) ){
               el.removeClass(activeClass);
               data.items[action] = 0;
          }else{
               el.addClass(activeClass);
               el.siblings().removeClass(activeClass);
               data.items[action] = el.attr('rel');
          }
          $.post('<?=site_url()?>/ajax/update',data);
     })
    
});

</script>