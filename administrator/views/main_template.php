<!DOCTYPE html>
<html>
<head>
    <title>CMS </title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex, nofollow">
    <meta name="author" content="NuWorks Interactive Labs, Inc." />
    <!-- favicon and touch icons -->
    <link rel="shortcut icon" href="<?=base_url()?>images/admin/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url()?>images/admin/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url()?>images/admin/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url()?>images/admin/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?=base_url()?>images/admin/ico/apple-touch-icon-57-precomposed.png">
    <!--[if lt IE 9]><script src="js/html5.js"></script><![endif]-->
    <!-- css -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>fonts/admin/fonts.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>fonts/admin/font-awesome.min.css" />
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="<?=base_url()?>fonts/admin/font-awesome-ie7.min.css" /><![endif]-->

	<link rel="stylesheet" media="screen" href="<?=base_url()?>css/admin/bootstrap-fileupload.min.css">
    <link rel="stylesheet" media="screen" href="<?=base_url()?>css/admin/bootstrap.css">
    <link rel="stylesheet" href="<?=base_url()?>css/admin/bootstrap-responsive.css">
    <link rel="stylesheet" href="<?=base_url()?>css/admin/mystyle.css" />

    <!-- jquery ui -->
    <link href="<?=base_url()?>plugins/jquery-ui/css/smoothness/jquery-ui-1.10.0.custom.css" rel="stylesheet">
    <script src="<?=base_url()?>plugins/jquery-ui/js/jquery-1.9.0.js"></script>
    <script src="<?=base_url()?>plugins/jquery-ui/js/jquery-ui-1.10.0.custom.js"></script>
    <!-- /jquery ui -->

    <script src="<?=base_url()?>js/admin/bootstrap-fileupload.min.js"></script>
    <script src="<?=base_url()?>js/admin/bootstrap.min.js"></script>
    <script src="<?=base_url()?>js/admin/custom.js"></script>

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/admin/lytebox.2.1.css">
    <script type="text/javascript" src="<?=base_url()?>js/admin/lytebox.2.1.js"></script>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjxtjNyjATUWGLx-1v0zehvr0bkc0ACnE&sensor=false">
    </script>

    <script type="text/javascript">

    </script>

</head>
    <body>
        <!-- header -->
    <header class="navbar navbar-inverse navbar-fixed-top">

        <div class="navbar-inner">
            <!-- container -->
            <div class="container-fluid">
                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></spasn>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>


<!-- Be sure to leave the brand out there if you want it shown -->
<a class="brand" href="<?=base_url()?>admin/home">CMS</a>

<!-- Everything you want hidden at 940px or less, place within here -->
<div class="nav-collapse collapse">
    <ul class="nav nav-pills">
        <li  <?=$this->uri->segment(1)=='home' || $this->uri->segment(1)=='' ? 'class="active"' : ''?> ><a href="<?=site_url('home')?>">Dashboard</a></li>
        <li  <?=$this->uri->segment(1)=='about'  ? 'class="active"' : ''?> ><a href="<?=site_url('about')?>">About</a></li>
        <li  <?=$this->uri->segment(1)=='registrants'  ? 'class="active"' : ''?> ><a href="<?=site_url('registrants')?>">Registrants</a></li>
        <li  <?=$this->uri->segment(1)=='clinics'  ? 'class="active"' : ''?> ><a href="<?=site_url('clinics')?>">Clinics</a></li>
        <!-- <li  <?=$this->uri->segment(1)=='pricing'  ? 'class="active"' : ''?> ><a href="<?=site_url('pricing')?>">Pricing</a></li> -->
        <li  <?=$this->uri->segment(1)=='packages'  ? 'class="active"' : ''?> ><a href="<?=site_url('packages')?>">Packages</a></li>
        <li  <?=$this->uri->segment(1)=='videos'  ? 'class="active"' : ''?> ><a href="<?=site_url('videos')?>">Videos</a></li>
        <li class="dropdown <?=$this->uri->segment(1)=='testimonials' || $this->uri->segment(1)=='tweets' || $this->uri->segment(1)=='fans' ? 'active' : ''?>">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Testimonials <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a tabindex="-1" href="<?=site_url('testimonials')?>">Testimonials</a></li>
                    <li><a tabindex="-1" href="<?=site_url('celeb_tweets')?>">Celebrity Tweets</a></li>
                    <li><a tabindex="-1" href="<?=site_url('sexyso_fans')?>">Sexy Solutions Fans</a></li>
                </ul>
        </li>
        <li class="dropdown <?=$this->uri->segment(1)=='promo_registrants' || $this->uri->segment(1)=='code' ? 'active' : ''?>">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Promos <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a tabindex="-1" href="<?=site_url('promo_registrants')?>">Registrants</a></li>
                    <li><a tabindex="-1" href="<?=site_url('code')?>">Code</a></li>
                </ul>
        </li>
        <li  <?=$this->uri->segment(1)=='analytics'  ? 'class="active"' : ''?> ><a href="<?=site_url('analytics')?>">Analytics</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Settings <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a tabindex="-1" href="<?=site_url('accounts')?>">CMS User Accounts</a></li>
                <li class="divider"></li>
                <li><a tabindex="-1" href="<?=site_url('myaccount')?>">My Account Settings</a></li>
                <li><a tabindex="-1" href="<?=site_url('logout')?>">Logout</a></li>
            </ul>
        </li>
    </ul>
</div>
    </div>
    <!-- /container -->
</div>
</header>

<!-- /header -->

<!-- content -->

<section class="container-fluid">
    <div class="page-header"> <h3><?=$title?><h3> </div>
    <div class="content"><?=$main_content?></div>
</section>

<!-- /content -->

<hr>

<!-- footer -->



<section class="container">

    <p><small>Managed with <a href="http://www.nuworks.ph" target="_blank">NuWorks Interactive Labs, Inc.</a></small></p>

</section>
</body>

</html>
<style type="text/css">
.fileupload-new .input-append .btn-file {
    width: 70px;
    height:20px;
}

.fileupload-exists .input-append .btn-file {
    width: 70px;
    height:20px;
}
</style>