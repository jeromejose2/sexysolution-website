<? if( isset($clinic[0]['id']) ) : ?>
    <form method="POST" action="<?=base_url()?>admin/clinics/update">
<? else : ?>
    <form method="POST" action="<?=base_url()?>admin/clinics/save">
<? endif; ?>
    <div class="page-controls">
        <button class="btn btn-primary"><i class="icon-ok"></i> Save</span></button>
        <a class="btn btn-primary hidden-phone" onclick="window.history.back()"><i class="icon-remove"></i> Cancel</a>
    </div>

    <input type="hidden" name="id" value="<?=isset($clinic[0]['id']) ? $clinic[0]['id'] : ''?>">
    <table style="margin-left: 2%">
        <tr style="vertical-align: top">
            <td><h5>Address</h5></td>
            <td><input type="text" name="address" class="input-large" placeholder="Branch address" value="<?=isset($clinic[0]['address']) ? $clinic[0]['address'] : ''?>"></td>

            <td style="padding-left: 10px"><h5>Email Address</h5></td>
            <td><input type="text" name="email" class="input-large" placeholder="example@gmail.com" value="<?=isset($clinic[0]['email']) ? $clinic[0]['email'] : ''?>"></td>
        </tr>

        <tr style="vertical-align: top">
            <td><h5>Mobile No.</h5></td>
            <td><input type="text" name="mobile" class="input-large" placeholder="09299999999" value="<?=isset($clinic[0]['mobile']) ? $clinic[0]['mobile'] : ''?>"></td>

            <td style="padding-left: 10px"><h5>Landline No.</h5></td>
            <td><input type="text" name="landline" class="input-large" placeholder="948-88-88" value="<?=isset($clinic[0]['landline']) ? $clinic[0]['landline'] : ''?>"></td>
        </tr>

        <tr style="vertical-align: top">
            <td><h5>Branch Hours</h5></td>
            <td><textarea id="content" name="branch_hours"><?=isset($clinic[0]['branch_hours']) ? $clinic[0]['branch_hours'] : ''?></textarea></td>

            <!-- <td><h5>Branch Hour From</h5></td>
            <td><input type="text" name="hours_from[]" class="input-large" placeholder="10:00 AM" value="<?=isset($clinic[0]['hours_from']) ? $clinic[0]['hours_from'] : ''?>"></td>

            <td style="padding-left: 10px"><h5>Branch Hour To</h5></td>
            <td><input type="text" name="hours_to[]" class="input-large" placeholder="5:00 PM" value="<?=isset($clinic[0]['hours_to']) ? $clinic[0]['hours_to'] : ''?>"></td> -->
        </tr>

        <tr>
            <td>*<small>Drag the pin to set location</small></td>
        </tr>

        <tr style="vertical-align: top">
            <td><h5>Latitude</h5></td>
            <td><input type="text" id="latitude" name="latitude" class="input-large" placeholder="Latitude" value="<?=isset($clinic[0]['latitude']) ? $clinic[0]['latitude'] : ''?>"></td>

            <td style="padding-left: 10px"><h5>Longitude</h5></td>
            <td><input type="text" id="longitude" name="longitude" class="input-large" placeholder="Longitude" value="<?=isset($clinic[0]['longitude']) ? $clinic[0]['longitude'] : ''?>"></td>
        </tr>
    </table>
</form>
<table style="margin-left: 2%">
    <tr style="vertical-align: top">
        <td><div id="map_canvas" style="height:600px; width: 600px; border: 2px solid black; border-radius: 5px"></div></td>
    </tr>
</table>

<script src="<?=base_url()?>js/admin/texteditor/nicEdit.js"></script>
<script type="text/javascript">
bkLib.onDomLoaded(function(){
    new nicEditor({iconsPath : '<?=base_url()?>js/admin/texteditor/nicEditorIcons.gif', buttonList : ['fontSize','bold','italic']}).panelInstance('content');
});
</script>

<script>
    $(function(){
        <? if( isset($clinic[0]['longitude']) || isset($clinic[0]['latitude']) ) : ?>
            var myLatlng = new google.maps.LatLng(<?=$clinic[0]['latitude']?>,<?=$clinic[0]['longitude']?>);
        <? else : ?>
            var myLatlng = new google.maps.LatLng(14.59885354948353, 120.98423133572385);
        <? endif; ?>
        var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

        var infowindow = new google.maps.InfoWindow();

        var bounds = new google.maps.LatLngBounds();

        var marker = new google.maps.Marker({
              draggable: true,
              position: myLatlng, 
              map: map,
              // icon: '<?=base_url()?>images/admin/marker.png',
              title: "Sexy Solutions Clinic"
          });

        google.maps.event.addListener(marker, 'drag', function(event){
           document.getElementById("latitude").value = event.latLng.lat();
           document.getElementById("longitude").value = event.latLng.lng();
    });

        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(15);
            google.maps.event.removeListener(listener);
        });

          // google.maps.event.addDomListener(window, 'load', initialize);
    });
</script>