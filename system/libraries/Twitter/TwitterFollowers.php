<?php

require_once 'TwitterResult.php';

/**
 * 
 * @author anthony
 * 
 */
class TwitterFollowers extends TwitterResult
{
    /**
     * 
     * @var array
     */
    private $cursors = array();

    /**
     * 
     * @param array $cursors
     * @return TwitterFollowers
     */
    public function set_cursors($cursors)
    {
        $this->cursors = $cursors;
        return $this;
    }

    /**
     * 
     * @param string $id
     * @return TwitterUser
     */
    public function get_user($id)
    {
        return $this->twitter->get_user_by_id($id);
    }

    /**
     * 
     * @param string $id
     * @return boolean
     */
    public function has_id($id)
    {
        return in_array($id, $this['ids']) ? true : false;
    }
}