<?php

/**
 * 
 * @author anthony
 * 
 */
class CI_Amf
{
    public function __construct()
    {
        require_once $this->getDirectory().'index.php';
    }

    /**
     * 
     * @return string
     */
    public function getDirectory()
    {
        return dirname(__FILE__).'/Amfphp/';
    }

    public function service($location)
    {
        $config = new Amfphp_Core_Config();//do something with config object here
        $config->serviceFolderPaths = array($location);
        $gateway = Amfphp_Core_HttpRequestGatewayFactory::createGateway($config);

        $output = $gateway->service();
        $response = $gateway->getResponseHeaders();
        foreach ($response as $res) {
            $this->output->set_header($res);
        }
        $ci = & get_instance();
        $ci->output->set_output($output);
    }
}