<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sexy_Treats extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function validate() {

		$codes = $this->mysql_queries->get_data(array('table'=>'tbl_code'));

		// check if user input a character;
		if($_POST['input']) {
			// loop every promo codes;
			foreach($codes as $k => $code) {
				$promo = $code['title'];
				$validate = unserialize($code['code']);
				foreach($validate as $k => $value) {
					// check if user input code matches admin's code
					if($value == $_POST['input']) {
						if(!$this->check_registrant($promo, $this->session->userdata('email'))) {
							$params = array(
								'table'=>'tbl_promo_registrants',
								'post'=>array(
									'name'=>$this->session->userdata('firstname').' '.$this->session->userdata('lastname'),
									'email'=>$this->session->userdata('email'),
									'promo'=>$promo,
									'code'=>$_POST['input']
								)
							);
							$saved = $this->mysql_queries->insert_data($params);
							if($saved) {
								echo 'Thank you! Please check your e-mail for more details.';
							}
						} else {
							echo 'Sorry, you’ve already used that code.';
						}
					}
				}
			}
		} 
		
	}

	private function check_registrant($promo, $email) {

		$params = array(
			'table'=>'tbl_promo_registrants',
			'where'=>'promo=\''.$promo.'\' AND email=\''.$email.'\''
		);
		return $this->mysql_queries->get_data($params);

	}

}

?>