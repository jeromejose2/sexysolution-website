<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Popups extends CI_Controller {
    public function map1(){
        $lat = $this->input->get('lat');
        $long = $this->input->get('long');
        $data = array('lat'=>$lat,'long'=>$long);
        $this->load->view('popups/map1-content',$data);
    }
     public function map2(){
        $lat = $this->input->get('lat');
        $long = $this->input->get('long');
        $data = array('lat'=>$lat,'long'=>$long);
        $this->load->view('popups/map2-content',$data);
    }
    public function celebrity_info($id){
        $params = array(
                                  'table'   =>   'tbl_testimonials',
                                  'where' => 'id='.$id
                                );
        $data['info'] = $this->mysql_queries->get_data($params);
        $this->load->view('popups/testimonial-content',$data);
    }


}

?>
