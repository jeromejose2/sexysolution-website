<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonials extends CI_Controller {

	public function __construct() {

		parent::__construct(); 

	}

	public function index() {

		$data = array();
		$data['title'] = 'Testimonials';
		$data['main_content'] = $this->main_content();

 		$this->load->view('main_template', $data);		

	}

	public function main_content(){

		$limit 	 = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset  = ($curpage-1)*$limit;
		$paging  = 3;


		$items = $this->mysql_queries->get_data(array('table' => 'tbl_testimonials', 'where' => 'category="testimonials"', 'offset'=> $offset, 'limit' => $limit, 'order' => 'timestamp DESC' ));
		$totalrows = $this->mysql_queries->get_data(array('table' => 'tbl_testimonials', 'where' => 'category="testimonials"'));

		$data = array();
		$data['items'] = $items;
		$data['total'] = sizeof($totalrows);
		$data['pagination'] = $this->globals->pagination(sizeof($totalrows), $curpage ,site_url('testimonials/index'), $paging, $limit);

		$main = $this->load->view('testimonials-content', $data, TRUE);

		return $main;

	}

	public function new_testimonial() {
		
		$data['title'] = 'New Testimonial';
		$data['main_content'] = $this->load->view('testimonials-new-content', $data, true);
		$this->load->view('main_template', $data);

	}

	public function save() {
		if($_POST){

			$config['upload_path'] = 'uploads/images/'; 
	        $config["allowed_types"] = 'gif|jpg|jpeg|png';
	        $this->load->library('upload', $config);

	        if(!$this->upload->do_upload()) 
	        {            
	            $error = $this->upload->display_errors();
	            print_r($error); die(); 
	        } else
	        {
	            $finfo=$this->upload->data();

	            $config2['image_library'] = 'gd2';
				$config2['source_image'] = 'uploads/images/'.$finfo['file_name'];
				$config2['create_thumb'] = TRUE;
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 210;
				$config2['height'] = 210;
				$this->load->library('image_lib', $config2);

				if (!$this->image_lib->resize())
				{
				    echo $this->image_lib->display_errors();
				}else{
					$this->image_lib->resize();
				}

				$post = array(
						'title' => $_POST['title'],
						'sub_title' => $_POST['sub_title'], 
						'description' => $_POST['description'],
						'photo' => 'images/'.$finfo['file_name'],
						'category' => $_POST['category'],
						'thumb_photo' => 'images/'.$finfo['raw_name'].'_thumb'.$finfo['file_ext']
						);

				$params = array(
					'table'=>'tbl_testimonials',
					'post'=> $post
				);

				$this->mysql_queries->insert_data( $params );
				redirect('testimonials');
			}			
		}
	}

	public function edit( $id ) {

		$params = array(
			'table'=>'tbl_testimonials',
			'where'=>'id='.$id
		);
		$data['testimonial'] = $this->mysql_queries->get_data( $params );
		$data['title'] = 'Edit Testimonial';
		$data['main_content'] = $this->load->view('testimonials-new-content', $data, true);
		$this->load->view('main_template', $data);

	}

	public function update() {

		if($_POST){

			$config['upload_path'] = 'uploads/images/'; 
	        $config["allowed_types"] = 'gif|jpg|jpeg|png';
	        $this->load->library('upload', $config);

	        if(!$this->upload->do_upload()) 
	        {            
	            $error = $this->upload->display_errors();
	            print_r($error); die(); 
	        } else
	        {
	            $finfo=$this->upload->data();

	            $config2['image_library'] = 'gd2';
				$config2['source_image'] = 'uploads/images/'.$finfo['file_name'];
				$config2['create_thumb'] = TRUE;
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 210;
				$config2['height'] = 210;
				$this->load->library('image_lib', $config2);

				if (!$this->image_lib->resize())
				{
				    echo $this->image_lib->display_errors();
				}else{
					$this->image_lib->resize();
				}

				$post = array(
						'title' => $_POST['title'],
						'sub_title' => $_POST['sub_title'], 
						'description' => $_POST['description'],
						'photo' => 'images/'.$finfo['file_name'],
						'thumb_photo' => 'images/'.$finfo['raw_name'].'_thumb'.$finfo['file_ext']
						);

				$params = array(
					'table'=>'tbl_testimonials',
					'where'=>'id='.$_POST['id'],
					'post'=> $post
				);

				$this->mysql_queries->update_data( $params );
				redirect('testimonials');
			}			
		}

	}

	public function delete( $id ) {

		$params = array(
			'table'=>'tbl_testimonials',
			'field'=>'id',
			'value'=>$id
		);
		$this->mysql_queries->delete_data( $params );
		redirect('testimonials');
	}
}



?>