<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promo_Registrants extends CI_Controller {

	public function __construct() {

		parent::__construct(); 

	}

	public function index() {

		$data = array();
		$data['title'] = 'Promo Registrants';
		$data['main_content'] = $this->main_content();

 		$this->load->view('main_template', $data);		

	}

	public function main_content(){

		$limit 	 = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset  = ($curpage-1)*$limit;
		$paging  = 3;

		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if( isset($_GET['search'])|| isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}

			//reset pagination by redirecting to page 1
			if(isset($filter['search'])) {
				/* 
				'search' is the trigger for reseting ng pagination
				we unset 'search' to avoid inifinite redirect and add
				'filter' to the array to retrigger the search
				*/

				unset($filter['search']);
				$filter['filter']=1;

				//here goes the reset
				redirect('promo_registrants/index/1'.'?'.http_build_query($filter, '', "&"), 'location');

			} else {
				//add the filters to 'where'(sql) statement
				$search_filters .= isset($filter['name']) ? " AND name LIKE '%".$filter['name']."%'" : false;
				$search_filters .= isset($filter['email']) ? " AND email LIKE '%".$filter['email']."%'" : false;
				$search_filters .= isset($filter['promo']) ? " AND promo LIKE '%".$filter['promo']."%'" : false;
				$search_filters .= isset($filter['from'])  ? " AND date(timestamp) >= '".$filter['from']."'" : false;
				$search_filters .= isset($filter['to'])  ? " AND date(timestamp) <= '".$filter['to']."'" : false;
			}

		}

		/*end search function*/

		$params = array(
			'table'=>'tbl_promo_registrants',
			'where'=>$search_filters,
			'offset'=>$offset,
			'limit'=>$limit,
			'order'=>'timestamp DESC'
		);
		$items = $this->mysql_queries->get_data($params);

		$params = array(
			'table'=>'tbl_promo_registrants',
			'where'=>$search_filters
		);
		$totalrows = $this->mysql_queries->get_data($params);

		$data = array();
		$data['items'] = $items;
		$data['total'] = sizeof($totalrows);
		$data['pagination'] = $this->globals->pagination(sizeof($totalrows), $curpage ,site_url('promo_registrants/index'), $paging, $limit);

		$main = $this->load->view('promo-registrants-content', $data, TRUE);

		return $main;

	}

}