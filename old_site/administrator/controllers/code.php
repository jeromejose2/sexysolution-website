<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Code extends CI_Controller {

	public function __construct() {

		parent::__construct(); 

	}

	public function index() {

		$data = array();
		$data['title'] = 'Promo Code';
		$data['main_content'] = $this->main_content();

 		$this->load->view('main_template', $data);		

	}

	

	public function main_content(){

		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$offset  = ($curpage-1)*$limit;
		$paging  = 3;

		$items = $this->mysql_queries->get_data(array(
			'table'	=> 'tbl_code',
			'offset'	=> $offset,
			'limit'	=> $limit,
			'order'	=> 'timestamp DESC'
		));

		$totalrows = $this->mysql_queries->get_data(array(
			'table'	=> 'tbl_code'
		));

		$data = array();
		$this->data['items'] = $items;
		$this->data['total'] = sizeof($totalrows);
		$this->data['pagination'] = $this->globals->pagination(sizeof($totalrows), $curpage ,site_url('code/index'), $paging, $limit);

		$main = $this->load->view('code-content', $this->data, TRUE);

		return $main;

	}

	public function save() {

		$params = array(
			'table'=>'tbl_code',
			'post'=>array(
				'title'=>$_POST['title'],
				'code'=>serialize($_POST['code'])
			)
		);
		$this->mysql_queries->insert_data( $params );

		redirect('code');

	}

	public function update() {

		$params = array(
			'table'=>'tbl_code',
			'where'=>'id='.$_POST['id'],
			'post'=>array(
				'title'=>$_POST['title'],
				'code'=>serialize(@$_POST['code'])
			)
		);
		$this->mysql_queries->update_data( $params );

		redirect('code');

	}

	public function edit( $id ) {

		$this->data['code'] = $this->mysql_queries->get_data(array(
			'table'	=> 'tbl_code',
			'where'	=> 'id='.$id
		));
		$this->data['codes'] = unserialize($this->data['code'][0]['code']);

		$this->load->view('popup/code/edit', $this->data);

	}



	public function delete( $id ) {

		$this->mysql_queries->delete_data(array(
			'table'	=> 'tbl_code',
			'field'	=> 'id',
			'value'	=> $id
		));

	}

	public function edit_code_content( $id ) {

		$params = array(
			'table'=>'tbl_code',
			'where'=>'id='.$id
		);
		$data['result'] = $this->mysql_queries->get_data( $params );
		$data['results'] = unserialize($data['result'][0]['code']);
		$this->load->view('edit-code-content', $data);

	}

}