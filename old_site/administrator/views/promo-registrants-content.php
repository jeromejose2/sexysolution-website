<div class="page-controls">
	<a class="btn advanced"> <i class="icon-search"></i><span class="hidden-phone"> Advance Query</span></a>
     <a href="<?=site_url('export/promo_registrants?'.http_build_query($_GET, '', "&"))?>" class="btn hidden-phone"><i class="icon-download-alt"></i> Export</a>

     <div class=" advance-search">
        <div class="form-content">
            <form class="form-search">

                <div class="input-prepend">
                    <span class="add-on">NAME</span>
                    <input type="text" class="input-medium" name="name" value="<?=isset($_GET['name']) ? $_GET['name'] : '' ?>" >
                </div>

                <div class="input-prepend">
                    <span class="add-on">EMAIL</span>
                    <input type="text" class="input-medium" name="email" value="<?=isset($_GET['email']) ? $_GET['email'] : '' ?>">
                </div>

                <div class="input-prepend">
                    <span class="add-on">PROMO</span>
                    <input type="text" class="input-medium" name="promo" value="<?=isset($_GET['promo']) ? $_GET['promo'] : '' ?>">
                </div>

                <div class="input-prepend">
                    <span class="add-on">DATE FROM</span>
                    <input type="text" class="input-small" value="<?=isset($_GET['from']) ? $_GET['from'] : '' ?>" name="from">
                </div>
                    
                <div class="input-prepend">
                    <span class="add-on">DATE TO</span>
                    <input type="text" class="input-small" value="<?=isset($_GET['to']) ? $_GET['to'] : '' ?>" name="to" >
                </div>

                <div class="input-prepend">
                    <span class="add-on">ITEMS/PAGE</span>
                    <select name="psize" class="input-small">
                        <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>
                        <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>
                        <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>
                        <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>
                        <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>
                    </select>
                </div>
                    
                <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>

            </form>
        </div>
     </div>
 
</div>

<table class="table table-hover table-bordered table-heading">
    <thead>
		<tr>
               <td>NAME</td>
               <td>EMAIL</td>
               <td>PROMO</td>
               <td>CODE</td>
               <td>TIMESTAMP</td>
		</tr>
	</thead>
		<tr>
			<?php 
        	if( $items ) : $i = 0;
        		foreach( $items as $k => $v ) : $i++;
        	?>
        	 <tr>
                <td><?=$v['name']?></td>
                <td><?=$v['email']?></td>
                <td><?=$v['promo']?></td>
                <td><?=$v['code']?></td>
                <td><?=$v['timestamp']?></td>
            </tr>
        <?php endforeach; else: ?>
          <tr>
               <td colspan="10"><center>No Result</center></td>
          </tr>
    	<?php endif;?>
          <tr>
               <td colspan="10"><h4>Total: <?=$total?></h4></td>
          </tr>
    </tbody>

</table>

<div class="pagination pull-right"><?php echo $pagination?></div>


<script type="text/javascript">
$(function(){

     $('.advanced').click(function(){
          $('.advance-search').slideToggle();
     })

     $('.deem').click(function(){
          var el = $(this);
          var data = {
               firstname : el.data('fname'),
               lastname : el.data('lname'),
               email : el.data('email'),
               fbid : el.data('fbid'),
               promo : el.data('promo')
          }

          $.post('<?=site_url()?>/ajax/promojoiner',data);
          el.parent().parent().addClass("promo-joiner");
          el.addClass('btn-danger');
     })

     $('input[name="from"]').datepicker({
          changeMonth: true,
          numberOfMonths: 1,
          dateFormat:'yy-mm-dd',
          onClose: function( selectedDate ) {
               $( 'input[name="to"]').datepicker( "option", "minDate", selectedDate );
          }
     });

     $('input[name="to"]').datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 1,
          dateFormat:'yy-mm-dd',
          onClose: function( selectedDate ) {
               $('input[name="from"]').datepicker( "option", "maxDate", selectedDate );
          }
     });
});
</script>