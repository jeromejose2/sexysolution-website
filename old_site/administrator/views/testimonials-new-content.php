<?php if( isset($testimonial[0]['id']) ) :?>
    <form method="POST" action="<?=base_url()?>admin/testimonials/update" enctype="multipart/form-data">
<?php else : ?>
    <form method="POST" action="<?=base_url()?>admin/testimonials/save" enctype="multipart/form-data">
<?php endif; ?>

    <div class="page-controls">
        <button class="btn btn-primary"><i class="icon-ok"></i> Save</span></button>
        <a class="btn btn-primary hidden-phone" onclick="window.history.back()"><i class="icon-remove"></i> Cancel</a>
    </div>

    <input type="hidden" name="id" value="<?=isset($testimonial[0]['id']) ? $testimonial[0]['id'] : ''?>">
    <table style="margin-left: 2%">
        <tr style="vertical-align: top">
           <td><h5>Photo</h5></td>
           <td style="width: 100%">
               <div class="col-sm-4">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-append">
                            <div class="uneditable-input span3">
                                <i class="glyphicon glyphicon-file fileupload-exists"></i>
                                <span class="fileupload-preview"></span>
                           </div>
                           <span class="btn btn-file">
                               <span class="fileupload-new">Select file</span>
                               <span class="fileupload-exists">Change</span>
                               <input type="file" width="30" required name="userfile" />
                           </span>
                           <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                       </div>
                   </div>
               </div>
            </td>
        </tr>
        <tr style="vertical-align: top">
            <td><h5>Name</h5></td>
            <input type="hidden" name="category" value="testimonials">
            <td style="width: 50%"><input type="text" name="title" value="<?=isset($testimonial[0]['title']) ? $testimonial[0]['title'] : ''?>"></td>
        </tr>
        <tr><td><br></td></tr>
        <tr style="vertical-align: top">
            <td><h5>Subtitle</h5></td>
            <td style="width: 50%"><input type="text" name="sub_title" value="<?=isset($testimonial[0]['sub_title']) ? $testimonial[0]['sub_title'] : ''?>"></td>
        </tr>
        <tr><td><br></td></tr>
        <tr style="vertical-align: top">
            <td><h5>Description</h5></td>
            <td style="width: 100%"><textarea id="content3" name="description" style="width: 100%"><?=isset($testimonial[0]['description']) ? $testimonial[0]['description'] : ''?></textarea></td>
        </tr>
    </table>
</form>

<script src="<?=base_url()?>js/admin/texteditor/nicEdit.js"></script>
<script type="text/javascript">
bkLib.onDomLoaded(function(){
    new nicEditor({iconsPath : '<?=base_url()?>js/admin/texteditor/nicEditorIcons.gif', buttonList : ['fontSize','bold','italic']}).panelInstance('content3');
});
</script>

<script>
    $(function(){
        <? if( isset($clinic[0]['longitude']) || isset($clinic[0]['latitude']) ) : ?>
            var myLatlng = new google.maps.LatLng(<?=$clinic[0]['latitude']?>,<?=$clinic[0]['longitude']?>);
        <? else : ?>
            var myLatlng = new google.maps.LatLng(14.59885354948353, 120.98423133572385);
        <? endif; ?>
        var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

        var infowindow = new google.maps.InfoWindow();

        var bounds = new google.maps.LatLngBounds();

        var marker = new google.maps.Marker({
              draggable: true,
              position: myLatlng, 
              map: map,
              // icon: '<?=base_url()?>images/admin/marker.png',
              title: "Sexy Solutions Clinic"
          });

        google.maps.event.addListener(marker, 'drag', function(event){
           document.getElementById("latitude").value = event.latLng.lat();
           document.getElementById("longitude").value = event.latLng.lng();
    });

        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(15);
            google.maps.event.removeListener(listener);
        });

          // google.maps.event.addDomListener(window, 'load', initialize);
    });
</script>