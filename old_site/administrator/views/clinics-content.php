<!-- BEGIN DELETE POPUP -->
<div class="modal hide fade" id="popupDelete">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><small>DELETE BRANCH</small></h4>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete this item?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-small btn-primary delete-yes" data-dismiss="modal" aria-hidden="true">YES</button>
    <button class="btn btn-small" data-dismiss="modal" aria-hidden="true">CLOSE</button>
  </div>
</div>
<!-- END DELETE POPUP -->

<div class="page-controls">

	<a href="<?=base_url()?>admin/clinics/add" class="btn btn-primary" id="new-clinic"> <i class="icon-plus"></i><span class="hidden-phone"> New</span></a>

     <!-- <a href="<?=site_url('export/registrants?'.http_build_query($_GET, '', "&"))?>" class="btn hidden-phone"><i class="icon-download-alt"></i> Export</a> -->



     <div class=" advance-search">

        <div class="form-content">

            <form class="form-search">



                <div class="input-prepend">

                    <span class="add-on">FIRST NAME</span>

                    <input type="text" class="input-small" name="firstname" value="<?=isset($_GET['firstname']) ? $_GET['firstname'] : '' ?>" >

                </div>



                <div class="input-prepend">

                    <span class="add-on">LAST NAME</span>

                    <input type="text" class="input-medium" name="lastname" value="<?=isset($_GET['lastname']) ? $_GET['lastname'] : '' ?>">

                </div>



                <div class="input-prepend">

                    <span class="add-on">EMAIL</span>

                    <input type="text" class="input-medium" name="email" value="<?=isset($_GET['email']) ? $_GET['email'] : '' ?>">

                </div>



                <div class="input-prepend">

                    <span class="add-on">DATE FROM</span>

                    <input type="text" class="input-small" value="<?=isset($_GET['from']) ? $_GET['from'] : '' ?>" name="from">

                </div>

                    

                <div class="input-prepend">

                    <span class="add-on">DATE TO</span>

                    <input type="text" class="input-small" value="<?=isset($_GET['to']) ? $_GET['to'] : '' ?>" name="to" >

                </div>



                <div class="input-prepend">

                    <span class="add-on">ITEMS/PAGE</span>

                    <select name="psize" class="input-small">

                        <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>

                        <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>

                        <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>

                        <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>

                        <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>

                    </select>

                </div>

                    

                <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>



            </form>

        </div>

     </div>

 

</div>



<table class="table table-hover table-bordered table-heading">

    <thead>

		<tr>

               <td>ADDRESS</td>

               <td>MOBILE</td>

               <td>LANDLINE</td>

               <td class="hidden-tablet hidden-phone">EMAIL</td>

               <td>BRANCH HOURS</td>

               <td style="width: 110px">ACTION</td>

		</tr>

	</thead>

		<tr>

			<?php 

        	if( $items ) : $i = 0;

        		foreach( $items as $k => $v ) : $i++;

        	?>

        	 <tr id="row-<?=$v['id']?>">

                <td><?=$v['address']?></td>

                <td><?=$v['mobile']?></td>

                <td><?=$v['landline']?></td>

                <td class="hidden-tablet hidden-phone"><?=$v['email']?></td>

                <td><?=$v['branch_hours']?></td>

                <td>

                    <a class="btn btn-small" href="<?=base_url()?>admin/clinics/edit/<?=$v['id']?>">Edit</a>

                    <a href="#popupDelete" class="btn btn-small delete" role="button" data-toggle="modal" data-id="<?=$v['id']?>" delete-id="<?=$v['id']?>">Delete</a>

                </td>

            </tr>

        <?php endforeach; else: ?>

          <tr>

               <td colspan="10"><center>No Result</center></td>

          </tr>

    	<?php endif;?>

          <tr>

               <td colspan="10"><h4>Total: <?=$total?></h4></td>

          </tr>

    </tbody>



</table>



<div class="pagination pull-right"><?php echo $pagination?></div>





<script type="text/javascript">

$(function(){


      $('.delete').on('click', function(){
          var id = $(this).data('id');
          $('.delete-yes').attr('data-id', id);
      });

      $('.delete-yes').on('click', function(){
          var id = $(this).data('id');
          $.post('<?=base_url()?>admin/clinics/delete/'+id, function(){
              $('#row-'+id).hide();
          });
      });
      // $('.delete').on('click', function(){

      //   var id = $(this).data('id');
      //   popup.open({message:'Delete this item?', top:150, type:'confirm', onConfirm:function(){
      //       $.post('<?=base_url()?>admin/clinics/delete/'+id, function( r ){
      //         if( r ) {
      //           $('#row-'+id).html('<td colspan="10"><center>No Result</center></td>');
      //         } else {
      //           $('#row-'+id).hide();
      //         }
      //     });
      //   }});

      // });



     $('.advanced').click(function(){

          $('.advance-search').slideToggle();

     })



     $('.deem').click(function(){

          var el = $(this);

          var data = {

               firstname : el.data('fname'),

               lastname : el.data('lname'),

               email : el.data('email'),

               fbid : el.data('fbid'),

               promo : el.data('promo')

          }



          $.post('<?=site_url()?>/ajax/promojoiner',data);

          el.parent().parent().addClass("promo-joiner");

          el.addClass('btn-danger');

     })



     $('input[name="from"]').datepicker({

          changeMonth: true,

          numberOfMonths: 1,

          dateFormat:'yy-mm-dd',

          onClose: function( selectedDate ) {

               $( 'input[name="to"]').datepicker( "option", "minDate", selectedDate );

          }

     });



     $('input[name="to"]').datepicker({

          defaultDate: "+1w",

          changeMonth: true,

          numberOfMonths: 1,

          dateFormat:'yy-mm-dd',

          onClose: function( selectedDate ) {

               $('input[name="from"]').datepicker( "option", "maxDate", selectedDate );

          }

     });

});

</script>