<? if( isset($sexysofan[0]['id']) ) : ?>
    <form method="POST" action="<?=base_url()?>admin/sexyso_fans/update">
<? else : ?>
    <form method="POST" action="<?=base_url()?>admin/sexyso_fans/save">
<? endif; ?>

    <div class="page-controls">
        <button class="btn btn-primary"><i class="icon-ok"></i> Save</span></button>
        <a class="btn btn-primary hidden-phone" onclick="window.history.back()"><i class="icon-remove"></i> Cancel</a>
    </div>

    <input type="hidden" name="id" value="<?=isset($sexysofan[0]['id']) ? $sexysofan[0]['id'] : ''?>">
    <table style="margin-left: 2%">
        <tr style="vertical-align: top">
            <td><h5>Name</h5></td>
            <input type="hidden" name="category" value="sexysofans">
            <td style="width: 50%"><input type="text" name="title" value="<?=isset($sexysofan[0]['title']) ? $sexysofan[0]['title'] : ''?>"></td>
        </tr>
        <tr><td><br></td></tr>
        <tr style="vertical-align: top">
            <td><h5>Description</h5></td>
            <td style="width: 100%">
                <textarea id="content1" name="description" style="width: 100%"><?=isset($sexysofan[0]['description']) ? $sexysofan[0]['description'] : ''?></textarea></td>
        </tr>
    </table>
</form>

<script src="<?=base_url()?>js/admin/texteditor/nicEdit.js"></script>
<script type="text/javascript">
bkLib.onDomLoaded(function(){
    new nicEditor({iconsPath : '<?=base_url()?>js/admin/texteditor/nicEditorIcons.gif', buttonList : ['fontSize','bold','italic']}).panelInstance('content1');
});
</script>

<script>
    $(function(){
        <? if( isset($clinic[0]['longitude']) || isset($clinic[0]['latitude']) ) : ?>
            var myLatlng = new google.maps.LatLng(<?=$clinic[0]['latitude']?>,<?=$clinic[0]['longitude']?>);
        <? else : ?>
            var myLatlng = new google.maps.LatLng(14.59885354948353, 120.98423133572385);
        <? endif; ?>
        var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

        var infowindow = new google.maps.InfoWindow();

        var bounds = new google.maps.LatLngBounds();

        var marker = new google.maps.Marker({
              draggable: true,
              position: myLatlng, 
              map: map,
              // icon: '<?=base_url()?>images/admin/marker.png',
              title: "Sexy Solutions Clinic"
          });

        google.maps.event.addListener(marker, 'drag', function(event){
           document.getElementById("latitude").value = event.latLng.lat();
           document.getElementById("longitude").value = event.latLng.lng();
    });

        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(15);
            google.maps.event.removeListener(listener);
        });

          // google.maps.event.addDomListener(window, 'load', initialize);
    });
</script>