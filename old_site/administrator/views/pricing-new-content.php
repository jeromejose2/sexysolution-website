<? if( isset($pricing[0]['id']) ) : ?>

    <form method="POST" action="<?=base_url()?>admin/pricing/update">

<? else : ?>

    <form method="POST" action="<?=base_url()?>admin/pricing/save">

<? endif; ?>

    <input type="hidden" name="id" value="<?=isset($pricing[0]['id'])?>">

    <div class="page-controls">

        <button class="btn btn-primary"><i class="icon-ok"></i> Save</span></button>

        <a class="btn btn-primary hidden-phone" onclick="window.history.back()"><i class="icon-remove"></i> Cancel</a>

    </div>



    <table style="margin-left: 2%">

        <tr style="vertical-align: top">

            <td><h5>Title</h5></td>

            <td><input type="text" class="input-large" name="title" value="<?=isset($pricing[0]['title']) ? $pricing[0]['title'] : ''?>"></td>

        </tr>



        <tr>

            <td><h5>Description</h5></td>

        </tr>

    </table>
            <div style="margin-left: 2%">
                <textarea id="description" name="description" style="width: 100%"><?=isset($pricing[0]['description']) ? $pricing[0]['description'] : ''?></textarea>
            </div>

    <table style="margin-left: 2%">

        <tr style="vertical-align: top">

            <td><h5>Services</h5></td>

    </table>
            <div style="margin-left: 2%">
                <textarea id="services" name="services" style="width: 100%"><?=isset($pricing[0]['services']) ? $pricing[0]['services'] : ''?></textarea>
            </div>

</form>



<script src="<?=base_url()?>js/admin/texteditor/nicEdit.js"></script>

<script type="text/javascript">

bkLib.onDomLoaded(function(){

  new nicEditor({iconsPath : '<?=base_url()?>js/admin/texteditor/nicEditorIcons.gif'}).panelInstance('description');

});

</script>



<script type="text/javascript">

bkLib.onDomLoaded(function(){

  new nicEditor({iconsPath : '<?=base_url()?>js/admin/texteditor/nicEditorIcons.gif'}).panelInstance('services');

});

</script>