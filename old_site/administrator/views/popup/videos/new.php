<div class="container" style="width:350px;margin:0 auto;">
<a href="javascript:void(0)" class="lytebox-close" style="right: 320px">x</a>
<div class="well">
      
    <div class="form-content" style="background: rgba(0,0,0,0);">

      <h5>New YouTube Video</h5>
      <span class="error"></span>
     <form action="<?=base_url()?>admin/videos/save" method="POST" onsubmit="return validate()">
        <div class="input-prepend">
                <span class="add-on">TITLE</span>
                <input type="text" class="input-medium required" name="name" alt="Enter video title" >
        </div>

        <div class="input-prepend">
                <span class="add-on">VIDEO URL</span>
                <input type="text" class="input-medium required" name="video_url" alt="Enter video url" >
        </div>
        <input class="btn btn-primary submit" type="submit" value="Save">
        <button style="display:none; width: 60px;" class="btn btn-primary disabled"><img style="width: 16px" src="<?=base_url()?>images/admin/preloader.gif"></button>
    </form>

   </div>
</div>
</div>

<script type="text/javascript">
function validate() {
  var error = false;
  $('.required').each(function(){
    var elem = $(this);
    var error_msg = $(this).attr('alt');
    var value = $.trim($(this).val());

    if( elem.val() == '' ) {
      error = error_msg;
      elem.focus();
      return false
    }
  });

  if( error ) {
      $('.error').html(error);
      return false;
    } else {
      return true;
    }
  return false;
}
</script>

<script type="text/javascript">
// $('.submit').on('click',function(){
//   $('.submit').hide();
//   $('.disabled').show();
// });

// $(function(){
//   $('.disabled').on('click',function(e){
//     e.preventDefault();
//   })
// });
</script>