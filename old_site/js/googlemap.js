function googleMap1() {
    var myLatLng = new google.maps.LatLng(map1Lat, map1Long);
    var mapOptions = {
            zoom: 18,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById('map1'), mapOptions);
    var infowindow = new google.maps.InfoWindow();
    var bounds = new google.maps.LatLngBounds();
    var marker = new google.maps.Marker({
            draggable: false,
            position: myLatLng,
            map: map,
            animation: false,
            title: 'Sexy Solutions Clinic'
    });

    var listener = google.maps.event.addListener(map, 'idle', function(){
        map.setZoom(18);
        google.maps.event.removeListener(listener);
    });
}
google.maps.event.addDomListener(window, 'load', googleMap1);

function googleMap2() {
    var myLatLng = new google.maps.LatLng(map2Lat, map2Long);
    var mapOptions = {
            zoom: 18,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById('map2'), mapOptions);
    var infowindow = new google.maps.InfoWindow();
    var bounds = new google.maps.LatLngBounds();
    var marker = new google.maps.Marker({
            draggable: false,
            position: myLatLng,
            map: map,
            animation: false,
            title: 'Sexy Solutions Clinic'
    });

    var listener = google.maps.event.addListener(map, 'idle', function(){
        map.setZoom(18);
        google.maps.event.removeListener(listener);
    });
}
google.maps.event.addDomListener(window, 'load', googleMap2);