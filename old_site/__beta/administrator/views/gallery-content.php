<script type="text/javascript">
     var count;
</script>
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Gallery</h3>
               <div class="actions">
                    <a href="javascript:deleteSelectedItems()" class="btn btn-primary delete-selected-items disabled">Delete Selected (<span class="count">0</span>)</a>
               </div>
          </div>


          <div class="row">
               <? if( $items ): ?>
                    <? foreach( $items as $k => $v ): ?>
                         <div class="col-sm-6 col-md-3" id="item-<?= $v['id'] ?>">
                              <div class="thumbnail">
                                   <a href="javascript:void(0)" role="button" data-toggle="modal" data-target="#edit-modal-<?= $v['id'] ?>"><img src="<?= base_url() ?>assets/admin/img/<?= $v['photo'] ?>" alt="Image"></a>
                                   <div class="caption">
                                        <h4><?= $v['title'] ?></h4>
                                        <p><?= $v['description'] ?></p>
                                        <p>
                                             <a href="javascript:void(0)" class="btn btn-small btn-primary" role="button" data-toggle="modal" data-target="#edit-modal-<?= $v['id'] ?>">Edit</a>
                                             <a href="javascript:deleteItem(<?= $v['id'] ?>)" class="btn btn-small btn-danger" role="button">Delete</a>
                                             <input type="checkbox" name="flag" data-id="<?= $v['id'] ?>">
                                        </p>
                                   </div>
                              </div>
                         </div>
                         <!-- edit modal -->
                         <div class="modal fade" id="edit-modal-<?= $v['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                   <div class="modal-content">
                                        <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                             <h4 class="modal-title" id="myLabel">Edit</h4>
                                        </div>
                                        <div class="modal-body">
                                             <!-- modal content -->
                                                  <img style="width: 100%" src="<?= base_url() ?>assets/admin/img/<?= $v['photo'] ?>">
                                             <!-- end modal content -->
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <!-- end edit modal -->
                    <? endforeach; ?>
               <? endif; ?>
          </div>


          <?= $pagination ?>

     </div>
     <!--end main content -->

     <script src="<?= base_url() ?>assets/admin/js/holder.js"></script>

<script>
     function deleteSelectedItems() {
          lytebox.dialog({message:'Are you sure you want to delete ('+count+') items?', type:confirm, onConfirm: function(){
               var checkboxes = $('input[name=flag]');
               var ids = [];
               checkboxes.each(function(){
                    var elem = $(this);
                    var id = $(this).data('id');
                    if( elem.is(':checked') ) {
                         ids.push(id);
                    }
               });
               var data = { ids : ids };
               $.post("<?= site_url('gallery/foo') ?>", data, function(response){
                    console.log(response);
               });
               for( i=0; i<ids.length; i++ ) {
                    $('#item-'+ids[i]).remove();
               }
               checkboxes.each(function(){
                    var elem = $(this);
                    if( elem.is(':checked') ) {
                         elem.prop('checked', false);
                    }
               });
               $('.count').html(0);
               $('.delete-selected-items').addClass('disabled');
          }});
     }

     $('input[name=flag]').on('change', function(){
          var hasSelected = $('input[name=flag]').is(':checked');
          var button = $('.delete-selected-items');
          if( hasSelected ) {
               if( button.hasClass('disabled') ) {
                    button.removeClass('disabled')
               }
          } else {
               if( !button.hasClass('disabled') ) {
                    button.addClass('disabled');
               }
          }
          var selected = $('.row').find('input[name=flag]:checked');
          count = selected.length;
          $('.count').html(count);
     });

     function deleteItem(id) {
          lytebox.dialog({message:'Are you sure you want to delete this item?', type:confirm, onConfirm: function(){
               var item = $('#item-'+id);
               var data = { id:id };
               $.post("<?= site_url('gallery/bar') ?>", data, function(){
                    item.remove();
               });
          }});
     }
</script>