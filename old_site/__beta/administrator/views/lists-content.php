<script>
     var count;
</script>
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>List</h3>

               <div class="actions">
                    <a href="#" class="btn btn-primary">New Item</a>
               </div>

               <div class="actions" style="margin-right: 5px">
                    <a href="javascript:deleteSelectedItems()" class="btn btn-primary delete-selected-items disabled">Delete Selected (<span class="count">0</span>)</a>
               </div>
          </div>


          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Title</th>
                         <th>Description</th>
                         <th>Action <input type="checkbox" name="select_all"></th>
                    </tr>
               </thead>
               <tbody class="row">
                    <? foreach($items as $k => $v): ?>
                         <tr id="item-<?= $v['id'] ?>">
                              <td><?= $v['id'] ?></td>
                              <td><?= $v['title'] ?></td>
                              <td><?= $v['description'] ?></td>
                              <td style="width: 10%">
                                   <a href="javascript:void(0)" style="margin-right: 10px" title="Edit" data-toggle="modal" data-target="#popup-<?php echo $v['id'];?>"><i class="glyphicon glyphicon-edit"></i></a>
                                   <a href="javascript:void(0)" style="margin-right: 10px" title="Delete" class="delete" data-id="<?php echo $v['id'];?>"><i class="glyphicon glyphicon-trash"></i></a>
                                   <input type="checkbox" name="flag" data-id="<?= $v['id'] ?>">
                              </td>
                         </tr>
                    <? endforeach; ?>
               </tbody>
          </table>

          <?= $pagination ?>

     </div>
     <!--end main content -->

<script>
     function deleteSelectedItems() {
          lytebox.dialog({message:'Are you sure you want to delete ('+count+') items?', type:confirm, onConfirm: function(){
               var checkboxes = $('input[name=flag]');
               var ids = [];
               checkboxes.each(function(){
                    var elem = $(this);
                    var id = $(this).data('id');
                    if( elem.is(':checked') ) {
                         ids.push(id);
                    }
               });
               var data = { ids : ids };
               // ajax process here...
               for( i=0; i<ids.length; i++ ) {
                    $('#item-'+ids[i]).remove();
               }
               checkboxes.each(function(){
                    var elem = $(this);
                    if( elem.is(':checked') ) {
                         elem.prop('checked', false);
                    }
               });
               $('.count').html(0);
               $('.delete-selected-items').addClass('disabled');
          }});
     }

     $('input[name=flag]').on('change', function(){
          var hasSelected = $('input[name=flag]').is(':checked');
          var button = $('.delete-selected-items');
          if( hasSelected ) {
               if( button.hasClass('disabled') ) {
                    button.removeClass('disabled')
               }
          } else {
               if( !button.hasClass('disabled') ) {
                    button.addClass('disabled');
               }
          }
          var selected = $('.row').find('input[name=flag]:checked');
          count = selected.length;
          $('.count').html(count);
     });

     $('input[name=select_all]').on('click', function(){
          var allCheckboxes = $('.row').find('input[name=flag]');
          if( $(this).is(':checked') ) {
               allCheckboxes.prop('checked', true);
          } else {
               allCheckboxes.prop('checked', false);
          }
          var all = $('.row').find('input[name=flag]:checked').length;
          var button = $('.delete-selected-items');
          if( all > 1 ) {
               button.removeClass('disabled');
          } else {
               button.addClass('disabled');
          }
          $('.count').html(all);
     });
</script>