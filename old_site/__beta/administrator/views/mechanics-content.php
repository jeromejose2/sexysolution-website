     <div class="container main-content">
          <div class="page-header">
               <h3>Mechanics</h3>
          </div>
          <form action="<?= site_url('mechanics') ?>" method="POST">
               <div class="container">
                    <textarea name="content"><?= $mechanics[0]['content'] ?></textarea>
               </div>

               <div style="padding: 25px">
                    <input type="submit" class="btn" value="Update">
               </div>
          </form>

     </div>

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/jquery-te-1.4.0.css">
<script type="text/javascript" src="<?= base_url() ?>assets/admin/js/jquery-te-1.4.0.min.js"></script>
<script type="text/javascript">
     $('textarea').jqte();
</script>