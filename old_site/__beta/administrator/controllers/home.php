<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index() {

		$this->template['content'] = $this->load->view('home', null, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}
}