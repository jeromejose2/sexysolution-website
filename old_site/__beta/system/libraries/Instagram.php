<?php

/** 
 * @class This class contains necessary methods to connect to and retrieve data from Instagram
 * @author Rexell Perez <rexellperez@gmail.com>
 * @version 1.0
 */
class Instagram
{
    private static $attempts = 0;
    /**
     * @property the access token 'thrown back' from instagram authentication
     */
    public $access_token;

    /**
     * @property the user id of the current Instagram user
     */
    public $user_id;
    
    /**
     * @property the url of the current user's profile picture
     */
    public $profilepic;

    /**
     * @property the username of the current Instagram user
     */
    public $username;

    function __construct(){
        //$this->access_token = $token;
    }

    

    /**
     * @method This method simply makes a call to the populate_user_info()
     * @return void
     */
    function initialize($client_id, $client_secret, $code, $redirect_uri)
    {
        $this->populate_user_info($client_id, $client_secret, $code, $redirect_uri);
    }

    /**
     * @method This method makes a cURL with the given URL.
     * @param string $url the url to be called
     * @param array $data an associative array to POST where key is the name and value is the value to be POSTed
     * @return string
     */
    private function exec_curl($url, $data = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if($data) {
            curl_setopt($ch, CURLOPT_POST, true);   
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    /**
     * @method This method populates the properties with their respective values.
     * @param string $client_id the application's client ID.
     * @param string $client_secret the application's client secret
     * @param string $code the code from Instagram authentication
     * @param string $redirect_uri the redirect uri of the Instagram client
     * @return void
     */
    private function populate_user_info($client_id, $client_secret, $code, $redirect_uri)
    {
        $data = array(
            'client_id'     => $client_id,
            'client_secret' => $client_secret,
            'grant_type'    => 'authorization_code',
            'redirect_uri'  => $redirect_uri,
            'code'          => $code
        );

        $output = $this->exec_curl('https://api.instagram.com/oauth/access_token', $data);
        $output = json_decode($output);
        if (!isset($output->access_token) && self::$attempts < 1) {
            self::$attempts++;
            $this->populate_user_info($client_id, $client_secret, $code, $redirect_uri);
        }
        $this->access_token = $output->access_token;
        $this->user_id = $output->user->id;
        $this->profilepic = $output->user->profile_picture;
        $this->username = $output->user->username;
    }

    

    /**
     * @method This method gets the most recent media of the current user
     * @return array
     */
    public function get_photos()
    {
        $output = $this->exec_curl('https://api.instagram.com/v1/users/' . $this->user_id . '/media/recent?access_token=' . $this->access_token);
        $output = json_decode($output); 
        return $output;
    }

    

    /**
     * @method This method retrieves data from Instagram based on the endpoint url
     * @return array
     */
    public function get_data($endpoint_url)
    {
        $output = $this->exec_curl($endpoint_url);
        $output = json_decode($output); 
        return $output;
    }

    

    /**
     * @method This method post will post a comment to a specific media
     */
    public function post_comment($media, $token, $comment)
    {
        $post = array( 'text'           => $comment,
                       'access_token'   => $token);
        $output = $this->exec_curl('https://api.instagram.com/v1/media/' . $media . '/comments', $post);
        return $output;
    }

    

    /**
     * @method This method like a specific media
     */
    public function like($media, $token)
    {
        $post = array('access_token'    => $token);
        $output = $this->exec_curl('https://api.instagram.com/v1/media/' . $media . '/likes', $post);
        return $output;
    }
}