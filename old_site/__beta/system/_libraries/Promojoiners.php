<?php

class Promojoiners
{
    const API_URL = 'https://nwshare.ph/nuworks/promo-joiners/admin/validate/get_ids';
	private $_curl;
    
	public function __construct() {
    
		$this->_curl = curl_init();
	}

	public function update_array($records, $fbid_fieldname = 'fbid') {
		if($records) {
			$joiners = $this->promo_joiners();
			foreach($records as $k => $v) {
				if(in_array($v[$fbid_fieldname], $joiners)) {
					$records[$k]['joiner'] = 1;
				} else {
					$records[$k]['joiner'] = 0;
				}
			}
			return $records;
		}
			
	}
	
	private function promo_joiners() {
		curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->_curl, CURLOPT_URL, self::API_URL);
		$result = curl_exec ($this->_curl);
		curl_close ($this->_curl);	
		return json_decode($result);
	}

}
?>