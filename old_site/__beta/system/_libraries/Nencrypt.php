<?php 
/** 
* @class This class is used to encrypt or decrypt inputs with a secret key
*/
class Nencrypt {
	/** 
	* @property secret key used for encryption
	*/
	var $secret_key;
	
	/** 
	* @method This method initializes the $secret_key property
	*/
	function __construct($key) {
		$this->secret_key = $key[0];
	}
	
	/** 
	* @method This method encrypts a given string
	* @param string $str_data string to be encrypted
	* @return string
	*/
	public function nwencrypt($str_data){
		$str_result = '';
		$key = $this->secret_key;
		for($i=0;$i<strlen($str_data);$i++){
			$str_char    = substr($str_data, $i, 1);
			$key_char = substr($key, ($i % strlen($key)) - 1, 1);
			$str_char    = chr(ord($str_char) + ord($key_char));
			$str_result .= $str_char;
		}
		return $this->encode_base64($str_result);
	}
	
	/** 
	* @method This method decrypts a given string
	* @param string $str_data string to be decrypted
	* @return string
	*/
	
	public function nwdecrypt($str_data){
		$str_result = '';
		$str_data   = $this->decode_base64($str_data);
		$key = $this->secret_key;
		for($i=0;$i<strlen($str_data);$i++){
			$str_char    = substr($str_data, $i, 1);
			$key_char = substr($key, ($i % strlen($key)) - 1, 1);
			$str_char    = chr(ord($str_char) - ord($key_char));
			$str_result .= $str_char;
		}
		return $str_result;
	} 
	
	/** 
	* @method This method encodes a given string to its base64 encoding equivalent
	* @param string $str_data string to be decrypted
	* @return string
	*/
	
	private function encode_base64($str_data){
		$base_64 = base64_encode($str_data);
		return strtr($base_64, '+/', '-_');
	}
	
	/** 
	* @method This method decodes a base64-encoded string to its original value
	* @param string $str_data string to be decrypted
	* @return string
	*/
	private function decode_base64($str_data){
		$base_64 = strtr($str_data, '-_', '+/');
		return base64_decode($base_64);
	}
}
?>