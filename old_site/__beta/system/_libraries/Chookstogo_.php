<?php



/**

 * 

 * @author anthony

 * 

 */

class Chookstogo

{

    /**

     * 

     * @var string

     */

    const API_URL = 'http://web.chookstogo.com.ph/api/ctg/59ba2f9aad9e5a0b6d3ba3223b923bea720b2c6b/';



    /**

     * 

     * @var string

     */

    const DB_USER = 'nwshare_bounty';



    /**

     * 

     * @var string

     */

    const DB_PASS = 'P$Th~F0mBsTf';



    /**

     * 

     * @var string

     */

    const DB_NAME = 'nwshare_bountychooks';



    /**

     * 

     * @var string

     */

    const DB_TABLE = 'tbl_registrants_ctg';
	
	
	
	/**

     * 

     * @var string

     */

    const DB_APP_REGISTRANTS = 'tbl_app_registrants';
	
	

	/**

     * 

     * @var string

     */

    const DB_VIEW = 'vw_ctg_registrants';



    /**

     * 

     * @var string

     */

    const DB_HOST = 'localhost';



    /**

     * 

     * @var resource

     */

    private $_curl;



    /**

     * 

     * @var string

     */

    private $_app_name;



    /**

     * 

     * @var PDO

     */

    private $_db;



    /**

     * 

     * @param string $app_name

     */

    public function __construct($app_name)

    {

        if(!session_id()){

            session_start();

        }

        $app_name = end($app_name);

        if(!isset($_SESSION[$app_name])){

            $_SESSION['APP' . $app_name] = array();

        }

        $this->_app_name = $app_name;

        $this->_curl = curl_init();

        $this->_db = new PDO(

            'mysql:host='.self::DB_HOST.';dbname='.self::DB_NAME,

            self::DB_USER,

            self::DB_PASS,

            array(

                PDO::ATTR_PERSISTENT => true

            )

        );

        $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    }



    /**

     * Login method

     * Password must be encrypted first before passing as argument

     * to this method

     * @param string $username

     * @param string $password

     * @return boolean|array

     */

    public function login($email, $password = null)

    {

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){

            return false;

        }

        if($password !== null){

            $login = json_encode(array(

                'user' => $email,

                'pass' => $password

            ));

            curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($this->_curl, CURLOPT_URL, self::API_URL.'auth/'.$login);

            // debug(self::API_URL.'auth/'.$login);

            $response = json_decode(curl_exec($this->_curl), true);

            if(curl_error($this->_curl)){

                return curl_error($this->_curl);

            }

            if(!$response){

                return false;

            }

            // debug($response);

        }

        

        $stmt = $this->_db->prepare("SELECT * FROM ".self::DB_TABLE." WHERE email = :email AND password = :password");

        $stmt->bindParam(":email", $email, PDO::PARAM_STR);

        $stmt->bindParam(":password", $password, PDO::PARAM_STR);

        $stmt->execute();

        if($user = $stmt->fetch(PDO::FETCH_ASSOC)){

            $this->_set_session($user);

            return $user;

        } else {

            $this->_set_session($response);

            return $response;

        }

    }



    /**

     * 

     * @param string $id

     * @return array|false

     */

    public function login_fbid($id)

    {

        $stmt = $this->_db->prepare("SELECT * FROM ".self::DB_TABLE." WHERE fbid = :fbid");

        $stmt->bindParam(":fbid", $id, PDO::PARAM_STR);

        $stmt->execute();

        if($user = $stmt->fetch(PDO::FETCH_ASSOC)){

            $this->_set_session($user);

            return $user;

        }

        return false;

    }



     public function valid_fbid($id)

    {

        $stmt = $this->_db->prepare("SELECT * FROM ".self::DB_TABLE." WHERE fbid = :fbid");

        $stmt->bindParam(":fbid", $id, PDO::PARAM_STR);

        $stmt->execute();

        if($user = $stmt->fetch(PDO::FETCH_ASSOC)){

            return true;

        }

        return false;

    }



    /**

     * 

     * @param array $data

     * @return void

     */

    private function _set_session($data)

    {

        $_SESSION[$this->_app_name] = $data;

    }



    /**

     * 

     * @param string $email

     * @return boolean

     */

    public function is_logged_in($username = null)

    {

        return !empty($_SESSION[$this->_app_name]);

    }



    /**

     * 

     * @param array $data

     * @param string $error_message

     * @return boolean

     */

    public function register($data, &$error_message = null)

    {

        $fbid = null;

		

		/*foreach($data as $k => $v) {

			if($k != 'email')

				$data[$k] = urlencode($v);

		}*/

		

		if(isset($data['fbid'])){

            $fbid = $data['fbid'];

            unset($data['fbid']);

        }

        if(empty($data['lname'])){

            $error_message = "Last name is empty";

            return false;

        } else if(empty($data['fname'])){

            $error_message = "First name is empty";

            return false;

        } else if(empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)){

            $error_message = "Invalid email";

            return false;

        } else if(empty($data['mobile'])){

            $error_message = "Invalid mobile number";

            return false;

        } else if(empty($data['password'])){

            $error_message = "Password is empty";

            return false;

        } else if(empty($data['gender'])){

            $error_message = "Gender is empty";

            return false;

        } else if(empty($data['bday'])){

            $error_message = "Bday is empty";

            return false;

        } else if(empty($data['province'])){

            $error_message = "Province is empty";

            return false;

        } else if(empty($data['country'])){

            $error_message = "Country is empty";

            return false;

        } else if(empty($fbid)){

            $error_message = "FBID is required";

            return false;

        }

        $password = $data['password'];

        unset($data['password']);

        $data['pass'] = $password;

		//$data['email'] = urlencode($data['email']);

        $ret_url = self::API_URL.'add_mem/'.urlencode(json_encode($data));

		

		// debug($ret_url);

        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($this->_curl, CURLOPT_URL, $ret_url);

        $response = json_decode(curl_exec($this->_curl), true);

        if(curl_error($this->_curl)){

            $error_message = curl_error($this->_curl);

            return false;

        }

        if(!empty($response['error'])){
            $error_message = $response['remarks'];
            return false;
        }

		

		if(!$this->add_to_app($fbid, $err_adding)){
            $error_message = $err_adding;
            return false;
            //return true;
        }

			

		

        // debug($response, true); 

        $data['fbid'] = $fbid;

        $data['date_registered'] = date('Y-m-d H:i:s');

        unset($data['pass']);

        $data['password'] = $password;

		$data['appname'] = $this->_app_name;

        foreach($data as &$val){

            $val = $this->_db->quote($val);

        }

        $stmt = $this->_db->prepare("INSERT INTO " . self::DB_TABLE . "(".implode(',', array_keys($data)).") VALUES (".implode(',', $data).")");

        try {

            $stmt->execute();

        } catch(PDOException $ex){

            $error_message = $ex->getMessage();

            return false;

        }

        $error = $stmt->errorInfo();

        if(!empty($error[1]) || !empty($error[2])){

            $error_message = $error[1].' : '.$error[2];

            return false;

        }

        if($stmt->rowCount() !== 1){

            return false;

        }

        return $this->_db->lastInsertId();

    }



    /**

     * 

     * @return void

     */

    public function logout()

    {

        $_SESSION[$this->_app_name] = array();

    }



    /**

     * 

     * @return array|null

     */

    public function get_user()

    {

        return !empty($_SESSION[$this->_app_name]) ? $_SESSION[$this->_app_name] : null;

    }



    /**

     * 0 - invalid

     * 1 - valid

     * @param string $email

     * @return int

     */

    public function valid_email($email, &$from_chic_moms = false)

    {

        $json = json_encode(array(

            'email' => $email

        ));

        curl_setopt($this->_curl, CURLOPT_URL, self::API_URL.'valid_email/'.urlencode($json));

        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);

        $validFromChics = (int) curl_exec($this->_curl);



        $stmt = $this->_db->prepare("SELECT COUNT(*) AS `total` FROM ".self::DB_TABLE." WHERE email = :email");

        $stmt->bindParam(":email", $email, PDO::PARAM_STR);

        $valid = null;

        try {

            $stmt->execute();

            if($row = $stmt->fetch(PDO::FETCH_ASSOC)){

                $valid = (int) $row['total'];

            }

        } catch(PDOException $ex) {

            return false;

        }

        $error = $stmt->errorInfo();

        if(!empty($error[1]) || !empty($error[2])){

            $error_message = $error[1].' : '.$error[2];

            return false;

        }

        if(!$valid && $validFromChics){

            $from_chic_moms = true;

        } else if($valid && $validFromChics){

            $from_chic_moms = false;

        }

        return $valid || $validFromChics;

    }



    /**

     * 0 - invalid

     * 1 - valid

     * @param string $mobile

     * @return int

     */

    public function is_registered_mobile($mobile)

    {

        $json = json_encode(array(

            'mobile' => $mobile

        ));

        curl_setopt($this->_curl, CURLOPT_URL, self::API_URL.'valid_mobile/'.urlencode($json));

        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);

        return (int) curl_exec($this->_curl);

    }



    public function get_users($condition = '', $limit = 30, $page = 1)

    {

        $results = array('rows' => null, 'total' => 0);

        try {

            if($condition){

                $condition = ' AND '.$condition;

            }

		    $stmt = $this->_db->prepare("SELECT COUNT(*) AS `total` FROM ".self::DB_VIEW." WHERE 1 {$condition}");

            $stmt->execute();

            $error = $stmt->errorInfo();

            if(!empty($error[1]) || !empty($error[2])){

                show_error($error[2]);

            }

            $results['total'] = $stmt->fetch(PDO::FETCH_ASSOC);

            $results['total'] = (int) $results['total']['total'];

            $page = $limit * (int) ($page - 1);

            $stmt = null;

            $stmt = $this->_db->prepare("SELECT * FROM ".self::DB_VIEW." WHERE 1 {$condition} ORDER BY date_registered DESC LIMIT {$limit} OFFSET {$page}");

            $stmt->execute();

            $error = $stmt->errorInfo();

            if(!empty($error[1]) || !empty($error[2])){

                show_error($error[2]);

            }

            $results['rows'] = $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch(PDOException $ex) {

            show_error($ex->getMessage());

        }

        return $results;

    }



    public function import($data)

    {

        foreach($data as &$val){

            $val = $this->_db->quote($val);

        }

        $stmt = $this->_db->prepare("INSERT INTO " . self::DB_TABLE . "(".implode(',', array_keys($data)).") VALUES (".implode(',', $data).")");

        try {

            $stmt->execute();

        } catch(PDOException $ex){

            $error_message = $ex->getMessage();

            return false;

        }

        $error = $stmt->errorInfo();

        if(!empty($error[1]) || !empty($error[2])){

            $error_message = $error[1].' : '.$error[2];

            return false;

        }

        if($stmt->rowCount() !== 1){

            return false;

        }

        return $this->_db->lastInsertId();

    }

	

	public function add_to_app($fbid, &$error_message = null) {

		if($this->check_app_registrant($fbid)) {
            // echo "check";
			$error_message = "Multiple registration detected";
			return false;

		}

		// insert to tbl_app_registrants

		$stmt = $this->_db->prepare("INSERT INTO " . self::DB_APP_REGISTRANTS . "(app_id, fbid) VALUES (" . $this->_app_name . ", :fbid)");
        $stmt->bindValue(":fbid", $fbid);

        try {

            $stmt->execute();

        } catch(PDOException $ex){

            $error_message = $ex->getMessage();

            return false;

        }

        $error = $stmt->errorInfo();

        if(!empty($error[1]) || !empty($error[2])){

            $error_message = $error[1].' : '.$error[2];

            return false;

        }

        if($stmt->rowCount() !== 1){

            return false;

        }
		
		return true;

	}

	

	private function check_app_registrant($fbid) {

		try {

			$stmt = $this->_db->prepare("SELECT * FROM " . self::DB_APP_REGISTRANTS . " WHERE app_id = :app_id AND fbid = :fbid");

			$stmt->bindValue(":app_id", $this->_app_name, PDO::PARAM_INT);

			$stmt->bindValue(":fbid", $fbid, PDO::PARAM_STR);

			$stmt->execute();

			$error = $stmt->errorInfo();

			if(!empty($error[1]) || !empty($error[2])){

				show_error($error[2]);

			}

			

			if($stmt->fetch(PDO::FETCH_ASSOC)) {

				return true; 

			}

			else return false;

			

		} catch(PDOException $ex){

            $error_message = $ex->getMessage();
			return false;

        }

		

	}

	

	public function auto_register($data, &$error = null) {

		if($this->valid_fbid($data['fbid'])) {
 			$error = 'Multiple id detected';
			return false;
		}
		$this->add_to_app($data['fbid']);

		$data['appname'] = $this->_app_name;

		$data['date_registered'] = date('Y-m-d H:i:s');

        foreach($data as &$val){

            $val = $this->_db->quote($val);

        }

        $stmt = $this->_db->prepare("INSERT INTO tbl_registrants_ctg(".implode(',', array_keys($data)).") VALUES (".implode(',', $data).")");

        try {

            $stmt->execute();

        } catch(PDOException $ex){

            $error = $ex->getMessage();

            return false;

        }

        $error = $stmt->errorInfo();

        if(!empty($error[1]) || !empty($error[2])){

            $error = $error[1].' : '.$error[2];

            return false;

        }

        if($stmt->rowCount() !== 1){
			
            return false;

        }

        return $this->_db->lastInsertId();



	}
	
	public function get_user_data($email) {
		$login = json_encode(array(
				 'email' => $email));

            curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->_curl, CURLOPT_URL, self::API_URL.'get_info/'.$login);
            // debug(self::API_URL.'auth/'.$login);
            $response = json_decode(curl_exec($this->_curl), true);
            if(curl_error($this->_curl)){
				return curl_error($this->_curl);
            }
            if(!$response){
				return false;
            }
			
			return $response;
	}
	
	
	public function delete_registrant($fbid) {
		$stmt = $this->_db->prepare("DELETE FROM " . self::DB_APP_REGISTRANTS . " WHERE fbid = :fbid");
		$stmt->bindValue(":fbid", $fbid, PDO::PARAM_STR);
		$stmt->execute();
		
		$stmt = $this->_db->prepare("DELETE FROM " . self::DB_TABLE . " WHERE fbid = :fbid");
		$stmt->bindValue(":fbid", $fbid, PDO::PARAM_STR);
		$stmt->execute();

	}

    

    

    // public function register() {

    //     $friends = array();

    //     $prize = $this->get_prize();

    //     for($i = 0; $i < 5; $i++) {

    //         $photo = $this->get_photo();

    //         $friends[] = array('fbid'   => $_POST['fbid'][$i],

    //                           'fb_name' => $_POST['fbname'][$i]);

    //         $this->globals->post_to_wall($_POST['fbid'][$i], $photo, $prize);

    //     }

        

    //     $friends_obj = serialize($friends);

        

    //     $ret = false;

    //     if($_POST['registered'] == 0) {

    //         $ret_url = 'http://www.chicmomsclub.com/api/cmc/d48170846c1e937640ae7e9f6743089c3c2a9074/add_mem/%7B%22lname%22:%22' . urlencode($_POST['last-name']) . '%22,%22fname%22:%20%22' . urlencode($_POST['first-name']) . '%22,%22email%22:%20%22' . urlencode($_POST['email']) . '%22,%22mobile%22:%20%22' . urlencode($_POST['network-prefix']) . urlencode($_POST['mobile-number']) . '%22,%22pass%22:%20%22' . md5($_POST['password']) . '%22,%22gender%22:%20%22' . urlencode($_POST['gender']) . '%22,%22bday%22:%20%22' . $_POST['year'] . '-' . $_POST['month'] . '-' . $_POST['day'] . '%22,%22province%22:%20%22' . $_POST['province'] . '%22,%22country%22:%20%22Philippines%22%7D';

            

    //         $curl = curl_init();

    //         curl_setopt ($curl, CURLOPT_URL, $ret_url);

    //         curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    //         $result = curl_exec ($curl);

    //         #close

    //         curl_close ($curl);

    //         $ret = json_decode($result);

    //     } else {

    //         $registrant = $this->is_registered_email($_POST['email']);

    //         if($registrant) 

    //             $ret = array('error'    => 1,

    //                          'remarks'  => 'Sorry, the email address you entered is already in use');

    //         else

    //             $ret = array('success'  => 1);

    //         $ret = json_decode(json_encode($ret));  

    //     }

    //     if(!isset($ret->error))

    //         $this->save_registrant($friends_obj);

    //     else {

    //         $post = json_encode($_POST);

    //         $fp = fopen('errors.txt', 'a+');

    //         $ret_url = 'http://www.chicmomsclub.com/api/cmc/d48170846c1e937640ae7e9f6743089c3c2a9074/add_mem/%7B%22lname%22:%22' . $_POST['last-name'] . '%22,%22fname%22:%20%22' . $_POST['first-name'] . '%22,%22email%22:%20%22' . $_POST['email'] . '%22,%22mobile%22:%20%22' . $_POST['network-prefix'] . $_POST['mobile-number'] . '%22,%22pass%22:%20%22' . md5($_POST['password']) . '%22,%22gender%22:%20%22' . $_POST['gender'] . '%22,%22bday%22:%20%22' . $_POST['year'] . '-' . $_POST['month'] . '-' . $_POST['day'] . '%22,%22province%22:%20%22' . $_POST['province'] . '%22,%22country%22:%20%22Philippines%22%7D';

    //         fwrite($fp, date('Y-m-d H:i:s') . ": [" . $post . "] \n\n" . $ret_url . "\n\n"  . $ret->remarks . " "   . "\n\n\n");

    //         fclose($fp);    

    //     }

    //     $data = array('response' => $ret);

    //     $this->load->view('login-content', $data, FALSE);

        

    // }

    

    // public function register2() {

    //     $friends = array();

    //     $prize = $this->get_prize();

    //     for($i = 0; $i < 5; $i++) {

    //         $photo = $this->get_photo();

    //         $friends[] = array('fbid'   => $_POST['fbid'][$i],

    //                           'fb_name' => $_POST['fbname'][$i]);

    //         $this->globals->post_to_wall($_POST['fbid'][$i], $photo, $prize);

    //     }

        

    //     $friends_obj = serialize($friends);

        

    //     $this->save_registrant($friends_obj);

    //     $ret = array('success'  => 1);

    //     $ret = json_decode(json_encode($ret));  

    //     $data = array('response' => $ret);

    //     $this->load->view('login-content', $data, FALSE);

        

    // }

    

    // private function is_registered_email($email) {

    //     $params = array('table' => 'tbl_registrants_ctg',

    //                         'where' => 'email = \'' . $email . '\'');

    //     $registrant = $this->mysql_queries->get_data($params);

    //     return $registrant;

    // }

    

    // private function save_registrant($friends_obj) {

        

    //     $params = array('table' => 'tbl_registrants_ctg',

    //                     'where' => 'fbid = ' . Globals::$fb_userid);

    //     $registrant = $this->mysql_queries->get_data($params);

    //     $registrant = @$registrant[0];

    //     $post = array();

    //     if($registrant) {

    //         $post = array('fbid'        => Globals::$fb_userid,

    //                       'first_name'  => $registrant['first_name'],

    //                       'last_name'   => $registrant['last_name'],

    //                       'gender'      => $registrant['gender'],

    //                       'email'       => $registrant['email'],

    //                       'birthday'    => $registrant['birthday'],

    //                       'mobile'      => $registrant['mobile'],

    //                       'address'     => $registrant['address'],

    //                       'friends'     => $friends_obj,

    //                       'is_existing' => $registrant['is_existing']);

    //     } else {

    //         $post = array('fbid'    => Globals::$fb_userid,

    //                       'first_name'  => $_POST['first-name'],

    //                       'last_name'   => $_POST['last-name'],

    //                       'gender'      => $_POST['gender'],

    //                       'email'       => $_POST['email'],

    //                       'birthday'    => $_POST['year'] . '-' . $_POST['month'] . '-' . $_POST['day'],

    //                       'mobile'      => $_POST['network-prefix'] . '-' . $_POST['mobile-number'],

    //                       'address'     => $_POST['province'],

    //                       'friends'     => $friends_obj,

    //                       'is_existing' => $_POST['registered']);

    //     }

    //     $params = array('table' => 'tbl_registrants_ctg',

    //                     'post'  => $post);

    //     $this->mysql_queries->insert_data($params);     

    // }

    

    // public function validate($type) {

    //     if($type == 'email') {

    //         $ret = false;

    //         if($_POST['registered'] == 0) {

    //             $ret = file_get_contents('http://www.chicmomsclub.com/api/cmc/d48170846c1e937640ae7e9f6743089c3c2a9074/valid_email/{"email":"' . $_POST['email'] . '"}');

    //             $ret = json_decode($ret);

    //         }

    //         $data = array('response' => $ret);

    //         $this->load->view('validation-content', $data, FALSE);

    //     } else {

    //         $ret = file_get_contents('http://www.chicmomsclub.com/api/cmc/d48170846c1e937640ae7e9f6743089c3c2a9074/valid_mobile/{"email":"' . $_POST['mobile'] . '"}');

    //         $ret = json_decode($ret);

    //         $data = array('response' => $ret);

    //         $this->load->view('validation-content', $data, FALSE);

    //     }

    // }

        

    // private function get_photo() {

    //     # get week

    //     $week = $this->mysql_queries->get_week();

        

    //     # slideshow

    //     $params = array('table' => 'tbl_photos p, tbl_prizes pr',

    //                     'where' => 'p.prize_id = pr.prize_id AND pr.week = ' . $week);

    //     $slideshow = $this->mysql_queries->get_data($params);

    //     $index = rand(0, count($slideshow) - 1);

    //     return base_url() . 'images/prizes/' . @$slideshow[$index]['photo'];

    // }

    

    // private function get_prize() {

    //     # get week

    //     $week = $this->mysql_queries->get_week();

        

    //     # slideshow

    //     $params = array('table' => 'tbl_prizes',

    //                     'where' => 'week = ' . $week);

    //     $slideshow = $this->mysql_queries->get_data($params);

    //     return @$slideshow[0]['name'];

    // }

    

}



?>