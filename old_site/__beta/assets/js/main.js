$(function () {
	preventDefault()
	popup()
	navScroll();

	// Parallax Effect
	if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
	    skrollr.init({
	        forceHeight: false
	    });
	}

	// FitVids
    $(".video_responsive").fitVids();


    $('#mininav').on('click',function(){
    	$('.navigation').slideToggle(200)
    	$('.navigation').toggleClass('active')
    })

    $('.navigation a').on('click',function(){
    	if($(window).width()<=768){
	    	$('.navigation').slideToggle(200)
	    	$('.navigation').toggleClass('active')
    	}
    })  
    
    if($(window).width()>=768){
	    $('.slider').slimScroll({
		    width: '100%',
		    height: '400px',
		    size: '8px',
		    color: '#e66ab0',
		    alwaysVisible: true,
		    distance: '0',
		    railVisible: true,
		    railColor: '#671242',
		    railOpacity: 1,
		    wheelStep: 10,
		    allowPageScroll: false,
		    disableFadeOut: false
		}); 
		$('.tweet-feed ul').slimScroll({
		    width: '100%',
		    height: '440px',
		    size: '8px',
		    color: '#e66ab0',
		    alwaysVisible: true,
		    distance: '0',
		    railVisible: true,
		    railColor: '#671242',
		    railOpacity: 1,
		    wheelStep: 10,
		    allowPageScroll: false,
		    disableFadeOut: false
		});  
	}
})


function preventDefault() {
	// Prevent Default
	$('a').on('click',function(event){
		event.preventDefault()
	})
}

function popup() {
	//  Hide
	$('#popup-video, #popup-full-mechanics').hide()

	// Close
	$('.close').on('click',function(){
		$(this).parents('.popup').parent().hide()
	})

	$('.see-video').on('click',function(){
		$('#popup-video').show()
	})

	$('.show_full_mechanics').on('click',function(){
		$('#popup-full-mechanics').show()
	})
}

function navScroll() {

    var links = $('.navigation a[href^="#"]')
    var linksA = $('.navigation a[href^="#"]')
    var scrollables = $('html, body')

    var homeSection = $('#home').offset().top
    var prizesSection = $('#prizes').offset().top
    var mechanicsSection = $('#mechanics').offset().top
    var productSection = $('#product').offset().top

    function doScroll(e) {
    	var link = $(this)
        var id = $(this).attr('href')
        var top = $(id).offset().top - ($('header.main').height()+25);

        e.preventDefault()

        scrollables.animate({
            scrollTop: top
        })

    }	

    $(window).scroll(function() {
    	var winPos = $(window).scrollTop(),
    		homeHeight = $('#home').height(),
			videoBtn = $('.see-video-again'),
			homeEnd = $('#home').offset().top + $('#home').height() - 200,
			galleryEnd = $('#gallery').offset().top + $('#gallery').height() - 200,
			prizesEnd = $('#prizes').offset().top + $('#prizes').height() - 200,
			mechanicsEnd = $('#mechanics').offset().top + $('#mechanics').height() - 200,
			productEnd = $('#product').offset().top + $('#product').height() - 200


		// Nav Active
		if(winPos < homeEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
        	$('.navigation a[href^="#home"]').addClass('active')
		} else if(winPos > homeEnd && winPos < galleryEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
		} else if(winPos > galleryEnd && winPos < prizesEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
			$('.navigation a[href^="#prizes"]').addClass('active')
		} else if(winPos > prizesEnd && winPos < mechanicsEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
			$('.navigation a[href^="#mechanics"]').addClass('active')
		} else if(winPos > mechanicsEnd && winPos < productEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
			$('.navigation a[href^="#product"]').addClass('active')
		}

    })
    links.on('click', doScroll)
}