<!DOCTYPE html>
<!--[if IE 8]><html class="ie8"><![endif]-->
<!--[if IE 9]><html class="ie9"><![endif]-->
<!--[if gt IE 9]><!--><html><!--<![endif]-->
<head>
    <title>Belo Essentials</title>	
    <meta charset="UTF-8" />    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Belo Essentials" />
    <meta name="keywords" content="belo essentials, essentials product, beauty product, cosmetic, body products, face product" />
    <!--[if lt IE 9]><script src="<?= base_url() ?>assets/js/vendor/html5.js"></script><![endif]-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/main.css?v=1" />
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=1543084259246162&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
</head>

<body id="skrollr-body">

<header class="curvy"></header>
<header class="main">
    <div class="container">
        <h1 class="logo from-sprites"><a href="#">Belo Essentials Get Beautyful #ANNEderarms</a></h1>
        <a href="#" id="mininav">Mini Nav</a>
        <nav class="navigation">
            <a href="#home" >Home</a>
            <a href="#mechanics"><span>Mechanics</span></a>
            <span class="logo-space">Logo Space</span>
            <a href="#prizes">Prizes</a>
            <a href="#product"><span>Products</span></a>
        </nav>
        <div class="socials">
            <span>Beauty loves company,<br>so follow us online!</span>
            <a href="#" class="fb from-sprites">Facebook</a>
            <a href="#" class="tw from-sprites">Twitter</a>
            <a href="#" class="in from-sprites">Instagram</a>
        </div>
    </div>
    <button class="see-video">
        CLICK HERE TO VIEW VIDEO
    </button>
</header>

<section class="main">
    <section id="home" class="clearfix" data-0="background-position:0px 0px;" data-100000="background-position:0px -50000px;">
        <div class="container">
            <div class="home-details">
                <div class="how-to">
                    <div class="ribbon">HOW TO JOIN</div>
                    <img src="<?= base_url() ?>assets/img/home-title.png">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                    <br>
                    
                    <a href="https://twitter.com/Belo_Essentials" class="twitter-follow-button" data-show-count="false" data-lang="en"  data-size="large">Follow @Belo_Essentials</a>
                    
                    <br>
                    
                    <a href="#" class="btn btn-blue btn-tweet" onclick="twitter()"><span></span> TWEET TO JOIN</a>
                </div>
                <div class="tweet-feed">
                    <div class="ribbon">LIVE FEED TWEETS</div>
                    <ul id="tweet-start-feed">
                        <!-- TWEET FEEDS -->
                        <?php foreach( $entries as $k => $v ): ?>
                            <li>
                                <div class="thumb"><img src="<?php echo $v['user_image'] ?>"></div>
                                <span class="name"><?php echo $v['full_name'] ?> <em>@<?php echo $v['username'] ?></em></span>
                                <span class="date"><?php echo date('m/d/y', strtotime($v['created_at'])) ?></span>
                                <span class="tweet"><?php echo $v['caption'] ?></span>
                            </li>
                        <?php endforeach; ?>
                        <!-- END TWEET FEEDS -->
                    </ul>

                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </section>

    <section id="mechanics" class="clearfix" data-0="background-position:0px 0px;" data-100000="background-position:0px -50000px;">
        
        <div class="container">
         
            <div class="mechanics-details">
                <div class="left">
                    <img src="<?= base_url() ?>assets/img/twitter-logo.png"> <br><br>
                    <img src="<?= base_url() ?>assets/img/kilikiliglines.png">

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>

                    <div class="text-center">
                        <img src="<?= base_url() ?>assets/img/beauty-deo.png"> 
                    </div>
                </div>
                
                <div class="right">
                    <ul class="caveman-mechanics">
                        <li>
                            <i>1</i>
                            <div class="copy">
                                <p>Follow @Belo_Essentials on Twitter
                                first or your tweets won't be part of the
                                #Annederarms #KiliKiligLines!</p>

                                <a href="https://twitter.com/Belo_Essentials" class="twitter-follow-button" data-show-count="false" data-lang="en"  data-size="large">Follow @Belo_Essentials</a>

                            </div>
                        </li>

                        <li>
                            <i>2</i>
                            <div class="copy">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>

                                <a href="#" class="btn btn-blue btn-tweet" onclick="twitter()"><span></span> TWEET TO JOIN</a>
                            </div>
                        </li>

                        <li>
                            <i>3</i>
                            <div class="copy">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>

                                <a href="#" class="show_full_mechanics">VIEW FULL MECHANICS</a>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="clearfix"></div>
            </div>
                     
            
        </div>
    </section>
    
    <section id="prizes" class="clearfix" data-0="background-position:0px 0px;" data-100000="background-position:0px -50000px;">
        <div class="container">
            <h2>At the end of the promotion, will select 10 (TEN) winners from all the entries submitted for the promotion. 
                The announcement date will be on September 1-7, 2014 which will be posted on  the following Belo social media accounts:</h2>
            <div class="content">

                <div class="sharer">
                    <button class="fb"></button>
                    <button class="tw"></button>
                    <button class="ig"></button>
                </div>
                
                <ul class="prizes clearfix">
                    <li>
                        <figure><img src="<?= base_url() ?>assets/img/prize-1.png" alt="3-day trip to Boracay for four"></figure>
                        Entries will be featured on a Belo digital billboard along
                        EDSA (Guadalupe) from September 7-13, 2014.
                    </li>
                    <li>
                        <figure><img src="<?= base_url() ?>assets/img/prize-2.png" alt="up to four Underarm Hair Removal sessions by Belo Medical Group"></figure>
                        #ANNEderarm tank top
                    </li>
                    <li>
                        <figure><img src="<?= base_url() ?>assets/img/prize-3.png" alt="one RevLite session by Belo Medical Group"></figure>
                        #ANNEderarm product kit
                    </li>
                </ul>

                
            </div>

            
        </div><!-- .container -->
    </section><!-- #prizes -->



    <section id="product" class="clearfix" data-0="background-position:0px 0px;" data-end="background-position:0px -5000px;">
        <div class="container">
            <h3>Say good bye to your underarm problems with Belo Essentials Beauty Deo!</h3>

            <ul class="skin">
                <li><img src="<?= base_url() ?>assets/img/skin-sweaty.png"> <span>SWEATY</span></li>
                <li><img src="<?= base_url() ?>assets/img/skin-rough.png"> <span>ROUGH</span></li>
                <li><img src="<?= base_url() ?>assets/img/skin-chicken.png"> <span>CHICKEN SKIN</span></li>
                <li><img src="<?= base_url() ?>assets/img/skin-dark.png"> <span>DARK</span></li>
                <li><img src="<?= base_url() ?>assets/img/skin-red.png"> <span>RED</span></li>
            </ul>
            
            <div class="product-container">
                <div class="bubble-box">
                    Aptly called "The Beauty Deo," this product comes as a roll-on and quick-drying spray. With regular use, it rids you of darkness, redness, roughness, sweatiness, and chicken skin to give you absolutely beautiful #ANNEderarms.
                </div>

                <div class="product-deo">
                    <div class="rollon">
                        Belo Essentials Whitening<br>
                        Deo Roll-On 40ml<br>
                        SRP: 89.75<br>
                        <a href="#" class="btn btn-default btn-medium">BUY NOW</a>
                    </div>
                    <div class="can"></div>
                    <div class="spray">
                        Belo Essentials Whitening<br>
                        Deo Spray 140ml<br>
                        SRP: 179.75<br>
                        <a href="#" class="btn btn-default btn-medium">BUY NOW</a>
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </section><!-- #product -->
</section><!-- .main content -->

<footer class="main">
    <div class="container">
        <div class="left clearfix">
            <h1 class="logo from-sprites"><a href="#">Belo Essentials Get Beautyful #ANNEderarms</a></h1>

            <div class="footnav">
                <a href="#home">HOME</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#prizes">PRIZES</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#mechanics">MECHANICS</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#products">PRODUCTS</a>
                <p class="copyright">Copyright © 2014 Belo Essentials. All rights reserved.</p>
            </div>

            <p>
                <a href="#" class="share-fb from-sprites" onclick="facebook()">Share with Facebook</a>
                <a href="#" class="share-tw from-sprites">Share with Twitter</a>
            </p>
        </div>
        <div class="right clearfix">
            <figure class="footer-product">Belo Deo</figure>
        </div>
    </div>
</footer><!-- .main footer -->

<script>window.jQuery || document.write('<script src="<?= base_url() ?>assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
<!--[if gt IE 8]><!--><script type="text/javascript" src="<?= base_url() ?>assets/js/vendor/skrollr.min.js"></script><!--<![endif]-->
<script type="text/javascript" src="<?= base_url() ?>assets/js/vendor/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/vendor/jquery-ui.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/vendor/jquery.slimscroll.min.js"></script>                        
<script type="text/javascript" src="<?= base_url() ?>assets/js/main.js?v=1"></script>

 <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

<!-- popup -->
<div id="popup-full-mechanics">
    <div class="popup">
        <div class="popup-content for-mechanics">
            <div class="content clearfix">
                <a href="#" class="close">Close Popup</a>

                <div class="slider">
                    <div class="padding">
                    <h3>Full Mechanics</h3>
                        <?php echo $mechanics[0]['content'] ?>
                    </div><!-- .padding -->
                </div><!-- .slider -->
            </div>
        </div><!-- .popup-conten -->
    </div><!-- .popup -->
    <div class="blur"></div>
</div>
<!-- .popup -->

<!-- popup -->
<div id="popup-video">
    <div class="popup">
        <div class="popup-content for-upload">
            <a href="#" class="close">CLOSE</a>
            <iframe width="100%" height="100%" src="//www.youtube.com/embed/q-E36QOtHi0?rel=0" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="blur"></div>
</div>
<!-- .popup -->

<script>
    function twitter() {
        window.open('http://twitter.com/?status='+encodeURIComponent('#ANNEderarms #KilikiligLines @Belo_Essentials '),'_blank','width=600,height=300')
    }

    function share_twitter() {
        window.open('http://twitter.com/?status='+encodeURIComponent('Lorem ipsum dolor sit amet'),'_blank','width=600,height=300')
    }

    function facebook() {
            FB.ui({ method: 'feed',
                    name: 'ANNEderarms Twitter Feeds',
                    link: '<?= site_url() ?>',
                    picture: 'http://nwshare.ph/chookstogo/aguinaldo/fb/branch/images/75x75.jpg',
                    caption: ' ',
                    description: 'Lorem ipsum dolor sit amet consectetur del ams.'
               });
        }
</script>
</body>
</html>