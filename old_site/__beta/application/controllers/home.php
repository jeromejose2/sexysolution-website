<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()	{

		$params = array(
			'table'=>'tbl_entries',
			'where'=>'status = 1',
			'order'=>'timestamp DESC'
		);
		$this->data['entries'] = $this->mysql_queries->get_data($params);

		$params = array(
			'table'=>'tbl_settings',
			'where'=>'type = \'mechanics\''
		);
		$this->data['mechanics'] = $this->mysql_queries->get_data($params);

		$this->load->view('index', $this->data);

	}
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */