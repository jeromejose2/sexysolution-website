<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reset_Password extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$params = array(
			'table'=>'tbl_registrants',
			'where'=>'id='.$_GET['id'].' AND token = "'.$_GET['token'].'"'
		);
		$result = $this->mysql_queries->get_data( $params );

		if( !$result ) {
			redirect(base_url());
			exit;
		}

		$this->data['r_id'] = @$result[0]['id'];
		$params = array(
			'table'=>'tbl_clinics'
		);
		$this->data['googleMap'] = $this->mysql_queries->get_data($params);
		$params = array(
			'table'=>'tbl_packages'
		);
		$this->data['packages'] = $this->mysql_queries->get_data($params);
		$params = array(
			'table'=>'tbl_videos'
		);
		$this->data['videos'] = $this->mysql_queries->get_data($params);
		$params = array(
			'table'=>'tbl_about'
		);
		$this->data['about'] = $this->mysql_queries->get_data($params);
		$params = array(
			'table'=>'tbl_settings',
			'where'=>'type=\'analytics\''
		);
		$this->data['analytics'] = $this->mysql_queries->get_data($params);

		$this->load->view('main-page', $this->data);

	}

	public function update() {

		$params = array(
			'table'=>'tbl_registrants',
			'where'=>'id='.$_POST['update_id'],
			'post'=>array(
				'password'=>md5($_POST['password']),
				'token'=>uniqid('', true)
			)
		);
		$this->mysql_queries->update_data( $params );

		$this->data['updateSuccess'] = true;
		$params = array(
			'table'=>'tbl_clinice'
		);
		$this->data['googleMap'] = $this->mysql_queries->get_data($params);
		$params = array(
			'table'=>'tbl_packages'
		);
		$this->data['packages'] = $this->mysql_queries->get_data($params);
		$params = array(
			'table'=>'tbl_videos'
		);
		$this->data['videos'] = $this->mysql_queries->get_data($params);
		$params = array(
			'table'=>'tbl_about'
		);
		$this->data['about'] = $this->mysql_queries->get_data($params);
		$params = array(
			'table'=>'tbl_settings',
			'where'=>'type=\'analytics\''
		);
		$this->data['analytics'] = $this->mysql_queries->get_data($params);

		$this->load->view('main-page', $this->data);

	}

}

?>