<!-- RAYMOND GUTIERREZ -->
<div id="celebrity" class="black-popup large-popup">
    <div class="celebrity-fullview clearfix">
        <div class="celeb-photo"><img src="uploads/<?=$info[0]['photo']?>" alt="<?=$info[0]['title']?>"></div>
        <div class="celeb-details">
            <p>
                <?= $info[0]['description'] ?>
            </p>
        </div>
    </div>
</div>
