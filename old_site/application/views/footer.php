<div id="login" class="mfp-hide pink-popup small-popup">
    <header>Log In</header>
    <div class="form-holder clearfix">
        <form action="<?php echo base_url() ?>home/login" method="POST" onsubmit="return validateLogin()" id="login-form">
            <div class="whole">
                <label>E-mail Address</label>
                <input type="text" name="email" class="required-login email-login" alt="Sorry, you need to enter your email address to login.">
            </div>
            <div class="whole">
                <label>Password</label>
                <input class="width-65-percent required-login password-login" name="password" type="password" alt="Sorry, you need to enter your password to login" maxlength="24">
            </div>
            <div class="whole text-center">
                <button type="submit">Submit</button>
                <br>
                <a href="javascript:void(0)" class="small-text show_forgot_password">FORGOT PASSWORD?</a>
                <div class="login-error-mgs"></div>
            </div>
        </form>
    </div>
</div>