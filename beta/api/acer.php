<?php



date_default_timezone_set('Asia/Manila');



class Acer

{

    private static $settings = array(

        'production' => array(

            'base_url' => 'https://crm.acer.com.ph:441/Pregistrant/',

            'username' => 'APHInuwork$',

            'password' => '@CERnuwork$2014'

        ),

        'staging' => array(

            'base_url' => 'https://crm-staging.acer.com.ph:441/Pregistrant/',

            'username' => 'APHInuwork$stg',

            'password' => '@CERnuwork$2014stg'

        )

    );



    private static $env = 'production';



    const BASE_TOKEN = 'B76FBDF582117A5C113C16E112125';



    public static function setDevMode($switch)

    {

        self::$env = $switch ? 'staging' : 'production';

    }



    public static function token()

    {

        $month = date('n');

        $token = strtoupper(substr(date('D'), 0, -1)).(date('Y') + $month + date('j') + date('G'));

        return substr_replace(self::BASE_TOKEN, $token, $month, 0);

    }



    public static function register($registrant, $onError = null)

    {

        $params = array(

            'fbid' => $registrant['fbid'],

            'first_name' => $registrant['first_name'],

            'last_name' => $registrant['last_name'],

            'mobile' => $registrant['mobile'],

            'email' => $registrant['email'],

            'password' => $registrant['password'],

            'address' => $registrant['address'],

            'birthdate' => $registrant['birthdate'],

            'gender' => $registrant['gender'],

            'date_registered' => $registrant['date_registered'],

            'sToken' => self::token()

        );

        $curl = curl_init(self::$settings[self::$env]['base_url'].'CreateRegistrant?'.http_build_query($params));

        $return = self::execute($curl);

        if (empty($return['success'])) {

            if ($onError && is_callable($onError)) {

                //$onError($return['errMsg']);
                $onError($return);

            }

            return false;

        }

        return true;

    }



    public static function isRegistered($email, $onError = null)

    {

        $params = array(

            'fb_email' => $email,

            'sToken' => self::token()

        );

        $curl = curl_init(self::$settings[self::$env]['base_url'].'ValRegExist?'.http_build_query($params));

        $return = self::execute($curl);

        if (empty($return['exist'])) {

            if ($onError && is_callable($onError)) {

                $onError($return['errMsg']);
                //$onError($return);

            }

            return false;

        }

        return true;

    }



    public static function update($registrant, $onError = null){

            $params = array(

                'fbid' => $registrant['fbid'],

                'first_name' => $registrant['first_name'],

                'last_name' => $registrant['last_name'],

                'mobile' => $registrant['mobile'],

                'email' => $registrant['email'],

                'password' => $registrant['password'],

                'address' => $registrant['address'],

                'birthdate' => $registrant['birthdate'],

                'gender' => $registrant['gender'],

                'date_registered' => $registrant['date_registered'],

                'sToken' => self::token()

            );

            $curl = curl_init(self::$settings[self::$env]['base_url'].'UpdateRegistrant?'.http_build_query($params));

            $return = self::execute($curl);

            if (empty($return['success'])) {

                if ($onError && is_callable($onError)) {

                    //$onError($return['errMsg']);
                    $onError($return);

                }

                return false;

            }

            return true;

    }



    private static function execute(&$curl)

    {

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_USERPWD, self::$settings[self::$env]['username'].':'.self::$settings[self::$env]['password']);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($curl);//json_decode(curl_exec($curl), true);

        curl_close($curl);

        return $result;

    }

}

