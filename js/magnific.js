$(document).ready(function() {
    if( r_id ) {
      $('.update-id').val(r_id);
      $('.show_password_verification').magnificPopup({
        items: {
          src: '#passwordverify',
          type: 'inline'
        }
      }).magnificPopup('open');
    }

    if( updateSuccess ) {
      $('.show_in_update_success').magnificPopup({
        items: {
          src: '#change-password-success',
          type: 'inline'
        }
      }).magnificPopup('open');
    }

    $('.show_in_password_retrieval').magnificPopup({
      items: {
        src: '#password-retrieval',
        type: 'inline'
      }
    });

    $('.show_in_update_success').magnificPopup({
      items: {
        src: '#change-password-success',
        type: 'inline'
      }
    });

    $('.show_register').magnificPopup({
      items: {
        src: '#signin',
        type: 'inline'
      }
    });

    $('.show_password_verification').magnificPopup({
      items: {
        src: '#passwordverify',
        type: 'inline'
      }
    });

    $('.show_forgot_password').magnificPopup({
      items: {
        src: '#forgotpassword',
        type: 'inline'
      }
    });

    $('.show_in_generic').magnificPopup({
      items: {
        src: '#generic',
        type: 'inline'
      }
    });

    $('.show_in_forgot_password').magnificPopup({
      items: {
        src: '#change-password-success',
        type: 'inline'
      }
    });

    $('.show_in_register_success').magnificPopup({
      items: {
        src: '#register-success',
        type: 'inline'
      }
    });

    $('.show_login').magnificPopup({
      items: {
        src: '#login',
        type: 'inline'
      }
    });

    $('.show_video').magnificPopup({
      items: {
        src: '#video',
        type: 'inline'
      },
      callbacks: {
        close: function() {
          $('#video').html('');
        }
      }
    });

    $('.show_celebrity').magnificPopup({
      items: {
        src: '#celebrity',
        type: 'inline'
      }
    });

    $('.show_celebrity_regine').magnificPopup({
      items: {
        src: '#celebrity-regine',
        type: 'inline'
      }
    });

    $('.show_celebrity_georgina').magnificPopup({
      items: {
        src: '#celebrity-georgina',
        type: 'inline'
      }
    });

    $('.show_celebrity_iza').magnificPopup({
      items: {
        src: '#celebrity-iza',
        type: 'inline'
      }
    });

    $('.show_celebrity_andi').magnificPopup({
      items: {
        src: '#celebrity-andi',
        type: 'inline'
      }
    });
});