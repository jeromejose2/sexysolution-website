//JavaScript Document
//lytebox by (c) jay ramirez

function Lytebox(){
	
	var thisClass = this;
	var onCloseCallback	= false;

	thisClass.open	= function(params,width)
	{
		/*
		@params
		params   =  string /object;
					params.top				: int (position top of the lytebox content)
					params.width			: int (width lytebox content)
					params.title			: string
					params.message			: string
					params.url				: url (external page/content)
					params.data				: object #if params has url 
					params.align			: string
					params.type 			: 'alert','confirm', default=blank.
					params.closeButton 		: boolean
					params.buttonAlign	 	: 'center/left/right'
					params.okCaption		: string, default='Ok'.
					params.cancelCaption	: string, default='Cancel'.
					params.onConfirm		: function ( callback function alter pressing confirm)
					params.onCancel			: function ( callback function alter pressing cancel)
					params.escButton		: boolean, (to close with esc button)
					params.blurClose		: boolean, (to close when the transparent bg is clicked, default is true)
					params.addClass			: add class attr to lytebox content to add a style
					params.fadeOut			: int, popup will auto close after the given time (mili seconds)
					params.onOpen			: callback function on open
					params.onClose			: callback function on close

		width  	  = int;
		*/
		
		var closebtn= params.url ? '' : '<div class="lytebox-close">x</div>';
		
		var	lytebox = '<div class="lytebox">';
			lytebox+= 	'<div class="lyteblur"></div>';
			lytebox+= 	'<div class="lytecontent">';
			lytebox+= 		closebtn;
			lytebox+= 	'</div>';
			lytebox+= '</div>';
	
		var extraButtons = '';

		var positionTop = params.top ? params.top : false;
		
		/* START APPEND LYTEBOX TO BODY */
		if( $('body').find('.lytebox') )
			thisClass.close();
		 
		$('body').append(lytebox);
		
		$('.lytebox').css('height',$(document).height()+'px');
		
		if(width){
			$('.lytecontent').css('width',width+'px');
		}
		if(params.width){
			$('.lytecontent').css('width',params.width+'px');
		}
		
		if(params.addClass){
			$('.lytecontent').addClass(params.addClass);
		}
		if(params.fadeOut){
			setTimeout(popup.close,params.fadeOut);
		}
		/********** END APPEND LYTEBOX TO BODY ************/
	
		/********** START LYTEBOX CONTENT ************/	
		if(params instanceof Object){
			if(params.url){
				//iframe popup

				thisClass.open({message:'<div class="lytebox-preload"></div>', closeButton: false});
				thisClass.display(params, true);	

				
				if(params.data){
					$.get(params.url, params.data, function(result)
					   {
						  $('.lytecontent').append(result);
						  thisClass.display(params);
						  $('.lytecontent .lytebox-preload').remove();
					   }
					);
				}else{
					 $('.lytecontent').load(params.url,function()
					 {
						thisClass.display(params);
						$('.lytecontent .lytebox-preload').remove();
					 });
				}
			}else{
				//message with parameters
				content 	  = params.title   	  ? '<h2>'+params.title+'</h2>' : '';
				content		 += params.message 	  ? params.message 			 	: '';
				align  		  = params.align   	  ? 't-'+params.align 			: '';
				
				if(params.closeButton==false || params.type=='alert' || params.type=='confirm'){
					$('.lytebox-close').remove();
				}
				
				if(params.type=='alert' || params.type=='confirm'){	
					var okCap	  = params.okCaption ? params.okCaption : 'Ok';
					var cancelCap = params.cancelCaption ? params.cancelCaption : 'Cancel';
					
					btnAlign	  =	params.buttonAlign ? 't-'+params.buttonAlign : 't-right';
					extraButtons	  = '<div class="extra-buttons '+btnAlign+'">';
					extraButtons	 += '<input type="button" value="'+okCap+'" class="jayLyteOk"/>';
					
					if(params.type=='confirm'){
						extraButtons  += '<input type="button" value="'+cancelCap+'" class="jayLyteCancel" />';
					}
					extraButtons	 += '</div>';
				}
				
				$('.lytecontent').append(content);
				$('.lytecontent').append(extraButtons);
				$('.lytecontent').addClass(align);
				thisClass.display(params);
			}
		}else{
			//simple string message
			content	= params;
			$('.lytecontent').append(content);
			thisClass.display(params);
		}
		/********** END LYTEBOX CONTENT ************/	
	}

	
	thisClass.close = function(callback)
	{
		if( $('body').find('.lytebox') )
		{
			$('.lytebox').fadeOut(100,function()
			{
				$(this).remove();		
				if(callback){
					callback();
				}
			});
		}
	}
	
	thisClass.display = function(globalParams,preloader)
	{
		if(!globalParams.url){
			$('.lytecontent').addClass('lyte-msg-container');
		}else{
			$('.lytecontent').removeClass('lyte-msg-container');
		}
		
		if(globalParams.url && !preloader){
			var newWidth = $('.lytecontent:first-child').outerWidth();
			$('.lytecontent').css({width: newWidth});
		}

		var ch = $('.lytecontent').outerHeight();
		var cw = $('.lytecontent').outerWidth();
		var wh = $(window).height();
		var wo = $('html, body').offset();


		$('.lytecontent').css(
				{'margin-left': '-'+(cw/2) +'px',
				 'display'	  : 'none'
				}); 

		$('.lytebox').fadeIn(100);
		$('.lytecontent').show(0);
		

		$('.lytecontent').css({'top' : globalParams.top!==false && globalParams.top!=undefined ? globalParams.top : ((wh-ch)/2)+(wo.top*-1) +'px'});	

		enableButtons(globalParams);


		if(globalParams.onOpen && !preloader){
			globalParams.onOpen();
		}
	}

	function enableButtons(globalParams){
		/********** START LYTEBOX CONTROL ************/	
		$('.jayLyteOk').unbind('click');
		$('.jayLyteCancel').unbind('click');
		$('.lyteblur').unbind('click');
		$('.lytebox-close').unbind('click'); 
		$('.popup-close').unbind('click');
		$(document).unbind('keydown');

		if(globalParams.onConfirm){
			$('.jayLyteOk').bind('click',function(){
				thisClass.close(globalParams.onClose);
				globalParams.onConfirm()
			});
		}else{
			$('.jayLyteOk').bind('click',function(){
				thisClass.close(globalParams.onClose);
			})
		}
		
		if(globalParams.onCancel){
			$('.jayLyteCancel').bind('click',function(){
				thisClass.close(globalParams.onClose);
				globalParams.onCancel();
			});
		}else{
			$('.jayLyteCancel').bind('click',function(){
				thisClass.close(globalParams.onClose);
			})
		}
		
		if(globalParams.blurClose!==false){
			$('.lyteblur').bind('click',function(){
				if(globalParams.closeButton!=false && globalParams.type!='alert' && globalParams.type!='confirm')
				thisClass.close(globalParams.onClose);
			});
		}
		
		if(globalParams.escButton!==false){
			$(document).bind('keydown',function(e){
					if (e.keyCode == 27) {
						if(globalParams.closeButton!=false && globalParams.type!='alert' && globalParams.type!='confirm')
						thisClass.close(globalParams.onClose);
				}
			});
		}

		$('.lytebox-close, .popup-close').bind('click',function(){
			thisClass.close(globalParams.onClose);
		});
		/********** END LYTEBOX CONTROL ************/
	}

	thisClass.alert = function(data, title, callback){
		
		if(data instanceof Object){
			var params = data;
				params.type = 'alert';
		}
		else{
			var params = {};
				params.message = data;
				params.type	= 'alert';
				params.onConfirm = callback;
				params.title = title;
		}

		popup.open(params);
		
	}

	thisClass.confirm = function(data, title, onConfirm, onCancel){
		
		if(data instanceof Object){
			var params = data;
				params.type= 'confirm';
		}
		else{
			var params = {};
				params.message = data;
				params.type	= 'confirm';
				params.onConfirm = onConfirm;
				params.onCancel = onCancel;
				params.title = title;
		}

		popup.open(params);
		
	}

	thisClass.load = function(url,data){
		var params = {};
			params.url = url;
			params.data = data;
		popup.open(params);
	}
}

var popup = new Lytebox();