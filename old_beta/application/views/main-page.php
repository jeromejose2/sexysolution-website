<!DOCTYPE html>
<?php session_start(); ?>
<!--[if IE 8]><html class="ie8"><![endif]-->
<!--[if IE 9]><html class="ie9"><![endif]-->
<!--[if gt IE 9]><!--><html><!--<![endif]-->
<head>
    <title>Sexy Solution by Belo</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Sexy Solution by Belo" />
    <meta name="keywords" content="sexy solution by belo, get sexy without surgery, sexy solution" />
    <link rel="shortcut icon" href="<?php echo base_url()?>img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url()?>img/favicon.ico" type="image/x-icon">
    <!--[if lt IE 9]>
            <script src="<?=base_url()?>js/vendor/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>js/vendor/fancybox/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/nuworks.css" />

</head>
<body id="skrollr-body">
<div>
    <a href="javascript:void(0)" class="show_password_verification invisible">Password Verify</a>
    <a href="javascript:void(0)" class="show_forgot_password invisible">Forgot Password</a>
    <a href="javascript:void(0)" class="show_in_generic invisible">Generic</a>
    <a href="javascript:void(0)" class="show_in_forgot_password invisible">Generic</a>
    <a href="javascript:void(0)" class="show_in_register_success invisible">Generic</a>
    <a href="javascript:void(0)" class="show_in_update_success invisible">Generic</a>
    <a href="javascript:void(0)" class="show_in_password_retrieval invisible">Generic</a>
    <a href="javascript:void(0)" class="show_register invisible">Sign In/Register</a>
    <a href="javascript:void(0)" class="show_login invisible">Login</a>
    <a href="javascript:void(0)" class="show_video invisible">Video</a>
    <a href="javascript:void(0)" class="show_celebrity invisible">Celebrity</a>
    <a href="javascript:void(0)" class="show_test_celebrity invisible">Celebrity</a>
</div>

<!-- START popup generic -->
<div id="generic" class="mfp-hide pink-popup">
    <div class="info-mgs">
        <header>Registration in Progress.</header>
        <div class="info-mgs-details">An email verification has been sent to your email to confirm your account.</div>
    </div>
</div>

<div id="password-retrieval" class="mfp-hide pink-popup">
    <div class="info-mgs">
        <header>Password Retrieval</header>
        <div class="info-mgs-details">Please check your email to generate a new password.</div>
    </div>
</div>

<div id="change-password-success" class="mfp-hide pink-popup">
    <div class="info-mgs">
        <header>Password reset success.</header>
        <div class="info-mgs-details">Your password has been updated. You may now log-in.</div>
    </div>
</div>

<div id="register-success" class="mfp-hide pink-popup">
    <div class="info-mgs">
        <header>Thank you</header>
        <div class="info-mgs-details">Thanks for registering! You can go ahead and explore the website now. Have fun!</div>
    </div>
</div>
<!-- END popup generic -->

<!-- START popup registration -->
<div id="signin" class="mfp-hide pink-popup">
    <header>REGISTER</header>
    <div class="form-holder clearfix">
        <form action="<?=base_url() ?>register" method="POST" onsubmit="return validateRegistration()" id="register-form">
            <div class="half">
                <label>First Name</label>
                <input type="text" name="firstname" class="required register-firstname" alt="One moment! You have to fill out the first name field to continue registration.">
            </div>
            <div class="half">
                <label>Last Name</label>
                <input type="text" name="lastname" class="required register-lastname" alt="One moment! You have to fill out the last name field to continue registration.">
            </div>
            <div class="whole">
                <label>E-mail Address</label>
                <input class="required register-email" type="text" name="email" alt="One moment! You have to fill out the email field to continue registration.">
            </div>
            <div class="whole">
                <label>Password (should contain 6 characters)</label>
                <input class="width-65-percent password required register-password" type="password" name="password" alt="One moment! You have to fill out the password field to continue registration." maxlength="24">
            </div>
            <div class="whole">
                <label>Re-type Password</label>
                <input class="width-65-percent required" type="password" name="re-password" alt="One moment! You have to fill out the re-type password field to continue registration." maxlength="24">
            </div>
            <div class="whole">
                <label>CAPTCHA</label>
                <div class="captcha-container"><img id="captcha" src="<?=base_url()?>securimage/securimage_show.php" alt="CAPTCHA IMAGE" class="captcha-image"></div>
                <input type="text" name="captcha_code" size="10" maxlength="6" class="width-40-percent required" alt="One moment! You have to fill out the captcha field to continue registration." />
                <a href="javascript:void(0)" onclick="refreshCaptcha()"><img class="captcha-refresh" src="<?=base_url()?>securimage/images/refresh.png" title="Generate new CAPTCHA image"></a>
                <!-- <img style="display: none" class="ajax-loader" src="<?=base_url()?>securimage/images/refresh.png" title="CAPTCHA refreshing"> -->
            </div>
            <div class="whole text-center">
                <button type="submit">Submit</button>
                <div class="registration-error-mgs"></div>
            </div>
        </form>
    </div>
</div>
<!-- END popup registration -->

<!-- START popup password verification -->
<div id="passwordverify" class="mfp-hide pink-popup small-popup">
    <div class="info-mgs">
        <div class="info-mgs-details">You have successfully verified the request for your password.</div>
    </div>
    <div class="form-holder clearfix">
        <form action="<?=base_url() ?>" method="POST" onsubmit="return validateUpdatePassword()">
            <input type="hidden" name="update_id" class="update-id">
            <div class="whole">
                <label>Please enter your New Password (should contain 6 characters)</label>
                <input class="width-65-percent update-password required-update-password" type="password" name="password" alt="Sorry, you need to enter your new password to update your account" maxlength="24">
            </div>
            <div class="whole">
                <label>Re-type New Password</label>
                <input class="width-65-percent update-repassword required-update-password" type="password" name="repassword" alt="Sorry, you need to re-enter your new password to update your account" maxlength="24">
            </div>
            <div class="whole text-center">
                <button type="submit">Submit</button>
                <div class="error-mgs-update-password">
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END popup password verification -->

<!-- START popup forgot password -->
<div id="forgotpassword" class="mfp-hide pink-popup">
    <div class="info-mgs">
        <div class="info-mgs-details">To retrieve your password, please enter the email address you used to register the account.</div>
    </div>
    <div class="form-holder clearfix">
        <form action="<?=base_url()?>fogot_password" method="POST" onsubmit="return validateForgotPassword()" id="forgot-password-form">
            <div class="whole">
                <label>E-mail Address</label>
                <input class="width-65-percent required-forgot-password" name="email" alt="Sorry, you need to enter your email address to reset your password.">
            </div>
            <div class="whole text-center">
                <button type="submit">Submit</button>
                <div class="forgotpw-error-mgs"></div>
            </div>
        </form>
    </div>
</div>
<!-- END popup forgot password -->

<!-- START popup login -->
<div id="login" class="mfp-hide pink-popup small-popup">
    <header>Log In</header>
    <div class="form-holder clearfix">
        <form action="<?=base_url() ?>home/login" method="POST" onsubmit="return validateLogin()" id="login-form">
            <div class="whole">
                <label>E-mail Address</label>
                <input type="text" name="email" class="required-login email-login" alt="Sorry, you need to enter your email address to login.">
            </div>
            <div class="whole">
                <label>Password</label>
                <input class="width-65-percent required-login password-login" name="password" type="password" alt="Sorry, you need to enter your password to login" maxlength="24">
            </div>
            <div class="whole text-center">
                <button type="submit">Submit</button>
                <br>
                <a href="javascript:void(0)" class="small-text show_forgot_password">FORGOT PASSWORD?</a>
                <div class="login-error-mgs"></div>
            </div>
        </form>
    </div>
</div>
<!-- END popup login -->

<!-- START popup video -->
<div id="video" class="mfp-hide video-popup large-popup" style="background-image: url('<?=base_url()?>images/admin/video_loading.GIF'); background-repeat: no-repeat; background-position: center">

</div>
<!-- END popup video -->

<!-- START popup celebrity -->
    <!-- RAYMOND GUTIERREZ -->
<div id="celebrity" class="mfp-hide black-popup large-popup">
    <div class="celebrity-fullview clearfix">
        <div class="celeb-photo"><img src="img/celeb-1-large.jpg" alt="Raymond Gutierrez"></div>
        <div class="celeb-details">
            <p>Raymond’s Resolution Made Real <br>
                        by Sexy Solutions</p>

            <p>Raymond wanted to start 2012 with a bang. Other people start the year with new looks, new hairdos, new habits. Raymond however, decided to enter the new year with a sexy new body.</p>

            <p>Men and women alike are conscious of their figures. Sexy Solutions tailor fits a program based on your needs and goals. For Raymond, it’s to lose the inches and gain the body he always wanted. Sexy Solutions helped him achieve his goal.</p>

            <p>“I already saw results on my first session! In my first session, which lasted less than 2 hours, I lost 4.7 inches from my midsection. Of course, it’s up to you how to maintain it. For me, I balanced a proper diet and exercise regimen with regular treatments at Sexy Solutions.” </p>

            <p>"Thanks to Sexy Solutions, my resolution was easier to fulfill. Now, I really have a new start. It feels great!" Raymond excitedly said.</p>

            <p>With all those inches he lost, he gained more confidence—now that’s really sexy.</p>

        </div>
    </div>
</div>
    <!-- RAYMOND GUTIERREZ -->
    <!-- REGINE VELASQUEZ -->
<div id="celebrity-regine" class="mfp-hide black-popup large-popup regine-popup-fix">
    <div class="celebrity-fullview clearfix">
        <div class="celeb-photo celeb-photo-fix regine-photo-fix"><img src="img/celeb-regine.jpg" alt="Regine Velasquez"></div>
        <div class="celeb-details regine-details-fix">
            <p>Regine’s Concert Almost Got Postponed</p>

            <p>After resting from the limelight to give time to her now 1-year old son, Nate, Asia’s Songbird, Regine Velasquez has proven, once again, that she can still amaze us with her jam-packed 25th anniversary concert, SILVER. But just like any come-back, it did not become easy for Regine. Aside from her sickness and loss of voice during her concert, another thing that nearly postponed her concert was her post partum weight plateau battle.</p>

            <p> “Hinintay pa nilang mas pumayat ako.” Regine says in one interview, when asked why her 25th Anniversary concert (which was supposedly held last year) got postponed and was moved to 2012. After giving birth, Regine had a 37.2-inch abdomen measurement. Sharing same thoughts with most moms at post partum, she will do anything to lose weight and get her old figure back. Regine enrolled in different diet programs. Although she lost pounds, she later on noticed that despite her being religious in following instructions, her weight loss just stopped. Her body reached its PLATEAU. That’s when she started going to Sexy Solutions and undergoing the Anti-Plateau Diet.</p>

            <p>Sexy Solutions’ Anti-plateau diet helped Regine lose pounds after she has hit a weight loss plateau or the weight set-point where her body got used to its normal weight making it difficult for her to lose pounds no matter how much exercise or diet she does. The diet is specially formulated by US Certified Fitness Nutritionist, Nadine Tengco. It contains Anti-inflammatory properties that fight the plateau, promote weight loss and believe it or not, gives a person a youthful glow as well. What made Regine get her smallest 29-inch abdomen measurement is the perfect combination of Sexy Solutions treatments plus the Anti-plateau diet. It is the only diet that compliments radio frequency treatments, achieving the maximum effect. Regine orders her Anti-plateau diet food from The Sexy Chef which is convenient for a busy bee like her.</p>

            <p>After Regine broke her plateau, she took one more ultimate move to hit that target figure for her concert. Regine underwent the ULTIMATE SEXY SOLUTIONS Program. It’s a 3 to 5-day program that can make one lose as much as 10 lbs in just 3 to 5 days. The program consists of drinks formulated by Nadine Tengco and salads by Cravings combined with Sexy Solutions treatments. This is perfect for those who are eyeing a specific weight-loss and inches-off target with only limited time to prepare, just like Regine for her concert.</p>

            <p>Regine also clarified that the program has nothing to do with her loss of voice. In fact, if she continued with the program, she should have not been affected with the virus because the program is packed with the necessary nutrients and treatments her body needs to boost her immune system and make her slimmer for the concert. During the concert, the Songbird’s confidence and drive, despite her being sick, was obvious, especially every time she came out in different costumes, which perfectly fit her and emphasized her amazing new figure. Now that her battle with the weight plateau is over, and she was able to hit that ultimate goal for her weight, she is definitely back to wow us again with what she’s got!</p>
        </div>
    </div>
</div>
    <!-- REGINE VELASQUEZ -->
    <!-- GEORGINA WILSON -->
<div id="celebrity-georgina" class="mfp-hide black-popup large-popup">
    <div class="celebrity-fullview clearfix">
        <div class="celeb-photo celeb-photo-fix"><img src="img/celeb-georgina.jpg" alt="Georgina Wilson"></div>
        <div class="celeb-details">
            <p>GEORGINA WILSON</p>

            <p>"I saw results immediately. Consequently, I became hooked right away! Areas that I had practically given up on were immediately smaller. I actually couldn’t believe the measurements after the first time I tried it because they were so drastic.  I went from a 29-inch to a 25-inch waist in two months. Sexy Solutions has been a large part of my commitment to fitness. I’m so happy because I feel like I have a partner in losing weight and maintaining my ideal body. It doesn’t all happen in Sexy Solutions clinics, exercising and dieting are integral to the program, but I definitely feel like we’re all working towards the same goal— a body that I am happy with. Sexy Solutions is my secret weapon to getting sexy and most importantly, staying sexy."</p>
        </div>
    </div>
</div>
    <!-- GEORGINA WILSON -->
    <!-- IZA CALZADO -->
<div id="celebrity-iza" class="mfp-hide black-popup large-popup">
    <div class="celebrity-fullview clearfix">
        <div class="celeb-photo"><img src="img/celeb-iza.jpg" alt="Iza Calsado"></div>
        <div class="celeb-details">
            <p>IZA CALZADO</p>
        </div>
    </div>
</div>
    <!-- IZA CALZADO -->
    <!-- ANDI EIGENMAN -->
<div id="celebrity-andi" class="mfp-hide black-popup large-popup">
    <div class="celebrity-fullview clearfix">
        <div class="celeb-photo celeb-photo-fix"><img src="img/celeb-andi.jpg" alt="Andi Eigenmann"></div>
        <div class="celeb-details">
            <p>ANDI EIGENMANN</p>

            <p>Sexy Solutions was just what Andi needed when she staged her post- pregnancy comeback. She doesn’t look like a mother of a year-old daughter (named Ellie), does she?</p>

            <p>How did she do it?</p>

            <p>Gleefully she replied, “Diet lang! I’ve been really busy with work lately. I’m actually happy that they allowed me to still be able to attend the event tonight because I’ve been really looking forward to this.”</p>

            <p>She added, laughing, “It’s just a matter of self-discipline, a healthy lifestyle, and Sexy Solutions!”</p>

            <p>Seeking cosmetic surgeon Dr. Vicki Belo’s aid is not a fact that Andi tries to hide. The young actress’ openness is something she is known for since she became active in showbiz five years ago.</p>
        </div>
    </div>
</div>
    <!-- ANDI EIGENMAN -->
<!-- END popup celebrity -->

<header class="main">
        <h1 class="logo"><a href="#home">Sexy Solution by Belo</a></h1>
        <a href="javascript:void(0)" id="mininav">Mini Nav</a>
        <nav class="navigation">
            <a href="#treatment" class="active">Treatment & Programs</a>
            <a href="#location">Location</a>
            <a href="#pricing">Pricing</a>
            <a href="#dream-team">Dream Team</a>
            <a href="#testimonials">Testimonials</a>
            <a href="#gallery">Videos</a>
            <a href="#sexy-treats">Sexy Treats</a>
            <a href="#contact">Contact</a>
        </nav>
        <?php if( $this->session->userdata('email') ) : ?>
            <div class="login-form">
                <div class="form-group clearfix">
                    <div class="user-info">Welcome, <?=$this->session->userdata('firstname') ?> <?=$this->session->userdata('lastname') ?>! </div>
                    <button type="submit" class="logout-btn"><a href="<?=base_url() ?>logout" class="logout-button">Log-out</a></button>
                </div>
            </div>
        <?php else : ?>
            <div class="login-form login-form-fix">
                <a href="javascript:void(0)" class="text-upp show_register padding-5">Sign Up</a>

                <div class="form-group clearfix">
                    <div class="already-a-member-text">Already a member?</div>
                    <button type="submit" class="show_login">Log-in</button>
                </div>
            </div>
        <?php endif; ?>
</header><!-- .main header -->

<section class="main">
    <section id="home" class="clearfix">
        <div class="container">
            <h1>Get Sexy Without Surgery</h1>
            <figure class="welcome-img">Welcome to the SEXY Revolution</figure>
            <div><strong>#SexyRevolution</strong></div>

            <div class="scroll-to-join">
                SCROLL TO <br>JOIN THE REVOLUTION
            </div>
        </div><!-- .container -->
    </section><!-- #home -->
    <section id="about" class="clearfix">
        <div class="about-img-bg">
            <div class="container">
                <h1>Get Sexy Without Surgery</h1>
                <p>Sexy Solutions by Belo is a non-surgical fat reduction clinic that specializes in losing those last few inches that never seem to come off no matter how much diet or exercise you do. </p>
                <p>The focus is non-invasive body contouring using the latest technology, coupled with smart and sustainable nutrition and fitness programs that go hand in hand in achieving your goals.</p>
            </div><!-- .container -->
        </div>
    </section><!-- #about -->
    <section id="treatment" class="clearfix">
        <div class="container">
            <hgroup>
                <h1>"How does it work?" & "Why does it work?" </h1>
                <h2>are the 2 most common questions asked of us. Our response?</h2>
                <h3>The SECRET is in the PROCESS.</h3>
            </hgroup>

            <div class="heading">
                <strong>Behind every great solution is an equation. Here's ours: MELT FAT + TIGHTEN SKIN + TONE MUSCLE</strong>
                This tried and tested formula lets you lose up to 2 inches on your first session.  The procedures complement one another, helping you achieve maximum results.
            </div>


            <div class="white-boxes clearfix">
                <div class="box with-plus">
                    <figure><img src="img/melt-fat.png" alt="melt fat"></figure>
                    <div class="content">
                        <h3>
                            <strong>MELT FAT</strong>
                            with LipoCavitation
                        </h3>
                        <p>LipoCavitation breaks down
                        the stubborn fat cells in areas
                        such as the abdomen, thighs,
                        arms, upper and lower back
                        by delivering low frequency
                        ultrasound waves. This causes
                        the fat cells to burst and the fat
                        to seep out,making it easier
                        to flush out.</p>
                        <button class="btn btn-gray btn-small">More Details</button>
                    </div><!-- .content -->
                </div><!-- .box -->
                <div class="box with-plus">
                    <figure><img src="img/tighten-skin.png" alt="melt fat"></figure>
                    <div class="content">
                        <h3>
                            <strong>TIGHTEN SKIN </strong>
                            with MultiFirm
                        </h3>
                        <p>MultiFirm uses radiofrequency energy to improve the appearance of cellulite and stretch marks. As a result, the body becomes reshaped and tightened. This procedure can also tighten sagging skin on the face and neck, and lessen fine lines and wrinkles, giving you that refreshing lift. </p>
                        <button class="btn btn-gray btn-small">More Details</button>
                    </div><!-- .content -->
                </div><!-- .box -->
                <div class="box">
                    <figure><img src="img/tone-muscle.png" alt="melt fat"></figure>
                    <div class="content">
                        <h3>
                            <strong>TONE MUSCLE</strong>
                            with BodySculpt
                        </h3>
                        <p>BodySculpt releases energy pulses to the muscles promoting contractions similar to exercise, causing a "squeezing" effect that is necessary for the acceleration of liquid fat drainage. It improves blood circulation, lymphatic drainage, and fat metabolism. </p>
                        <button class="btn btn-gray btn-small">More Details</button>
                    </div><!-- .content -->
                </div><!-- .box -->
            </div><!-- .white-boxes -->

            <div class="teaser">
                <i class="arrow-on-left from-sprites"></i>
                Let's kick up the intensity notch higher with these advanced treatments that will hit fat like a runaway train.
                <i class="arrow-on-right from-sprites"></i>
            </div><!-- .teaser -->

            <div class="red-boxes clearfix">
                <div class="box">
                    <div class="content">
                        <h3>
                            <strong>KILL THE FAT AND TIGHTEN SKIN </strong>
                            with TiteFX
                        </h3>
                        <p>TiteFX is the newest and most innovative non-invasive body contouring and fat-destroying machine. It sends high voltage pulses that result in electroporation, causing permanent destruction of fat cells. It also makes use of radiofrequency that provides precise optimal heating of the skin for tightening and body shaping.</p>
                        <button class="btn btn-gray btn-small">More Details</button>
                    </div><!-- .content -->
                </div><!-- .box -->
                <div class="box">
                    <div class="content">
                        <h3>
                            <strong>WORK OUT THE FAT</strong>
                            with ION Magnum
                        </h3>
                        <p>Advanced fitness system that rapidly burns fat and delivers the benefits of an intense, professional workout without the pain, physical exertion, and exhaustion that comes with conventional exercising. The Ion Magnum uses specific, patented micro-current waveforms to stimulate highly efficient and complete muscle contractions and to improve how your nerves activate and communicate with your muscles.</p>
                        <button class="btn btn-gray btn-small">More Details</button>
                    </div><!-- .content -->
                </div><!-- .box -->
                <div class="box">
                    <div class="content">
                        <h3>
                            <strong>GET SEXY ON LUNCH BREAKS</strong>
                            with QuadSculpt
                        </h3>
                        <p>Quad Sculpt is a lunch break procedure that combines all modalities of the other machines.
it melts fat using low frequency ultrasound, tightens skin with its radio frequency diodes, and tones muscle too. All that in just 45 minutes!</p>
                        <button class="btn btn-gray btn-small">More Details</button>
                    </div><!-- .content -->
                </div><!-- .box -->
            </div><!-- .white-boxes -->

        </div><!-- .container -->
    </section><!-- #treatment -->
    <section id="location" class="clearfix">
        <header class="section-heading">
            <h1>BRANCHES</h1>
        </header>
        <div class="location-container">
            <div class="container">
                <div class="row with two-column">
                    <div class="column">
                        <?php if( @$googleMap[0] ) : ?>

                              <figure style="height: 258px"><a class="fancy-link fancybox.ajax" href="popups/map1/?lat=<?=$googleMap[0]['latitude'] ?>&long=<?=$googleMap[0]['longitude'] ?>"><img src="img/location-qc.jpg"/></a></figure>



                        <address>
                            <dl>
                                <dt>Address:</dt>
                                    <dd><?=$googleMap[0]['address'] ?></dd>
                                <dt>Mobile:</dt>
                                    <dd><?=$googleMap[0]['mobile'] ?></dd>
                                <dt>Landline:</dt>
                                    <dd><?=$googleMap[0]['landline'] ?></dd>
                                <dt>Branch Email Address:</dt>
                                    <dd><a href="mailto:bhsfort@sexysolutions.net"><?=$googleMap[0]['email'] ?></a></dd>
                                <dt>Branch Hours:</dt>
                                    <dd><?=$googleMap[0]['branch_hours'] ?></dd>
                                </dl>
                        </address>
                        <?php endif; ?>
                    </div>
                    <div class="column">
                        <?php if( @$googleMap[1] ) : ?>
                        <figure style="height: 258px"><a class="fancy-link fancybox.ajax" href="popups/map2/?lat=<?=$googleMap[1]['latitude'] ?>&long=<?=$googleMap[1]['longitude'] ?>"><img src="img/location-bgc.jpg"/></a></figure>
                        <address>
                            <dl>
                                <dt>Address:</dt>
                                    <dd><?=$googleMap[1]['address'] ?></dd>
                                <dt>Mobile:</dt>
                                    <dd><?=$googleMap[1]['mobile'] ?></dd>
                                <dt>Landline:</dt>
                                    <dd><?=$googleMap[1]['landline'] ?></dd>
                                <dt>Branch Email Address:</dt>
                                    <dd><a href="mailto:morato@sexysolutions.net"><?=$googleMap[1]['email'] ?></a></dd>
                                <dt>Branch Hours:</dt>
                                    <dd><?=$googleMap[1]['branch_hours'] ?></dd>
                            </dl>
                        </address>
                        <?php endif; ?>
                    </div>
                </div>
            </div><!-- .container -->
        </div>
    </section><!-- #location -->
    <section id="pricing" class="clearfix">
        <header class="section-heading">
            <h1>Packages</h1>
        </header>
        <div class="pricing-img-bg">
            <div class="transparent-white-container">
                <!-- Nav -->
                <a href="#prev_owl_item">Previous Item</a>
                <a href="#next_owl_item">Next Item</a>
                <!-- /Nav -->
                <div class="container">
                    <div id="packages" class="owl-carousel">
                        <?php foreach( $packages as $k => $v ) : ?>
                            <div>
                                <div class="row with two-column">
                                    <div class="column left">
                                        <h2><?=$v['title'] ?></h2>
                                        <?=$v['description'] ?>
                                    </div>
                                    <div class="column right">
                                        <?=$v['items'] ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div><!-- #packages -->
                </div><!-- .container -->
            </div><!-- .transparent-white-container -->
        </div><!-- .pricing-img-bg -->
        <div class="instruction">
            The diagram below defines the segmentation of areas. <br>
            When availing a package, you may interchange areas when you come in for sessions,  <br>
            as long as the price per area is the same.
        </div><!-- .instruction -->
        <div class="tab-container">
            <div class="container">
                <div class="tabs">
                    <nav class="tabs-nav">
                        <a href="javascript:void(0)" class="current">Male</a>
                        <a href="javascript:void(0)">Female</a>
                    </nav>
                    <article>
                        <button>See Back View</button>
                        <figure class="front-view active"><img src="img/front-male.jpg" alt="front"></figure>
                        <figure class="back-view"><img src="img/back-male.jpg" alt="back"></figure>
                    </article>
                    <article>
                        <button>See Back View</button>
                        <figure class="front-view active"><img src="img/front-girl.jpg" alt="front"></figure>
                        <figure class="back-view"><img src="img/back-girl.jpg" alt="back"></figure>
                    </article>
                </div><!-- .tab -->
            </div><!-- .container -->
    </div><!-- .tab-container -->
    </section><!-- #pricing -->
    <section id="dream-team" class="clearfix">
        <div class="container">
            <header>
                Here in <em>Sexy Solutions</em>, the experts are always <em>IN</em>. This is your support system
                to help you achieve and sustain the body you've always dreamt of.
            </header>

            <div class="row team featured">
                <figure class="cristalle big-photo"></figure>
                <div class="content">
                    <figure class="circled-img">
                        <img class="gray-img" src="img/cristalle-gray-circle.png" alt="cristalle">
                        <img class="colored-img" src="img/cristalle-colored-circle.png" alt="cristalle">
                    </figure>
                    <div class="details">
                        <h4>Cristalle Belo-Henares<span>Beauty Expert</span></h4>
                        <button class="btn btn-gray btn-small">More Details</button>
                        <article>Born into the beauty industry, Cristalle embraced all the opportunities presented her way allowing her to become the woman she is today - smart, strong, and sophisticated. She believes in effortless beauty that is founded on a natural approach to enhance what is already within.</article>
                    </div>
                </div>
            </div>
            <div class="row team right-align">
                <figure class="nadine big-photo"></figure>
                <div class="content">
                    <figure class="circled-img">
                        <img class="gray-img" src="img/nadine-gray-circle.png" alt="nadine">
                        <img class="colored-img" src="img/nadine-colored-circle.png" alt="nadine">
                    </figure>
                    <div class="details">
                        <h4>Nadine Tengco<span>Nutrition Expert</span></h4>
                        <button class="btn btn-gray btn-small">More Details</button>
                        <article>A US Certified Nutritionist and Weight Loss Specialist of the Biggest Loser Philippines, Nadine created proprietary diet plans and drinks that complement and further enhance the effect of the treatments by reducing swelling and bloating.</article>
                    </div>
                </div>
            </div>
            <div class="row team left-align">
                <figure class="edward big-photo"></figure>
                <div class="content">
                    <figure class="circled-img">
                        <img class="gray-img" src="img/edward-gray-circle.png" alt="edward">
                        <img class="colored-img" src="img/edward-colored-circle.png" alt="edward">
                    </figure>
                    <div class="details">
                        <h4>Edward Mendez<span>Fitness Expert</span></h4>
                        <button class="btn btn-gray btn-small">More Details</button>
                        <article>A Sports Science and Nutrition Expert, author of the fitness book, "Your Dream Body Come True," Ed offers consultations and specialized diet plans and exercise programs that guide you to achieve a lean and toned physique.</article>
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </section><!-- #dream-team -->
    <section id="testimonials" class="clearfix" data-0="background-position:0px 0px;" data-100000="background-position:0px -50000px;">
        <div class="container">
            <header class="section-heading normal-text">
                <h1>Everyone's Geared Up for the Sexy Revolution. See their Testimonial.</h1>
            </header>

            <div class="celebrities">
                <div class="celebrity show_celebrity">
                    <header>
                        <span>RAYMOND GUTIERREZ</span>
                        This is not a quick fix, only a quick start.
                    </header>
                    <figure><a href="javascript:void(0)"><img src="img/celeb-1.jpg" alt="name"></a></figure>
                    <a href="javascript:void(0)" class="read-more">Read More</a>
                </div>
                <div class="celebrity show_celebrity_regine">
                    <header>
                        <span>REGINE VELASQUEZ</span>
                        Breaking the Plateau.
                    </header>
                    <figure><a href="javascript:void(0)"><img src="img/celeb-2.jpg" alt="name"></a></figure>
                    <a href="javascript:void(0)" class="read-more">Read More</a>
                </div>
                <div class="celebrity show_celebrity_georgina">
                    <header>
                        <span>GEORGINA WILSON</span>
                        You are not fat, you have fat.
                    </header>
                    <figure><a href="javascript:void(0)"><img src="img/celeb-3.jpg" alt="name"></a></figure>
                    <a href="javascript:void(0)" class="read-more">Read More</a>
                </div>
                <div class="celebrity show_celebrity_iza">
                    <header>
                        <span>IZA CALZADO</span>
                        The Fat Girl Inside
                    </header>
                    <figure><a href="javascript:void(0)"><img src="img/celeb-4.jpg" alt="name"></a></figure>
                    <a href="javascript:void(0)" class="read-more">Read More</a>
                </div>
                <div class="celebrity show_celebrity_andi">
                    <header>
                        <span>ANDI EIGENMANN</span>
                        Salvation Army
                    </header>
                    <figure><a href="javascript:void(0)"><img src="img/celeb-5.jpg" alt="name"></a></figure>
                    <a href="javascript:void(0)" class="read-more">Read More</a>
                </div>
            </div><!-- .celebrity -->

            <header class="section-heading normal-text">
                <h1>Celebrity Tweets</h1>
            </header>
            <div class="celebrity-tweets">
                <div class="each-tweet">
                    <div class="tweet">
                        Back-to-back non-invasive slimming treatments at @sxysolutions need to get rid of the 10lbs I gained during the holidays. #projectsexy
                    </div>
                    <div class="tprofile clearfix">
                        <figure>
                            <img src="img/twitter-pic.jpg">
                        </figure>
                        <div class="tname">
                            <span class="name">RUFFA GUTIERREZ</span>
                            <a href="javascript:void(0)">@iloveruffag</a>
                        </div>
                    </div>
                </div>
                <div class="each-tweet">
                    <div class="tweet">
                        Holy mother of wow. Dropped 18 pounds in the past month! :)) I’m so friggin happy! :)) Now at @SexySolutions again with @chicogarcia :)
                    </div>
                    <div class="tprofile clearfix">
                        <figure>
                            <img src="img/twitter-pic2.jpg">
                        </figure>
                        <div class="tname">
                            <span class="name">GINO QUILLAMOR</span>
                            <a href="javascript:void(0)">@ginoboi</a>
                        </div>
                    </div>
                </div>
                <div class="each-tweet">
                    <div class="tweet">
                        OMG!!!! I just lost 5.3 inches again @sexysolutions @cristallebelo Grabe! I can’t believe it?! Miraculous talaga dito!
                    </div>
                    <div class="tprofile clearfix">
                        <figure>
                            <img src="img/twitter-pic3.jpg">
                        </figure>
                        <div class="tname">
                            <span class="name">RAJO LAUREL</span>
                            <a href="javascript:void(0)">@rajolaurel</a>
                        </div>
                    </div>
                </div>
                <div class="each-tweet">
                    <div class="tweet">
                        Now off to @SexySolutions quickest way to lose those extra inches when you can’t workout!!!
                    </div>
                    <div class="tprofile clearfix">
                        <figure>
                            <img src="img/twitter-pic4.jpg">
                        </figure>
                        <div class="tname">
                            <span class="name">ANNE CURTIS-SMITH</span>
                            <a href="javascript:void(0)">@annecurtissmith</a>
                        </div>
                    </div>
                </div><br class="clear"/><br/>
                <div class="each-tweet">
                    <div class="tweet">
                        Just the same I ‘cheated’ today by tryin TiteFX by @SexySolutions. Some stubborn fats need some serius help from tecahnology.
                    </div>
                    <div class="tprofile clearfix">
                        <figure>
                            <img src="img/twitter-pic6.jpg">
                        </figure>
                        <div class="tname">
                            <span class="name">DOC HAYDEN</span>
                            <a href="javascript:void(0)">@haydenjr</a>
                        </div>
                    </div>
                </div>
                <div class="each-tweet">
                    <div class="tweet">
                        is at @SexySolutions doing my RF slimming treatments, I’m telling u this stuff works no joke!!
                    </div>
                    <div class="tprofile clearfix">
                        <figure>
                            <img src="img/twitter-pic7.jpg">
                        </figure>
                        <div class="tname">
                            <span class="name">AUBREY MILES</span>
                            <a href="javascript:void(0)">@realaubreymiles</a>
                        </div>
                    </div>
                </div>
                <div class="each-tweet">
                    <div class="tweet">
                        Im at Sexy Solutoins doing Quad Sculpt on my abdomenn and arms. It zaps your muscles and heats the fat!... instagram.com/p/bncuOfr3w_/
                    </div>
                    <div class="tprofile clearfix">
                        <figure>
                            <img src="img/twitter-pic8.jpg">
                        </figure>
                        <div class="tname">
                            <span class="name">RHIAN RAMOS</span>
                            <a href="javascript:void(0)">@whianwamos</a>
                        </div>
                    </div>
                </div>
            </div><!-- .celebrity-tweets --><br class="clear"/>

            <header class="section-heading normal-text">
                <h1>Sexy Solutions Fans</h1>
            </header>
            <div class="celebrity-tweets">
                <div class=" hidden each-tweet">
                    <div class="tweet">
                        Back-to-back non-invasive slimming treatments at @sxysolutions need to get rid of the 10lbs I gained during the holidays. #projectsexy
                    </div>
                    <div class="tprofile clearfix">
                        <figure>
                            <img src="img/twitter-pic9.jpg">
                        </figure>
                        <div class="tname">
                            <span class="name">RUFFA GUTIERREZ</span>
                            <a href="javascript:void(0)">@iloveruffag</a>
                        </div>
                    </div>
                </div>
                <div class="each-tweet hidden">
                    <div class="tweet">
                        Holy mother of wow. Dropped 18 pounds in the past month! :)) I’m so friggin happy! :)) Now at @SexySolutions again with @chicogarcia :)
                    </div>
                    <div class="tprofile clearfix">
                        <figure>
                            <img src="img/twitter-pic2.jpg">
                        </figure>
                        <div class="tname">
                            <span class="name">GINO QUILLAMOR</span>
                            <a href="javascript:void(0)">@ginoboi</a>
                        </div>
                    </div>
                </div>
                <div class=" hidden each-tweet">
                    <div class="tweet">
                        OMG!!!! I just lost 5.3 inches again @sexysolutions @cristallebelo Grabe! I can’t believe it?! Miraculous talaga dito!
                    </div>
                    <div class="tprofile clearfix">
                        <figure>
                            <img src="img/twitter-pic11.jpg">
                        </figure>
                        <div class="tname">
                            <span class="name">RAJO LAUREL</span>
                            <a href="javascript:void(0)">@rajolaurel</a>
                        </div>
                    </div>
                </div>
            </div><!-- .fans-tweets -->

            <div class="fans-message">
                <div class="each-message">
                    <div class="fan-message">
                        “Tried other slimming centers before but none of them worked. But with Sexy Solutions, I lost a total of 10 inches. I also admired the professionalism of the consultants and therapists.”
                    </div>
                    <div class="fan-name">
                        - Nancy Balingit, after losing 10 inches on her abdomen
                    </div>
                </div>
                <div class="each-message">
                    <div class="fan-message">
                        “The Multifirm and Body Sculpt treatments aided in significantly decreasing my belly fat. Since I started my treatments at Sexy Solutions, my waistline dropped from 36 to 31 inches. Now I feel more confident than ever!”
                    </div>
                    <div class="fan-name">
                        - RV Mitra after losing 5 inches on her tummy
                    </div>
                </div>
                <div class="each-message">
                    <div class="fan-message">
                        “Sexy Solutions was a huge part in helping me lose inches and weight. After my package, I was able to buy any clothes that I want without worrying about the size. I hugely recommend Sexy Solutions to those who are frustrated like I was before.”
                    </div>
                    <div class="fan-name">
                        - Dennis Lee after losing 9.5 inches on his abdomen
                    </div>
                </div>
                <div class="each-message">
                    <div class="fan-message">
                        “In my opinion, from the machines I've tried, TiteFX has been the most effective. I grew up with a lot of weight and when I lost the wieght, it left me with saggy skin around my tummy. After the first treatment, there was a difference already in my measurements, and the skin was already tightening. After just 3 treatments, there's already a remarkable difference in appearace. my skin looked tighter and firmer. when I had completed all 6 treatments.”
                    </div>
                    <div class="fan-name">
                        - Dennis Ramos
                    </div>
                </div>
                <div class="each-message">
                    <div class="fan-message">
                        “When I started at Sexy Solutions, I weighed 196 lbs. My goal was just to tone my flabby arms but after 6 just 6 sessions of Lipo Cavitation, I lost 16 lbs and inches off my arms. Can't wait for my next sessions for my upper back.”
                    </div>
                    <div class="fan-name">
                        - Christine Yao after losing 16 lbs and 2.6 inches on her arms
                    </div>
                </div>
                <div class="each-message">
                    <div class="fan-message">
                        “After my 4 appointments, I've already lost a total of 8.6 inches off my tummy!”
                    </div>
                    <div class="fan-name">
                        - Raddy Tropa
                    </div>
                </div>
            </div><!-- .fans-tweets -->
        </div><!-- .container -->
    </section><!-- #testimonials -->
    <section id="gallery" class="clearfix" data-0="background-position:0px 0px;" data-100000="background-position:0px -50000px;">
        <div class="container">
            <header class="section-heading normal-text">
                <h1>VIDEOS</h1>
            </header>
            <div class="gallery-list clearfix">
                <ul>
                    <?php foreach( $videos as $k => $v ) : ?>
                        <li class="show_video"><img style="width: 100%" src="https://img.youtube.com/vi/<?=$v['video_url'] ?>/0.jpg"><span class="overlay" onclick="vidPopup('<?=$v['video_url']?>')"></span></li>
                    <?php endforeach; ?>
                </ul>
            </div><!-- .gallery-list -->
        </div><!-- .container -->
    </section><!-- #gallery -->
    <section id="sexy-treats" class="clearfix">
        <div class="container">
            <header class="section-heading normal-text">
                <h1>SEXY TREATS</h1>
            </header>

            <?php if(!$this->session->userdata('email')) : ?>
                <p class="subheading">Make sure you're logged-in to access this feature.</p>
            <?php endif; ?>
                <p class="subheading sexy-treats-result"></p>

            <div class="sexy-treats-form">
                <p class="label">Key in your unique code to download your promo voucher or get exclusive downloadables.</p>
                <form action="<?=base_url()?>sexy_treats/validate" method="POST" onsubmit="return validatePromoCode()">
                    <input type="text" name="sexy_treats_code">
                    <?php if($this->session->userdata('email')) : ?>
                        <button type="submit" class="submit-sexy-treats-code">Submit</button>
                    <?php else : ?>
                        <button type="submit" class="show_login">Submit</button>
                    <?php endif; ?>
                </form>
            </div>
        </div><!-- .container -->
    </section><!-- #sexy-treats -->
    <section id="contact" class="clearfix">
        <div class="contact-img-bg">
            <div class="container">
                <p>Got a Question? We're here to listen. </p>
                <figure class="contact-number">8107399</figure>
                <p><a href="mailto:inquiries@sexysolutions.net">inquiries@sexysolutions.net</a></p>

                <hr>

                <p>Want to be part of our team? Email your Curriculum Vitae to:</p>
                <p><a href="mailto:recruitment@sexysolutions.net">recruitment@sexysolutions.net</a></p>

                <hr>

                <p>Got a proposal for us? Want to collaborate? Shoot us an email at</p>
                <p><a href="mailto:marketing@sexysolutions.net">marketing@sexysolutions.net</a></p>
            </div><!-- .container -->
        </div>
    </section><!-- #contact -->
    <div class="pink-bar"></div>
</section><!-- .main content -->

<footer class="main">
    <div class="container">
        <div class="hashtag"><a href="#">#SexyRevolution</a></div>
        <div class="socials">
            <a href="http://www.facebook.com/SexySolutions" class="fb" target="_blank" title="Facebook">Facebook</a>
            <a href="http://www.twitter.com/sxysolutions" class="tw" target="_blank" title="Twitter">Twitter</a>
            <a href="https://www.youtube.com/user/sexysolutionsbybelo" class="yt" target="_blank" title="YouTube">Youtube</a>
            <a href="http://www.instagram.com/sxysolutions" class="in" target="_blank" title="Instagram">Instagram</a>
        </div>
    </div><!-- .container -->
</footer><!-- .main footer -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=base_url()?>js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAl_Gv7L6SFryECn6LLncPicpNjsXNliJA&sensor=false"></script>
    <script type="text/javascript">
        var baseUrl = '<?=base_url() ?>';
        var updateSuccess = '<?=@$updateSuccess?>';
    </script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      <?php if($this->session->userdata('userid')): ?>
        ga('create', 'UA-42684485-9', { 'userId' : '<?php echo $this->session->userdata("userid")?>' });
      <?php else: ?>
        ga('create', 'UA-42684485-9', 'sexysolutions.com');
      <?php endif; ?>
      
      ga('send', 'pageview');
    </script>

<!--[if gt IE 8]><!-->
    <script type="text/javascript" src="<?=base_url()?>js/vendor/skrollr.min.js"></script>
<!--<![endif]-->
<script type="text/javascript" src="<?=base_url()?>js/vendor/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/vendor/jquery-ui.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/vendor/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/vendor/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/vendor/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/main.min.js"></script> <!-- ORIG main.js MIN main.min.js -->
<script type="text/javascript" src="<?=base_url()?>js/magnific.min.js"></script> <!-- ORIG magnific.js MIN magnific.min.js -->
<script type="text/javascript" src="<?=base_url()?>js/vendor/fancybox/jquery.fancybox.js"></script>

<script type="text/javascript">
   var map1Long = '<?=@$googleMap[0]["longitude"] ?>',
        map1Lat = '<?=@$googleMap[0]["latitude"] ?>',
        map2Long = '<?=@$googleMap[1]["longitude"]?>',
        map2Lat = '<?=@$googleMap[1]["latitude"]?>',
        r_id = '<?=@$r_id ?>';

    $('.logout-btn').click(function(){
        window.location.href = baseUrl+'logout';
    });
    $('.fancy-link').fancybox({});
</script>

<script type="text/javascript" src="<?=base_url()?>js/custom.min.js"></script> <!-- ORIG custom.js MIN custom.min.js -->
<script type="text/javascript" src="<?=base_url()?>js/googlemap.min.js"></script> <!-- ORIG custom.js MIN custom.min.js -->
</body>
</html>
