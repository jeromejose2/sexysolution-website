<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	private $CI;

	public function __construct() {

		parent::__construct();
		$this->CI =& get_instance();

	}

	public function index() {

		if( $_POST ) {
			$params = array(
				'table'=>'tbl_registrants',
				'where'=>'id='.$_POST['update_id'],
				'post'=>array(
					'password'=>md5($_POST['password'].SALT),
					'token'=>uniqid('', true)
				)
			);
			$this->mysql_queries->update_data( $params );

			$this->data['updateSuccess'] = true;
		} else {
			$this->data['updateSuccess'] = false;
		}

		$this->data['googleMap'] = $this->mysql_queries->get_data(array('table'=>'tbl_clinics'));
		$this->data['packages'] = $this->mysql_queries->get_data(array('table'=>'tbl_packages'));
		$this->data['videos'] = $this->mysql_queries->get_data(array('table'=>'tbl_videos'));
		$this->data['about'] = $this->mysql_queries->get_data(array('table'=>'tbl_about'));
		$this->data['analytics'] = $this->mysql_queries->get_data(array('table'=>'tbl_settings','where'=>'type=\'analytics\''));

		$this->CI->load->view('main-page', $this->data);

	}

	public function login() {

		$params = array(
			'table'=>'tbl_registrants',
			'where'=>'email=\''.$_POST['email'].'\' AND password=\''.md5($_POST['password'].SALT).'\''
		);
		$result = $this->mysql_queries->get_data( $params );
		if( $result ) {
			$newdata = array(
				'userid'=>$result[0]['id'],
				'firstname'=>$result[0]['firstname'],
				'lastname'=>$result[0]['lastname'],
				'email'=>$result[0]['email']
			);
			$this->session->set_userdata($newdata);
			echo true;
		} else {
			echo false;
		}

	}
	
}

?>