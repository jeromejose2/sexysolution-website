<!-- BEGIN NEW POPUP -->
<div class="modal hide fade" id="popupNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 370px; margin-left: -115px">
  <div class="form-content" style="background: rgba(0,0,0,0);">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4><small>NEW CODE</small></h4>
        </div>
        <div class="modal-body">
       <form action="<?=base_url()?>admin/code/save" method="POST">
          <div class="input-prepend">
                  <span class="add-on">TITLE</span>
                  <input type="text" class="input-medium required" name="title" alt="Enter code title" >
          </div>
          <div id="more"></div>
          <div class="input-prepend">
                  <span class="add-on">CODE</span>
                  <input type="text" class="input-medium required" name="code[]" alt="Enter code" >
          </div>
          <a class="btn btn-small add-code">Add</a>
        </div>
        </div>
          <div class="modal-footer">
          <input class="btn btn-small btn-primary submit" type="submit" value="SAVE">
          </div>
      </form>
</div>
<!-- END NEW POPUP -->

<!-- BEGIN EDIT POPUP -->
<div class="modal hide fade edit-code-content" id="popupEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 370px; margin-left: -115px">
    
        <!-- <div class="edit-code-content"></div> -->
  </div>
<!-- END EDIT POPUP -->

<!-- BEGIN DELETE POPUP -->
<div class="modal hide fade" id="popupDelete">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><small>DELETE VIDEO</small></h4>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete this item?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-small btn-primary delete-yes" data-dismiss="modal" aria-hidden="true" onclick="delete">YES</button>
    <button class="btn btn-small" data-dismiss="modal" aria-hidden="true">CLOSE</button>
  </div>
</div>
<!-- END DELETE POPUP -->

<div class="page-controls">

    <a href="#popupNew" class="btn btn-primary" role="button" data-toggle="modal"> <i class="icon-plus"></i><span class="hidden-phone"> New</span></a>

     <a href="<?=site_url('export/promo_codes?'.http_build_query($_GET, '', "&"))?>" class="btn hidden-phone"><i class="icon-download-alt"></i> Export</a> 

</div>



    <table class="table table-hover table-bordered table-heading">

        <thead>

            <tr>

                <td>TITLE</td>

                <td>CODE</td>

                <td style="width: 110px">ACTION</td>

            </tr>

        </thead>

        <tbody>

            </tr>

            <tr>

               <?php 

            if( $items ) : 

                foreach( $items as $k => $v ) :

            ?>

             <tr id="row-<?=$v['id']?>">

                <td><?=$v['title']?></td>

                <td>
                    <?php
                        $codes = unserialize($v['code']);
                        if( $codes ) {
                            foreach( $codes as $k => $value ) {
                                echo $value.'<br>';
                            }
                        }
                    ?>
                </td>

                <td>

                  <a href="#popupEdit" role="button" data-toggle="modal" class="btn btn-small edit" edit-id="<?=$v['id']?>" data-id="<?=$v['id']?>">Edit</a>

                  <a href="#popupDelete" class="btn btn-small delete" role="button" data-toggle="modal" data-id="<?=$v['id']?>" delete-id="<?=$v['id']?>">Delete</a>

                </td>

            </tr>

            <?php endforeach; else: ?>

            <tr>

                <td colspan="10"><center>No Result</center></td>

            </tr>

            <?php endif;?>

            <tr>

               <td colspan="10"><h4>Total: <?=$total?></h4></td>

          </tr>

        </tbody>



    </table>





    <div class="pagination  pull-right"><?php echo $pagination?></div>



<style type="text/css">img._status{cursor: pointer;}</style>



<script type="text/javascript">
  function deletePromo(id) {
        $.post('<?=base_url()?>admin/code/delete/'+id, function(){
            $('#row-'+id).hide();
        });
    }
$(function() {
    var id;
    $('#new-code').click(function(){
          popup.open({url:'<?=base_url()?>admin/popup/new_code', top: 40});
    });

    $('.edit').on('click', function(){
        var id = $(this).data('id');
        $('.edit-code-content').html('<div style="margin: 25% auto 25% 45%"><img src="<?=base_url()?>images/admin/content-loader.GIF"></div>');
        $.post('<?=base_url()?>admin/code/edit_code_content/'+id, function (result){
            $('.edit-code-content').html(result);
        });
    });

    $('.delete').on('click', function(){
        var id = $(this).data('id');
        $('.delete-yes').attr('data-id', id);
        $('.delete-yes').attr('onclick', 'deletePromo('+id+')');
    });

    // $('.delete-yes').on('click', function(){
    //     var id = $(this).data('id');
    //     $.post('<?=base_url()?>admin/code/delete/'+id, function(){
    //         $('#row-'+id).hide();
    //     });
    // });


    // $('.edit').on('click', function(){

    //       id = $(this).attr('edit-id');

    //       popup.confirm('Edit this video?', 'Edit', function(){

    //         popup.open({url:'<?=base_url()?>admin/code/edit/'+id});

    //       });

    // });


    // $('.delete').on('click', function(){

    //       id = $(this).attr('delete-id');

    //       popup.confirm('Delete this video?', 'Delete', function(){

    //         $.post('<?=base_url()?>admin/code/delete/'+id, function(){

    //             $('#row-'+id).hide();

    //         });

    //       });

          

    // });



    $('.advanced').click(function(){

          $('.advance-search').slideToggle();

     })



    $('input[name="from"]').datepicker({

        changeMonth: true,

        numberOfMonths: 1,

        dateFormat:'yy-mm-dd',

        onClose: function( selectedDate ) {

            $( 'input[name="to"]').datepicker( "option", "minDate", selectedDate );

        }

    });

    $('input[name="to"]').datepicker({

        defaultDate: "+1w",

        changeMonth: true,

        numberOfMonths: 1,

        dateFormat:'yy-mm-dd',

        onClose: function( selectedDate ) {

            $('input[name="from"]').datepicker( "option", "maxDate", selectedDate );

        }

    });



    $('.action > .btn').click(function(){

          var el = $(this);

          var activeClass = 'btn-success';

          var action = 'status';

          var post;

          var data = {table : 'entries',

                      wherec: 'id',

                      wheref: el.parent().data('id'),

                      items : {}}



          if( el.hasClass(activeClass) ){

               el.removeClass(activeClass);

               data.items[action] = 0;

          }else{

               el.addClass(activeClass);

               el.siblings().removeClass(activeClass);

               data.items[action] = el.attr('rel');

          }

          $.post('<?=site_url()?>/ajax/update',data);

     })

    

});

$('.add-code').bind('click', function(){
        var id = Math.floor(Math.random() * 90000) + 10000;
        $('#more').after('<div class="input-prepend" id="'+id+'"><span class="add-on">CODE</span><input type="text" class="input-medium required" name="code[]" alt="Enter code" ><a title="Remove this code" class="badge badge-important" style="margin-top: 5px" onclick="del('+id+')"><i class="icon-remove"></i></a></div>');
    });

    function del( id ) {
        $('#'+id).remove();
    }


</script>