<form method="POST" action="<?=base_url()?>admin/code/update">
<div class="form-content" style="background: rgba(0,0,0,0);">
<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h4><small>EDIT CODE</small></h4>
</div>
<div class="modal-body">
<input type="hidden" name="id" value="<?=$result[0]['id']?>">
<div class="input-prepend">
      <span class="add-on">TITLE</span>
      <input type="text" class="input-medium required" name="title" alt="Enter code title" value="<?=$result[0]['title']?>">
</div>

<div id="more2"></div>

<?php if( $results ) : ?>
    <?php $i=1; foreach( $results as $k => $v ) : ?>
        <div class="input-prepend" id="<?=$i?>">
            <span class="add-on">CODE</span>
            <input type="text" class="input-medium" name="code[]" value="<?=$v?>">
            <a title="Remove this code" class="badge badge-important" onclick="del(<?=$i?>)" style="margin-top: 5px"><i class="icon-remove"></i></a>
        </div>
    <?php $i++; endforeach ?>
<?php else : ?>
    <div class="input-prepend">
            <span class="add-on">CODE</span>
            <input type="text" class="input-medium required" name="code[]" alt="Enter code" >
    </div>
<?php endif; ?>
<a class="btn btn-small add-code2">Add</a>
<!-- <input class="btn btn-small btn-primary submit" type="submit" value="UPDATE">
</form> -->
</div>
    
<div class="modal-footer" style="width: 100%; margin-left: -15px; margin-bottom: -35px">
    <input class="btn btn-small btn-primary submit" type="submit" value="UPDATE">
</div>
</form>
<!-- </div> -->

<script type="text/javascript">
  $('.add-code2').bind('click', function(){
      var id = Math.floor(Math.random() * 90000) + 10000;
      $('#more2').after('<div class="input-prepend" id="'+id+'"><span class="add-on">CODE</span><input type="text" class="input-medium required" name="code[]" alt="Enter code" ><a title="Remove this code" class="badge badge-important" style="margin-top: 5px" onclick="del('+id+')"><i class="icon-remove"></i></a></div>');
  });
</script>