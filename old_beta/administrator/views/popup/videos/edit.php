<div class="container" style="width:350px;margin:0 auto;">
<a href="javascript:void(0)" class="lytebox-close" style="right: 320px">x</a>
<div class="well">
      
    <div class="form-content" style="background: rgba(0,0,0,0);">

      <h5>Update YouTube Video</h5>

     <form action="<?=base_url()?>admin/videos/update" method="POST">
        <input style="display:none" name="id" value="<?=$video[0]['id']?>">
        <div class="input-prepend">
                <span class="add-on">TITLE</span>
                <input type="text" class="input-medium" name="name" value="<?=$video[0]['title']?>">
        </div>

        <div class="input-prepend">
                <span class="add-on">VIDEO URL</span>
                <input type="text" class="input-medium" name="video_url" value="http://www.youtube.com/watch?v=<?=$video[0]['video_url']?>">
        </div>
        <input class="btn btn-primary submit" type="submit" value="Update">
    </form>

   </div>
</div>
</div>