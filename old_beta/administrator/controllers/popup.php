<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Popup extends CI_Controller {

	public function videos_preview() {

		$params = array(
			'table'=>'tbl_videos',
			'where'=>'id='.$_GET['id']
		);
		$result = $this->mysql_queries->get_data($params);

		$this->data['video_url'] = $result[0]['video_url'];
		$this->load->view('popup/videos/preview', $this->data);

	}

	public function new_video() {

		$this->load->view('popup/videos/new');

	}

	public function new_code() {

		$this->load->view('popup/code/new');

	}

}