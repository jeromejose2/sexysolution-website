// Clear Input Fields
$('.show_register').click(function(){
	$('#register-form').trigger('reset');
	$('.registration-error-mgs').html('');
});
$('.show_forgot_password').click(function(){
	$('#forgot-password-form').trigger('reset');
	$('.forgotpw-error-mgs').html('');
});
$('.show_login').click(function(){
	$('#login-form').trigger('reset');
	$('.login-error-mgs').html('');
});

// CAPTCHA
function refreshCaptcha() {
	$('.captcha-refresh').addClass('ajax-loader');
	$('#captcha').attr('src', baseUrl+'securimage/securimage_show.php?'+Math.random()).load(function(){
		$('.captcha-refresh').removeClass('ajax-loader');
	});
	return false;
}

// Video Popup
function vidPopup(url) {
	$('#video').html('<br><iframe width="750" height="380" src="https://www.youtube.com/embed/'+url+'?autoplay=1" frameborder="0" allowfullscreen></iframe>');
}

// Validate Registration
function validateRegistration() {
	var error = false;
	var letters_format = /^[a-zA-Z-'\s]+$/;
	var email_format = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	var password = $('.password').val();

	$('.required').each(function(){
		var elem = $(this);
		var error_msg = $(this).attr('alt');
		var value = $.trim($(this).val());

		if(elem.val() == '') {
			error = error_msg;
			elem.focus();
			return false;
		} else if (elem.attr('name') == 'firstname') {
			if(!letters_format.test(value)) {
				error = 'Sorry, you may only input valid character letters';
				elem.focus();
				return false;
			}
		} else if (elem.attr('name') == 'lastname') {
			if(!letters_format.test(value)) {
				error = 'Sorry, you may only input valid character letters';
				elem.focus();
				return false;
			} 
		} else if (elem.attr('name') == 'email') {
			if(!email_format.test(value)) {
				error = 'Sorry, the email address you entered doesn’t seem to be valid.';
				elem.focus();
				return false;
			}
		} else if (elem.attr('name') == 'password') {
			if(value.length < 6) {
				error = 'Sorry, the password must have atleast 6 characters or more.';
				elem.focus();
				return false;
			}
		} else if (elem.attr('name') == 're-password') {
			if(value != password) {
				error = 'Hold on, the passwords that you typed aren’t the same! Please retype them and try again'
				return false;
			}
		}
	});

	if( error ) {
		$('.registration-error-mgs').html('<br>'+error);
		return false;
	} else {
		$('.registration-error-mgs').html('<br>Please wait.');
		data = {firstname:$('.register-firstname').val(), lastname:$('.register-lastname').val(), email:$('.register-email').val(), password:$('.register-password').val(), captcha_code : $('input[name=captcha_code]').val()};
		$.post(baseUrl+'register', data, function (result){
			if(result) {
				$('.registration-error-mgs').html('<br>'+result);
			} else {
				$('.show_in_register_success').trigger('click');
				$('.registration-error-mgs').html('');
				$('.captcha-refresh').addClass('ajax-loader');
				$('#captcha').attr('src', baseUrl+'securimage/securimage_show.php?'+Math.random()).load(function(){
					$('.captcha-refresh').removeClass('ajax-loader');
				});
			}
		});
		return false
	}
	return false;
}

// Validate Login
function validateLogin() {
	var error = false;
	var email_format = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

	$('.required-login').each(function(){
		var elem = $(this);
		var error_msg = $(this).attr('alt');
		var value = $.trim($(this).val());

		if(elem.val() == '') {
			error = error_msg;
			elem.focus();
			return false;
		} else if (elem.attr('name') == 'email') {
			if(!email_format.test(value)) {
				error = 'Sorry, the email address you entered doesn’t seem to be valid.'
				elem.focus();
				return false;
			}
		} else if (elem.attr('name') == 'password') {
			if(value.length < 6) {
				error = 'Sorry, the password must have atleast 6 characters or more.';
				elem.focus();
				return false;
			}
		}
	});

	if( error ) {
		$('.login-error-mgs').html('<br>'+error);
		return false;
	} else {
		$('.login-error-mgs').html('<br>Please wait.');
		data = {email : $('.email-login').val(), password : $('.password-login').val()};
		$.post(baseUrl+'home/login', data, function(r){
			if(r) {
				window.location.href = baseUrl;
			} else {
				$('.login-error-mgs').html('<br>Sorry, the email or password you have entered is invalid.');
				return false;
			}
		});
	}
	return false;
}

// Validate Forgot Password
function validateForgotPassword() {
	var error = false;
	var email_format = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

	$('.required-forgot-password').each(function(){
		var elem = $(this);
		var error_msg = $(this).attr('alt');
		var value = $.trim($(this).val());

		if(elem.val() == '') {
			error = error_msg;
			elem.focus();
			return false;
		} else if (elem.attr('name') == 'email') {
			if(!email_format.test(value)) {
				error = 'Sorry, the email address you entered doesn’t seem to be valid.';
				elem.focus();
				return false;
			}
		}
	});

	if(error) {
		$('.forgotpw-error-mgs').html('<br>'+error);
		return false;
	} else {
		$('.forgotpw-error-mgs').html('<br>Please wait.');
		data = {email : $('.required-forgot-password').val()};
		$.post(baseUrl+'forgot_password', data, function (result){
			if(result == 'failed') {
				$('.forgotpw-error-mgs').html('<br>Sorry, the email address you entered is not yet registered.');
				$('.required-forgot-password').focus();
			} else {
				$('.show_in_password_retrieval').trigger('click');
			}
		});
		return false;
	}
	return false;
}

function validateUpdatePassword() {
	var error = false;
	var password = $('.update-password').val();

	$('.required-update-password').each(function(){
		var el = $(this);
		var error_msg = $(this).attr('alt');
		var value = $.trim($(this).val());

		if(el.val() == '') {
			error = error_msg;
			el.focus();
			return false;
		} else if(el.attr('name') == 'repassword') {
			if(value != password) {
				error = 'Sorry, your password did not match.'
				el.focus();
				return false;
			}
		} else if(el.attr('name') == 'password') {
			if(value.length < 6) {
				error = 'Sorry, the password must have atleast 6 characters or more.';
				el.focus();
				return false;
			}
		}
	});

	if( error ) {
		$('.error-mgs-update-password').html('<br>'+error);
		return false;
	} else {
		$('.error-mgs-update-password').html('<br>Please wait.');
		return true;
	}
}

function validatePromoCode() {
	el = $('.sexy-treats-result');
	var user_input_code = $.trim($('input[name=sexy_treats_code]').val());
	if(!user_input_code == '') {
		el.html('<img src="'+baseUrl+'img/AjaxLoader.gif">');
		$.post(baseUrl+'sexy_treats/validate', {input:user_input_code}, function (result){
			if(!result) {
				el.html('Oh no, that code isn’t valid. Please check your code and try again.');
				$('input[name=sexy_treats_code]').focus();
			} else {
				el.html(result);
			}
		});
	}
	return false;
}