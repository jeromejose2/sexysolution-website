$(function () {
	// preventDefault()
	customSelect()
	popup()
	backtoTop()
	navScroll()
	showNavName()
	tab()
	moreDetails()
	seeOtherView()

	// Parallax Effect
	if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
	    skrollr.init({
	        forceHeight: false
	    });
	}

	// FitVids
    $(".video_responsive").fitVids();

    $('#mininav').on('click',function(){
    	$('.navigation').slideToggle(200)
    	$('.navigation').toggleClass('active')
    })  
    
    $('.slider').slimScroll({
	    width: '100%',
	    height: '400px',
	    size: '8px',
	    color: '#e66ab0',
	    alwaysVisible: true,
	    distance: '0',
	    railVisible: true,
	    railColor: '#671242',
	    railOpacity: 1,
	    wheelStep: 10,
	    allowPageScroll: false,
	    disableFadeOut: false
	}); 

    // Packages Slider
    var owl = $("#packages");

  	owl.owlCarousel({
	    // Most important owl features
	    items : 1,
	    itemsCustom : false,
	    itemsDesktop : [1199,1],
	    itemsDesktopSmall : [980,1],
	    itemsTablet: [768,1],
	    itemsTabletSmall: false,
	    itemsMobile : [479,1],
	    singleItem : false,
	    itemsScaleUp : false,
	 
	    //Pagination
	    pagination : true,
	    paginationNumbers: false
	 
	});

	$('a[href^="#prev_owl_item"]').on('click',function(e){
		e.preventDefault()
		owl.trigger('owl.prev')
	});
	$('a[href^="#next_owl_item"]').on('click',function(e){
		e.preventDefault()
		owl.trigger('owl.next')
	});
 
})

function seeOtherView() {
	var	buttons = $('.tabs article button')

	function showOtherView() {
		var button = $(this),
			figure = button.siblings(),
			front = button.siblings('.front-view'),
			back = button.siblings('.back-view')

		if(front.hasClass('active')) {
			figure
				.hide()
			front
				.removeClass('active')
			back
				.addClass('active')
				.show()
			button
				.html('See Front View')

			return
		}

		figure
			.hide()
		back
			.removeClass('active')
		front
			.addClass('active')
			.show()
		button
			.html('See Back View')
		

	}

	buttons.on('click', showOtherView)
}
function moreDetails() {
	var buttons = $('.white-boxes>.box .btn, .red-boxes>.box .btn')
		contents = buttons.siblings('p')

	function showContent() {
		var button = $(this),
			content = button.siblings('p')


		if (button.hasClass('open')) {
			$(this)
				.removeClass('open')
				.html('More Details')
			content
				.hide()
			return
		}

		content
			.show()
		button
			.addClass('open')
			.html('Less Details')

	}

	buttons.on('click', showContent)

}

function tab() {
	var tabs = $('.tabs-nav a')
    var panels = $('.tabs article')

    function showPanel(e) {
        var tab = $(this)
        var index = tab.index()

        e.preventDefault()

        tab
            .addClass('current')
            .siblings()
            .removeClass('current')

        panels
            .hide()
            .eq(index)
            .show()
    }

    panels
        .not(':first')
        .hide()

    tabs.on('click', showPanel)
}

function preventDefault() {
	// Prevent Default
	$('a').on('click',function(event){
		event.preventDefault()
	})
}

function backtoTop() {
	$('.logo>a[href^="#home"]').click(function () {
		$('html,body').animate({
			scrollTop: 0
		}, 500);
		return false;
	});
}

function customSelect() {
	var select = jQuery('.select-style'), 
        list = select.children('ul'),
        listItems = list.children(),
        win = jQuery(window)

    function showList() {
        var thisList = jQuery(this),
            list = thisList.children('ul')

        list.slideToggle(200)
    }

    function selectItem() {
        var item = jQuery(this),
            text = item.text(),
            selected = item.parent('ul').siblings('.selected')

        if(text == 'Select Filter') {
            selected
                .html(text)
                .css('color','#959595')
        }
        else {
            selected
                .html(text)
                .css('color','#363636')
        }
    }

    select.on('click', showList)
    listItems.on('click', selectItem)
}

function popup() {
	//  Hide
	$('#popup-register, #popup-upload, #popup-thankyou, #popup-full-mechanics').hide()

	// Close
	$('.close').on('click',function(){
		$(this).parents('.popup').parent().hide()
	})

	// Show Register
	$('.show_register_popup').on('click',function(){
		$('#popup-register').show()
	})

	// Show Upload
	$('.show_upload_popup').on('click',function(){
		$('#popup-upload').show()
	})

	// Show Thanks
	$('.show_thanks_popup').on('click',function(){
		$('#popup-thankyou').show()
	})

	// Show Full Mechanics
	$('.show_full_mechanics').on('click',function(){
		$('#popup-full-mechanics').show()
	})
}

function navScroll() {

    var links = $('.navigation a[href^="#"]')
    var linksA = $('.navigation a[href^="#"]')
    var scrollables = $('html, body')

    function doScroll(e) {
    	var link = $(this)
        var id = $(this).attr('href')
        if(id == '#dream-team' || id == '#treatment') {
        	var top = $(id).offset().top - 45
        } else {
        	var top = $(id).offset().top - 15
        }

        e.preventDefault()

        scrollables.animate({
            scrollTop: top
        })

    }	

    $(window).scroll(function() {
    	var winPos = $(window).scrollTop(),
    		homeHeight = $('#home').height(),
			showThem = $('header.main, footer.main'),
			homeEnd = $('#home').offset().top + $('#home').height() - 60,
			aboutEnd = $('#about').offset().top + $('#about').height() - 60,
			treatmentEnd = $('#treatment').offset().top + $('#treatment').height() - 60,
			locationEnd = $('#location').offset().top + $('#location').height() - 60,
			pricingEnd = $('#pricing').offset().top + $('#pricing').height() - 60,
			dreamteamEnd = $('#dream-team').offset().top + $('#dream-team').height() - 60,
			testimonialsEnd = $('#testimonials').offset().top + $('#testimonials').height() - 60,
			galleryEnd = $('#gallery').offset().top + $('#gallery').height() - 60,
			sexytreatsEnd = $('#sexy-treats').offset().top + $('#sexy-treats').height() - 60,
			contactEnd = $('#contact').offset().top + $('#contact').height() - 60


		// Nav Active
		if(winPos < homeEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
		} else if(winPos > homeEnd && winPos < aboutEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
		} else if(winPos > aboutEnd && winPos < treatmentEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
			$('.navigation a[href^="#treatment"]').addClass('active');
		} else if(winPos > treatmentEnd && winPos < locationEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
			$('.navigation a[href^="#location"]').addClass('active')
		} else if(winPos > locationEnd && winPos < pricingEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
			$('.navigation a[href^="#pricing"]').addClass('active')
		} else if(winPos > pricingEnd && winPos < dreamteamEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
			$('.navigation a[href^="#dream-team"]').addClass('active')
		} else if(winPos > dreamteamEnd && winPos < testimonialsEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
			$('.navigation a[href^="#testimonials"]').addClass('active')
		} else if(winPos > testimonialsEnd && winPos < galleryEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
			$('.navigation a[href^="#gallery"]').addClass('active')
		} else if(winPos > galleryEnd && winPos < sexytreatsEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
			$('.navigation a[href^="#sexy-treats"]').addClass('active')
		} else if(winPos > sexytreatsEnd && winPos < contactEnd) {
			$('.navigation a[href^="#"]').removeClass('active')
			$('.navigation a[href^="#contact"]').addClass('active')
		}

		// See video btn
		if (winPos > homeHeight) {
			showThem.show()
		} else {
			showThem.hide()
		}	

    })
    
    links.on('click', doScroll)

}

function showNavName() {
	var items = $('.navigation a[href^="#"]'),
		winWidth = $(window).width()

	if(winWidth < 1000) {
		return
	}

	function showName() {
		var item = $(this),
			name = item.text()

		$('.show-full-name').remove()
		item.prepend('<span class="show-full-name">'+ name +'</span>')
		item.on('mouseout', function(){
			$('.show-full-name').remove()
		})

	}

	items.on('mouseover', showName)
}